package com.project.terabit.validatorTest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.model.Image;
import com.project.terabit.model.Property;
import com.project.terabit.validator.AddPropertyValidator;

public class AddPropertyValidatorTest extends TerabitApplicationTests {
	
	
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	UUID uuid=UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445");
	public Property getAddPropertyUser() {
		
		Property addPropertyUser =new Property();
		addPropertyUser.setPropertyCent(BigInteger.valueOf(1l));
		addPropertyUser.setPropertyCity("MIT");
		addPropertyUser.setPropertyCountry("India");
		addPropertyUser.setPropertyDescription("good neart to urappakkam");
		
		addPropertyUser.setPropertyEsteematedAmount("25");
		List<Image> imageList =new ArrayList<>();
		addPropertyUser.setPropertyImageIds(imageList);
		addPropertyUser.setPropertyLandmark("VGP HOUse");
		addPropertyUser.setPropertyLatitude("10");
		addPropertyUser.setPropertyLongitude("01");
		addPropertyUser.setPropertyOwnedBy(1);
		addPropertyUser.setPropertyState("State");
		addPropertyUser.setPropertyType("type");
		
		
		
		return addPropertyUser;
	}
	@Test
	public void isValidPropertyOwnedByAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_owned_by");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyOwnedBy(3);
		
		AddPropertyValidator.validate(addPropertyUser,uuid);
		
	}
	

	@Test
	public void isValidCityAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_city");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyCity("1234");
		
		AddPropertyValidator.validate(addPropertyUser,uuid);
		
		
	
		
	
	}
	
	@Test
	public void isValidStateAdd() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_state_or_country");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyState("1234");
		
		AddPropertyValidator.validate(addPropertyUser,uuid);
		
	}
	
	@Test
	public void isValidCountryAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_state_or_country");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyCountry("1223");
		AddPropertyValidator.validate(addPropertyUser,uuid);
		
		
	}
	
	@Test
	public void isValidPropertyLatitudeAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_latitude_or_longitude");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyLatitude("solai");
		
		AddPropertyValidator.validate(addPropertyUser,uuid);
		
	}
	
	@Test
	public void isValidPropertyLongitudeAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_latitude_or_longitude");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyLongitude("solai");
		
		AddPropertyValidator.validate(addPropertyUser,uuid);
	}
	
	@Test
	public void isValidPropertyEstimatedAmountAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_esteemated_amount");
		
		Property addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyEsteematedAmount("solai");
		
		AddPropertyValidator.validate(addPropertyUser,uuid);
		
	}


}
