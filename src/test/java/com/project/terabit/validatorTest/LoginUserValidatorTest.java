package com.project.terabit.validatorTest;

import java.math.BigInteger;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import com.project.terabit.model.User;
import com.project.terabit.validator.LoginUserValidator;

public class LoginUserValidatorTest {
	

	
	public User getUser() {
		 User user = new User();
	     user.setUserEmailId("lisadeepik@gmail.com");
	     user.setUserContactNo(BigInteger.valueOf(8681999437l));
	     user.setUserPassword("Deepik@lisa79");
	     return user;
	 }
	
	@Rule
	 public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	 public void isValidEmailIdNormalString() throws Exception{
		
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai");
	     
	     LoginUserValidator.validate(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutSC() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai.com");
	     
	     LoginUserValidator.validate(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutDot() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai@gmailcom");
	     
	     LoginUserValidator.validate(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithExtraLengthAfterDot() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai@gmail.comin");
	     
	     LoginUserValidator.validate(user_to_be_logged_in);
	     
	 }
	 
	 @Test
	 public void isValidContactNumberLessLength() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_phone_no");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserContactNo(BigInteger.valueOf(868199943l));
	     
	     LoginUserValidator.validate(user_to_be_logged_in);
		 
	 }
	 
	 @Test
	 public void isValidContactNumberMoreLength() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_phone_no");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserContactNo(BigInteger.valueOf(868199943333l));
	     
	     LoginUserValidator.validate(user_to_be_logged_in);
		 
	 }

}
