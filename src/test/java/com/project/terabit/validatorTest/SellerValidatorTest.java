package com.project.terabit.validatorTest;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.model.CreateSeller;
import com.project.terabit.validator.SellerValidator;

public class SellerValidatorTest extends TerabitApplicationTests {
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	public CreateSeller getCreateseller() {
		UUID uuid = UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb");
		CreateSeller createseller =new CreateSeller();
		createseller.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		createseller.setUserId(uuid);
		return createseller;
		
	}
	@Test
	public void isValidPhoneNumberMinLenght() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERVALIDATOR.invalid_phone_no");
		
		CreateSeller createseller=this.getCreateseller();
		createseller.setSellerPrivateContact(BigInteger.valueOf(723456l));
		
		
		SellerValidator.validate(createseller);
		
	}
	@Test
	public void isValidPhoneNumberMaxLenght() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERVALIDATOR.invalid_phone_no");
		
		CreateSeller createseller=this.getCreateseller();
		createseller.setSellerPrivateContact(BigInteger.valueOf(712334567799l));
		
		
		SellerValidator.validate(createseller);
		
	}
	
	@Test
	public void isValidPhoneNumberAllSame() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERVALIDATOR.invalid_phone_no");
		
		CreateSeller createseller=this.getCreateseller();
		createseller.setSellerPrivateContact(BigInteger.valueOf(7777777777l));
		
		
		SellerValidator.validate(createseller);
		
	}
	@Test
	public void isValidPhoneNumberStartWithLessThanSix() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERVALIDATOR.invalid_phone_no");
		
		CreateSeller createseller=this.getCreateseller();
		createseller.setSellerPrivateContact(BigInteger.valueOf(5123456789l));
		
		
		SellerValidator.validate(createseller);
		
	}


}
