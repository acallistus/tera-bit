package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Property;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validatorTest.AddPropertyValidatorTest;

public class ViewedPropertyServiceTest extends TerabitApplicationTests {
	@Mock
	PropertyRepository propertyRepository;
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	ViewedPropertyServiceImpl viewedPropertyServiceImpl;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	String saltString="aaaaaaaa";
	UUID uuid=UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445");
	public ViewedProperty getViewedProperty()
	{
		AddPropertyValidatorTest addPropertyValidatorTest = new AddPropertyValidatorTest();
		ViewedProperty viewedProperty=new ViewedProperty();
		viewedProperty.setViewedPropertyId(BigInteger.valueOf(100l));
		viewedProperty.setViewedSellerId(BigInteger.valueOf(1l));
		viewedProperty.setViewedPropertyPropertyId(addPropertyValidatorTest.getAddPropertyUser());
		viewedProperty.setViewedUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
		return viewedProperty;
	}
	
	@Test
	public void isSaltStringNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.no_saltstring");
		
		saltString =null;
		ViewedProperty viewedProperty=this.getViewedProperty();
		
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}
	
	@Test
	public void isUserIdNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEVIEWEDPROPERTY.no_user_id");
		
		saltString ="aaa";
		ViewedProperty viewedProperty=this.getViewedProperty();
		viewedProperty.setViewedUserId(null);
		
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}
	
	@Test
	public void isNoUserExists() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_user");
		
		saltString ="aaa";
		ViewedProperty viewedProperty=this.getViewedProperty();
		
		Mockito.when(userRepository.findUserById(viewedProperty.getViewedUserId(), saltString)).thenReturn(null);
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}
	
	@Test
	public void isPropertyIdNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEVIEWEDPROPERTY.no_property_id");
		
		ViewedProperty viewedProperty=this.getViewedProperty();
		viewedProperty.setViewedPropertyPropertyId(null);
		UsersEntity userEntity = new UsersEntity();
		
		
		Mockito.when(userRepository.findUserById(viewedProperty.getViewedUserId(), saltString)).thenReturn(userEntity);
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}
	
	@Test
	public void isPropertyNotExist() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.no_property");
		AddPropertyValidatorTest addPropertyValidatorTest = new AddPropertyValidatorTest();
		ViewedProperty viewedProperty=this.getViewedProperty();
		
		Property property = addPropertyValidatorTest.getAddPropertyUser();
		property.setPropertyId(BigInteger.valueOf(1l));
		viewedProperty.setViewedPropertyPropertyId(property);
		UsersEntity userEntity = new UsersEntity();
		
		Mockito.when(userRepository.findUserById(viewedProperty.getViewedUserId(), saltString)).thenReturn(userEntity);
		Mockito.when(propertyRepository.getPropertyForViewedProperty(property.getPropertyId())).thenReturn(null);
		
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
		
	}
	
	
	//getPropertiesByUser
	
	@Test
	public void isSaltStringNullGetProperties() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.no_saltstring");
		
		saltString =null;
		ViewedProperty viewedProperty=this.getViewedProperty();
		
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}
	
	@Test
	public void isUserIdNullGetProperties() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEVIEWEDPROPERTY.no_user_id");
		
		saltString ="aaa";
		ViewedProperty viewedProperty=this.getViewedProperty();
		viewedProperty.setViewedUserId(null);
		
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}
	
	@Test
	public void isNoUserExistsGetProperties() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_user");
		
		saltString ="aaa";
		ViewedProperty viewedProperty=this.getViewedProperty();
		
		Mockito.when(userRepository.findUserById(viewedProperty.getViewedUserId(), saltString)).thenReturn(null);
		viewedPropertyServiceImpl.createViewedProperty(viewedProperty, saltString);
	}

}
