package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Notification;
import com.project.terabit.repository.NotificationRepository;
import com.project.terabit.repository.UserRepository;

public class NotificationServicetTest extends TerabitApplicationTests{
	@Mock 
	UserRepository userrepository;
	
	@Mock 
	NotificationRepository notificationrepository;
	
	@InjectMocks
	NotificationServiceImpl notificationservice;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	String saltstring="aaaaaaaa";
	UUID uuid =UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb");
	
	public Notification getNotification() {
		
		Notification notification=new Notification();
		
		notification.setNotificationContent("solai");
		notification.setNotificationIsActive(true);
		notification.setNotificationNotifiedTime(null);
		notification.setNotificationTo(uuid);
		notification.setNotificationViewedTime(null);
		
		return notification;
	}
	
	public UsersEntity getUserEntity() throws Exception
	{
		UsersEntity usernotifiedentity=new UsersEntity();
		usernotifiedentity.setSaltString(saltstring);
		return usernotifiedentity;
	}
	
	public NotificationEntity getNotificationEntity(){
		
		NotificationEntity notificationEntity=new NotificationEntity();
		notificationEntity.setNotificationIsActive(true);
		return notificationEntity;
		
	}
	@Test
	public void isNotificationIsNullSave() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_notification_provided");
		
		Notification notificationtobesaved=null;
		
		notificationservice.saveNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isSaltStringIsNullSave() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_saltstring");
		
		Notification notificationtobesaved=this.getNotification();
		saltstring=null;
		
		notificationservice.saveNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isContendIsNullSave() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_notification_content");
		
		Notification notificationtobesaved=this.getNotification();
		notificationtobesaved.setNotificationContent(null);
		
		notificationservice.saveNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isNotifiedToIsNullSave() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.notified_to_isempty");
		
		Notification notificationtobesaved=this.getNotification();
		notificationtobesaved.setNotificationTo(null);
		
		notificationservice.saveNotification(saltstring, notificationtobesaved);
		
	}
	
	
	@Test
	public void isNotifiedUserToIsNullSave() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.No_User_Exists");
		
		Notification notificationtobesaved=this.getNotification();
		
		
		Mockito.when(userrepository.findUserByUserId(notificationtobesaved.getNotificationTo())).thenReturn(null);
		
		notificationservice.saveNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isSaltStringMismatchSave() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.saltString_mismatch");
		
		Notification notificationtobesaved=this.getNotification();
		
		UsersEntity usernotifiedentity=this.getUserEntity();
		
		saltstring=saltstring+"a";
		
		Mockito.when(userrepository.findUserByUserId(notificationtobesaved.getNotificationTo())).thenReturn(usernotifiedentity);
		
		notificationservice.saveNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isNotifiedUserToIsNullDelete() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.No_User_Exists");
		
		Notification notificationtobesaved=this.getNotification();
		
		
		Mockito.when(userrepository.findUserByUserId(notificationtobesaved.getNotificationTo())).thenReturn(null);
		
		notificationservice.deleteNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isSaltStringMismatchDelete() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.saltString_mismatch");
		
		Notification notificationtobesaved=this.getNotification();
		
		UsersEntity usernotifiedentity=this.getUserEntity();
		
		saltstring=saltstring+"a";
		
		Mockito.when(userrepository.findUserByUserId(notificationtobesaved.getNotificationTo())).thenReturn(usernotifiedentity);
		
		notificationservice.deleteNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isnotificationToBeDeletedIsNotActiveDelete() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.NOT_AN_ACTIVE_NOTIFICATION");
		
		Notification notificationtobesaved=this.getNotification();
		notificationtobesaved.setNotificationId(BigInteger.valueOf(1));
		
		UsersEntity usernotifiedentity=this.getUserEntity();
		usernotifiedentity.setUserId(UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb"));
		
		NotificationEntity notificationEntity=this.getNotificationEntity();
		notificationEntity.setNotificationIsActive(false);
		
		Mockito.when(userrepository.findUserByUserId(notificationtobesaved.getNotificationTo())).thenReturn(usernotifiedentity);
		Mockito.when(notificationrepository.findNotificationByNotificationId(notificationtobesaved.getNotificationId(),usernotifiedentity.getUserId())).thenReturn(notificationEntity);
		
		notificationservice.deleteNotification(saltstring, notificationtobesaved);
		
	}
	
	
	@Test
	public void isNotifiedToIsNullDelete() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.notified_to_isempty");
		
		Notification notificationtobesaved=this.getNotification();
		notificationtobesaved.setNotificationTo(null);
		
		notificationservice.deleteNotification(saltstring, notificationtobesaved);
		
	}
	
	
	
	
	@Test
	public void isNotificationIsNullDelete() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_notification_provided");
		
		Notification notificationtobesaved=null;
		
		notificationservice.deleteNotification(saltstring, notificationtobesaved);
		
	}
	
	@Test
	public void isSaltStringIsNullDelete() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_saltstring");
		
		Notification notificationtobesaved=this.getNotification();
		saltstring=null;
		
		notificationservice.deleteNotification(saltstring, notificationtobesaved);
		
	}
	
	
	
	
	
	
	
	@Test
	public void isNotificationIsNullGet() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_user_id");
		
		uuid=null;
		
		notificationservice.getNotification(saltstring, uuid);
		
	}
	
	@Test
	public void isSaltStringIsNullGet() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.no_saltstring");
		
		Notification notificationtobesaved=this.getNotification();
		saltstring=null;
		
		notificationservice.getNotification(saltstring, uuid);
		
	}
	@Test
	public void isNotifiedUserToIsNullGet() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.No_User_Exists");
		
		Mockito.when(userrepository.findUserByUserId(uuid)).thenReturn(null);
		
		notificationservice.getNotification(saltstring, uuid);
		
	}
	
	@Test
	public void isSaltStringMismatchGet() throws Exception {
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("NOTIFICATIONSERVICE.saltString_mismatch");
		
		
		UsersEntity usernotifiedentity=this.getUserEntity();
		
		saltstring="a";
		
		Mockito.when(userrepository.findUserByUserId(uuid)).thenReturn(usernotifiedentity);
		
		notificationservice.getNotification(saltstring, uuid);
		
	}
	
	

}
