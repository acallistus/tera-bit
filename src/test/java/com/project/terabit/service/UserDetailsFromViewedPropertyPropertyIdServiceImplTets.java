package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Image;
import com.project.terabit.model.UserDetail;
import com.project.terabit.repository.UserRepository;

public class UserDetailsFromViewedPropertyPropertyIdServiceImplTets extends TerabitApplicationTests {

	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserDetailsFromViewedPropertyPropertyIdServiceImpl userDetailsFromViewedPropertyPropertyIdServiceImpl;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	String saltString ="aaaaaaaaa";
	
	public UserDetail getUserUserDetail() {
		
		UserDetail userDetail =new UserDetail();
		userDetail.setIncommingPropertyId(BigInteger.valueOf(1l));
		userDetail.setIncommingSellerId(BigInteger.valueOf(1l));
		userDetail.setUserContactNo(BigInteger.valueOf(1234567890l));
		userDetail.setUserEmailId("solaiganesh@gmail.com");
		userDetail.setUserFirstName("solai");
		Image image=new Image();
		userDetail.setUserImage(image);
		userDetail.setUserIsActive(true);
		userDetail.setUserLastName("ganesh");
		return userDetail;
		
	}
	
	public SellerEntity getSeller() {
		SellerEntity seller=new SellerEntity();
		seller.setSellerId(BigInteger.valueOf(1l));
		seller.setSellerCompanyName("Kandu Mozhi");
		seller.setSellerIsActive(true);
		seller.setSellerPrivateContact(BigInteger.valueOf(1234567890l));
		seller.setSellerPropertyId(this.getProperty());
		
		return seller;
		
		
		
	}
	public List<PropertyEntity> getProperty()
	{
		List<PropertyEntity> propertyEntityList=new ArrayList<>();
		PropertyEntity addPropertyUserEntity=new PropertyEntity();
		
		addPropertyUserEntity.setPropertyCent(BigInteger.valueOf(1l));
		addPropertyUserEntity.setPropertyCity("MIT");
		addPropertyUserEntity.setPropertyCountry("India");
		addPropertyUserEntity.setPropertyDescription("good neart to urappakkam");
		addPropertyUserEntity.setPropertyEsteematedAmount("25");
		
		addPropertyUserEntity.setPropertyLandmark("VGP HOUse");
		addPropertyUserEntity.setPropertyLatitude("10");
		addPropertyUserEntity.setPropertyLongitude("01");
		addPropertyUserEntity.setPropertyOwnedBy(1);
		addPropertyUserEntity.setPropertyState("State");
		addPropertyUserEntity.setPropertyType("type");
		
		propertyEntityList.add(addPropertyUserEntity);
		
		return propertyEntityList;
		
		
	}
	
	
	public UsersEntity getUserEntity() {
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
		userEntity.setUserSellerId(this.getSeller());
		userEntity.setSaltString(saltString);
		userEntity.setIsuserAuthenticated(true);
		userEntity.setUserContactNo(BigInteger.valueOf(1234567890l));
		userEntity.setUserEmailId("solaiganesh@gmail.com");
		userEntity.setUserIsActive(true);
		userEntity.setUserVerifed(true);
		
		
		
	
		return userEntity;
		
		
	}
	
	@Test
	public void isSaltStringNullGetUserDetailsForPropertyId() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("getUserDetailsForPropertyId.saltstringmissing");
		
		saltString=null;
		
		UserDetail incomminguserdetail=this.getUserUserDetail();
		
		userDetailsFromViewedPropertyPropertyIdServiceImpl.getUserDetailsForPropertyId(saltString, incomminguserdetail);
	}
	
	@Test
	public void isPropertyIdNull() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("getUserDetailsForPropertyId.nopropertyid");
		
		UserDetail incomminguserdetail=this.getUserUserDetail();
		incomminguserdetail.setIncommingPropertyId(null);
		
		userDetailsFromViewedPropertyPropertyIdServiceImpl.getUserDetailsForPropertyId(saltString, incomminguserdetail);
		
	}
	
	@Test
	public void isSaltStringMissMatch() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("getUserDetailsForPropertyId.saltstringmissmatch");
		
		UserDetail incommingUserDetail=this.getUserUserDetail();
		
		Mockito.when(userRepository.saltstringComparisonForSellerId(incommingUserDetail.getIncommingSellerId())).thenReturn(saltString+"a");
		
		userDetailsFromViewedPropertyPropertyIdServiceImpl.getUserDetailsForPropertyId(saltString, incommingUserDetail);
	}
	@Test
	public void isUserListIsEmpty() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("getUserDetailsForPropertyId.nouserdetailsavailable");
		
		UserDetail incommingUserDetail=this.getUserUserDetail();
		
		List<UsersEntity> userList=new ArrayList<>();
		
		Mockito.when(userRepository.saltstringComparisonForSellerId(incommingUserDetail.getIncommingSellerId())).thenReturn(saltString);
		Mockito.when(userRepository.userDetailsForViewedPropertyPropertyId(incommingUserDetail.getIncommingPropertyId())).thenReturn(userList);
		
		userDetailsFromViewedPropertyPropertyIdServiceImpl.getUserDetailsForPropertyId(saltString, incommingUserDetail);
	}

}
