package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;


import org.mockito.Mockito;


import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.User;
import com.project.terabit.repository.UserRepository;


public class UpdateUserServiceTest extends TerabitApplicationTests {
	
	@Mock
	UserRepository userrepository;
	
	@InjectMocks
	SignUpUserServiceImpl signupservice;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	String salt_string="aaaaaaaa";
	UUID uuid = UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb");
	
	 public User getUser() {
		 User user = new User();
		 
	     user.setUserEmailId("lisadeepik@gmail.com");
	     user.setUserContactNo(BigInteger.valueOf(8681999437l));
	     user.setUserPassword("Deepik@lisa79");
	     user.setSaltstring(salt_string);
	     user.setUserId(uuid);
	     user.setUserFirstName("solai");
	     user.setUserLastName("ganesh");
	     
	     return user;
	 }
	 
	 public UsersEntity getUserEntity() {
		 UsersEntity userentity=new UsersEntity();
         userentity.setUserEmailId("lisadeepik@gmail.com");
         userentity.setUserPassword("-1165077272");
         userentity.setSaltString(salt_string);
         userentity.setUserIsActive(true);
         userentity.setUserId(uuid);
         return userentity;
	 }
	 
	 @Test
	 public void isUserExistSave() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPSERVICE.invalid_signUp");
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     
	     Mockito.when(userrepository.checkUser(user_to_be_logged_in.getUserEmailId(), user_to_be_logged_in.getUserContactNo())).thenReturn(user_to_be_logged_in_entity);
	     signupservice.saveUser(user_to_be_logged_in);
	  }
	 
	 @Test
	 public void isUserIdisNull() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.no_userid_provided");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserId(null);
	     
	     signupservice.updateUser(user_to_be_logged_in, salt_string);
	 }
	 @Test
	 public void isSaltStringIsNull() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.saltstring_not_provided");
	     
	     salt_string=null;
	     User user_to_be_logged_in=this.getUser();
	     
	     
	     signupservice.updateUser(user_to_be_logged_in, salt_string);
	 }
	 
	 @Test
	 public void isUserNotFoundSignUp() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.invalid_user");
	     
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(null);
	     
	     signupservice.updateUser(user_to_be_logged_in, salt_string);
	 }
	 
	 @Test
	 public void isSaltStringMismatchSignUp() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.invalid_saltstring");
	     
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	     
	     
	     signupservice.updateUser(user_to_be_logged_in, salt_string+"a");
	 }
	 
	 @Test
	 public void isMailExistSignUp() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATESERVICE.invalid_mailId");
	     
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	     
	     Mockito.when(userrepository.checkUserMailId(user_to_be_logged_in.getUserEmailId())).thenReturn(user_to_be_logged_in_entity);
	     
	     signupservice.updateUser(user_to_be_logged_in, salt_string);
	 }
	 
	 @Test
	 public void isContactExistSignUp() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATESERVICE.invalid_contactNumber");
	     
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	     
	     Mockito.when(userrepository.checkUserContactNumber(user_to_be_logged_in.getUserContactNo())).thenReturn(user_to_be_logged_in_entity);
	     
	     signupservice.updateUser(user_to_be_logged_in, salt_string);
	 }
	 
	 @Test
	 public void isUserIdisNullDelete() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.no_userid_provided");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserId(null);
	     
	     signupservice.deleteUser( salt_string,user_to_be_logged_in);
	 }
	 @Test
	 public void isSaltStringIsNullDelete() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.saltstring_not_provided");
	     
	     salt_string=null;
	     User user_to_be_logged_in=this.getUser();
	     
	     
	     signupservice.deleteUser( salt_string,user_to_be_logged_in);
	 }
	 
	 @Test
	 public void isUserNotFoundDelete() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.invalid_user");
	     
	     
	     User user_to_be_logged_in=this.getUser();
	     
	    
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(null);
	     
	     signupservice.deleteUser( salt_string,user_to_be_logged_in);
	 }
	 
	 @Test
	 public void isSaltStringMismatchDelete() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UTILITYSERVICE.invalid_saltstring");
	     
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	     
	     
	     signupservice.deleteUser( salt_string+"a",user_to_be_logged_in);
	 }

	 @Test
	 public void isValidContactNumberStartWithAllNine() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserId(uuid);
	     user_to_be_logged_in.setUserContactNo(BigInteger.valueOf(9999999999l));
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     
	     
	     Mockito.when(userrepository.findUserByUserId(user_to_be_logged_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	   

	     signupservice.updateUser(user_to_be_logged_in, salt_string);
		 
	 }
	 
	 //SignUpValidator
	 @Test
	 public void isValidFirstNameInterger() throws Exception {
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_firstname");
	     
	     User user =this.getUser();
	     user.setUserFirstName("sol5gb");
	     
	     signupservice.saveUser(user);
	     
		 
	 }
	 @Test
	 public void isValidFirstNameSpecialChar() throws Exception {
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_firstname");
	     
	     User user =this.getUser();
	     user.setUserFirstName("sol*gb");
	     
	     signupservice.saveUser(user);
	     
		 
	 }
	 @Test
	 public void isValidLastNameSpecialChar() throws Exception {
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_lastname");
	     
	     User user =this.getUser();
	     user.setUserLastName("sol*gb");
	     
	     signupservice.saveUser(user);
	     
		 
	 }
	 @Test
	 public void isValidLastNameInterger() throws Exception {
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_lastname");
	     
	     User user =this.getUser();
	     user.setUserLastName("sol5gb");
	     
	     signupservice.saveUser(user);
	     
		 
	 }
	 
	 @Test
	 public void isValidEmailIdNormalString() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai");
	     
	     signupservice.saveUser(user_to_be_saved_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutSC() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai.com");
	     
	     signupservice.saveUser(user_to_be_saved_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutDot() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai@gmailcom");
	     
	     signupservice.saveUser(user_to_be_saved_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithExtraLengthAfterDot() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai@gmail.comin");
	     
	     signupservice.saveUser(user_to_be_saved_in);
	     
	 }
	 
	 @Test
	 public void isValidContactNumberLessLength() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943l));
	     
	     signupservice.saveUser(user_to_be_saved_in);
	     
		 
	 }
	 
	 @Test
	 public void isValidContactNumberMoreLength() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943333l));
	     
	     signupservice.saveUser(user_to_be_saved_in);
	     
		 
	 }
	 
	 //Update Validate
	 @Test
	 public void isValidEmailIdNormalStringUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai");
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     Mockito.when(userrepository.findUserByUserId(user_to_be_saved_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	    
	     signupservice.updateUser(user_to_be_saved_in, salt_string);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutSCUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai.com");
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     Mockito.when(userrepository.findUserByUserId(user_to_be_saved_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	    
	     signupservice.updateUser(user_to_be_saved_in, salt_string);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutDotUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai@gmailcom");
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     Mockito.when(userrepository.findUserByUserId(user_to_be_saved_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	    
	     signupservice.updateUser(user_to_be_saved_in, salt_string);
	     
	 }
	 @Test
	 public void isValidEmailIdWithExtraLengthAfterDotUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai@gmail.comin");
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     Mockito.when(userrepository.findUserByUserId(user_to_be_saved_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	    
	     signupservice.updateUser(user_to_be_saved_in, salt_string);
	     
	 }
	 
	 @Test
	 public void isValidContactNumberLessLengthUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943l));
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     Mockito.when(userrepository.findUserByUserId(user_to_be_saved_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	     signupservice.updateUser(user_to_be_saved_in, salt_string);
	     
		 
	 }
	 
	 @Test
	 public void isValidContactNumberMoreLengthUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943333l));
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     Mockito.when(userrepository.findUserByUserId(user_to_be_saved_in.getUserId())).thenReturn(user_to_be_logged_in_entity);
	    
	     signupservice.updateUser(user_to_be_saved_in, salt_string);
	     
		 
	 }
	
	 
}

