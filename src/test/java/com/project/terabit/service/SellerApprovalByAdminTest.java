package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.AdminEntity;

import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Admin;
import com.project.terabit.model.Seller;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;

public class SellerApprovalByAdminTest  extends TerabitApplicationTests{

	@Mock
	UserRepository userRepository;
	
	@Mock
	AdminRepository adminRepository;
	
	@Mock
	SellerRepository sellerRepository;
	
	@InjectMocks
	SellerApprovalServiceImpl sellerApprovalService;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void isSaltStringNullCreateSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.no_saltstring");
		
		String saltstring=null;
		Seller seller = new Seller();
		
		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	@Test
	public void isSellerIdNullCreateSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.no_seller_id");
		
		String saltstring="aaaa";
		Seller seller = new Seller();
		
		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	@Test
	public void isSellerNullCreateSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_seller_id");
		String saltstring = "aaa";
		Seller seller = new Seller();
		seller.setSellerId(BigInteger.valueOf(1l));
		
		Mockito.when(sellerRepository.getSellerBySellerId(seller.getSellerId())).thenReturn(null);
		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	@Test
	public void isUserNullCreateSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.No_User_Exists");
		String saltstring = "aaa";
		Seller seller = new Seller();
		seller.setSellerId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1l));
		
		Mockito.when(sellerRepository.getSellerBySellerId(seller.getSellerId())).thenReturn(sellerEntity);
		Mockito.when(userRepository.findUserFromSellerId(sellerEntity)).thenReturn(null);

		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	
	@Test
	public void isAlreadyApprovedSellerCreateSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.seller_already_approved");

		String saltstring = "aaa";
		Seller seller = new Seller();
		seller.setSellerId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setSaltString("aaa");
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		SellerEntity checkSellerApproved = new SellerEntity();
		
		Mockito.when(sellerRepository.getSellerBySellerId(seller.getSellerId())).thenReturn(sellerEntity);
		Mockito.when(userRepository.findUserFromSellerId(sellerEntity)).thenReturn(userEntity);
		Mockito.when(sellerRepository.checkSellerAlreadyApproved(seller.getSellerId())).thenReturn(checkSellerApproved);


		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	@Test
	public void isInvalidAdminIdCreateSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_admin_id");
		String saltstring = "aaa";
		Seller seller = new Seller();
		seller.setSellerId(BigInteger.valueOf(1l));
		seller.setSellerRightBy(BigInteger.valueOf(1l));

		SellerEntity sellerEntity = new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setSaltString("aaa");
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		
		
		Mockito.when(sellerRepository.getSellerBySellerId(seller.getSellerId())).thenReturn(sellerEntity);
		Mockito.when(userRepository.findUserFromSellerId(sellerEntity)).thenReturn(userEntity);
		Mockito.when(sellerRepository.checkSellerAlreadyApproved(seller.getSellerId())).thenReturn(null);
		Mockito.when(adminRepository.getAdminByAdminId(seller.getSellerRightBy())).thenReturn(null);


		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	@Test
	public void isSaltstringMissmatchCreateSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_saltstring");
		String saltstring = "aaa";
		Seller seller = new Seller();
		seller.setSellerId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		seller.setSellerRightBy(BigInteger.valueOf(1l));
		sellerEntity.setSellerId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setSaltString("aaa1");
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		AdminEntity adminEntity = new AdminEntity();

		
		Mockito.when(sellerRepository.getSellerBySellerId(seller.getSellerId())).thenReturn(sellerEntity);
		Mockito.when(userRepository.findUserFromSellerId(sellerEntity)).thenReturn(userEntity);
		Mockito.when(sellerRepository.checkSellerAlreadyApproved(seller.getSellerId())).thenReturn(null);
		Mockito.when(adminRepository.getAdminByAdminId(seller.getSellerRightBy())).thenReturn(adminEntity);
		Mockito.when(userRepository.findUserByAdminId(adminEntity)).thenReturn(userEntity);


		sellerApprovalService.createSellerByAdmin(saltstring, seller);
	}
	
	@Test
	public void isSaltStringNullGetSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.no_saltstring");
		
		String saltstring=null;
		Admin admin = new Admin();
		
		sellerApprovalService.getSellerByAdmin(saltstring, admin);
	}
	@Test
	public void isUserNullGetSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.no_user_for_admin");
		
		String saltstring="aaa";
		Admin admin = new Admin();
		admin.setAdminId(BigInteger.valueOf(1l));
		
		Mockito.when(adminRepository.getUserByAdminId(admin.getAdminId())).thenReturn(null);
		sellerApprovalService.getSellerByAdmin(saltstring, admin);
	}
	
	
	
	@Test
	public void isSaltstringMissmatchGetSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_saltstring");
		String saltstring = "aaa";
		Admin admin = new Admin();
		admin.setAdminId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		userEntity.setSaltString("aaa1");
		String userId = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		
		Mockito.when(adminRepository.getUserByAdminId(admin.getAdminId())).thenReturn(userId);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(userId))).thenReturn(userEntity);
		sellerApprovalService.getSellerByAdmin(saltstring, admin);
	}
	
	@Test
	public void isSellerListNullGetSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_null_sellerlist");
		String saltstring = "aaa";
		Admin admin = new Admin();
		admin.setAdminId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		userEntity.setSaltString("aaa");
		String userId = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		Mockito.when(adminRepository.getUserByAdminId(admin.getAdminId())).thenReturn(userId);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(userId))).thenReturn(userEntity);

		Mockito.when(sellerRepository.getSellerByAdmin(admin.getAdminId())).thenReturn(null);

		sellerApprovalService.getSellerByAdmin(saltstring, admin);
	}
	
	@Test
	public void isSaltStringNullDeleteSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.no_saltstring");
		
		String saltstring=null;
		Admin admin = new Admin();
		
		sellerApprovalService.deleteSellerByAdmin(saltstring, admin);
	}
	
	@Test
	public void isUserNullDeleteSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.no_user_for_admin");
		
		String saltstring="aaa";
		Admin admin = new Admin();
		admin.setAdminId(BigInteger.valueOf(1l));
		
		Mockito.when(adminRepository.getUserByAdminId(admin.getAdminId())).thenReturn(null);
		sellerApprovalService.deleteSellerByAdmin(saltstring, admin);
	}
	
	@Test
	public void isSaltstringMissmatchDeleteSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_saltstring");
		String saltstring = "aaa";
		Admin admin = new Admin();
		admin.setAdminId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		userEntity.setSaltString("aaa1");
		String userId = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		
		Mockito.when(adminRepository.getUserByAdminId(admin.getAdminId())).thenReturn(userId);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(userId))).thenReturn(userEntity);
		sellerApprovalService.deleteSellerByAdmin(saltstring, admin);
	}
	
	@Test
	public void isInvalidSellerIdDeleteSeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERAPPROVALSERVICE.invalid_seller_id");
		String saltstring = "aaa";
		Admin admin = new Admin();
		admin.setAdminId(BigInteger.valueOf(1l));
		admin.setSellerId(BigInteger.valueOf(1l));

		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449"));
		userEntity.setSaltString("aaa");
		String userId = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		
		Mockito.when(adminRepository.getUserByAdminId(admin.getAdminId())).thenReturn(userId);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(userId))).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(admin.getSellerId())).thenReturn(null);
		sellerApprovalService.deleteSellerByAdmin(saltstring, admin);
	}
	
	
}
