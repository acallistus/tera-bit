package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Image;
import com.project.terabit.repository.ImageRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;

public class ImageServiceTest extends TerabitApplicationTests{
	@Mock
	ImageRepository filterRepository;
	
	@Mock 
	UserRepository userRepository;
	
	@Mock 
	SellerRepository sellerRepository;
	
	@Mock 
	PropertyRepository propertyRepository;
	
	@Mock 
	ImageRepository imageRepository;
	
	@InjectMocks
	ImageServiceImpl service;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void isSaltstringNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.saltString_null");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setUserId(UUID.fromString(uuid));
		String saltstring = null;
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isImagePathNotExists() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_image_path");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImagePath("imagepath");
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(imageEntity);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isNoUserIdPassed() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.no_userid");
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(null);
		String saltstring = "aaa";
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isUserIdNotExists() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_user");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isImageAlreadyExists() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEIMAGESERVICE.invalid_image");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(null);
		image.setPropertyId(null);
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImageIsActive(true);
		userEntity.setUserImage(imageEntity);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isSellerIdNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_seller");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserIsSeller(true);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isSellerFalse() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_seller");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(false);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isSellerNotAuthorized() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.seller_not_authorised");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(true);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(null);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void invalidInput() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_input");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(true);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(sellerEntity);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void imageAlreadyExists() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEIMAGESERVICE.invalid_image");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserImage(null);
		SellerEntity sellerEntity = new SellerEntity();
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImageIsActive(true);
		sellerEntity.setSellerCompanyLogoId(imageEntity);
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(true);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(sellerEntity);
		service.createImage(saltstring, image);
	}
	
	@Test
	public void invalidProperty() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_property");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(null);
		image.setPropertyId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		SellerEntity sellerEntity = new SellerEntity();
		ImageEntity imageEntity = new ImageEntity();
		sellerEntity.setSellerCompanyLogoId(imageEntity);
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(true);
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(propertyRepository.getPropertyByPropertyId(image.getPropertyId())).thenReturn(null);
		
		
		service.createImage(saltstring, image);
	}
	
	@Test
	public void isNullSaltstring() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.saltString_null");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setUserId(UUID.fromString(uuid));
		String saltstring = null;
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isInvalidImage() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_image_id");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("imagepath");
		String saltstring = "aaa";
		image.setImageId(BigInteger.valueOf(1l));
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isInvalidUserId() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.no_userid");
		Image image = new Image();
		image.setImagePath("imagepath");
		image.setImageId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImagePath("imagepath");
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isUserNotExist() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_user");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setUserId(UUID.fromString(uuid));
		image.setImagePath("imagepath");
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isDeleteInvalidInput() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_input");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setUserId(UUID.fromString(uuid));
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		ImageEntity imageEntity = new ImageEntity();
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(true);
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(sellerEntity);
		service.deleteImage(saltstring, image);
	}
	@Test
	public void isDeleteInvalidSellerId() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.seller_not_authorised");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImageId(BigInteger.valueOf(1l));
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImageId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(false);
		
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(null);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isDeleteSellerFalse() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_seller");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImageId(BigInteger.valueOf(1l));
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImageId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserIsSeller(false);
		
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(sellerEntity);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isDeleteUserSellerNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_seller");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImageId(BigInteger.valueOf(1l));
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		image.setPropertyId(null);
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImageId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(null);
		userEntity.setUserIsSeller(true);
		
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(sellerEntity);
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isDeleteInvalidPropertyId() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_property");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImageId(BigInteger.valueOf(1l));
		image.setImagePath("imagepath");
		image.setUserId(UUID.fromString(uuid));
		image.setPropertyId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		imageEntity.setImageId(BigInteger.valueOf(1l));
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		SellerEntity sellerEntity = new SellerEntity();
		userEntity.setUserSellerId(null);
		userEntity.setUserIsSeller(true);
		sellerEntity.setSellerCompanyLogoId(imageEntity);
		sellerEntity.getSellerCompanyLogoId().setImageId(BigInteger.valueOf(1l));
		
		
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(propertyRepository.getPropertyByPropertyId(image.getPropertyId())).thenReturn(null);
		
		service.deleteImage(saltstring, image);
	}
	
	@Test
	public void isUpdateNullSaltstring() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.saltString_null");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setUserId(UUID.fromString(uuid));
		String saltstring = null;
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdatePropertyException() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UPDATEIMAGESERVICE.invalid_property_id");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setPropertyId(BigInteger.valueOf(1l));
		image.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdateInvalidImagePath() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_image_path");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("path");
		image.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdateInvalidImageId() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_image_id");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("path");
		image.setImageId(BigInteger.valueOf(1l));
		image.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(null);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdateInvalidUserId() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_user");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("path");
		image.setImageId(BigInteger.valueOf(1l));
		image.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdateSellerFalse() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_seller");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("path");
		image.setImageId(BigInteger.valueOf(1l));
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		SellerEntity sellerEntity = new SellerEntity();
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserIsSeller(false);
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdateUserNotSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.invalid_seller");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("path");
		image.setImageId(BigInteger.valueOf(1l));
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserIsSeller(true);
		userEntity.setUserSellerId(null);
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		service.updateImage(saltstring, image);
	}
	
	@Test
	public void isUpdateNoSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("IMAGESERVICE.seller_not_authorised");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Image image = new Image();
		image.setImagePath("path");
		image.setImageId(BigInteger.valueOf(1l));
		image.setUserId(UUID.fromString(uuid));
		image.setSellerId(BigInteger.valueOf(1l));
		String saltstring = "aaa";
		ImageEntity imageEntity = new ImageEntity();
		SellerEntity sellerEntity = new SellerEntity();
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserIsSeller(true);
		userEntity.setUserSellerId(sellerEntity);
		userEntity.setUserImage(imageEntity);
		userEntity.getUserImage().setImageId(BigInteger.valueOf(1l));
		
		Mockito.when(imageRepository.getImageByPath(image.getImagePath())).thenReturn(null);
		Mockito.when(imageRepository.getImageByImageId(image.getImageId())).thenReturn(imageEntity);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(image.getSellerId())).thenReturn(null);
		service.updateImage(saltstring, image);
	}
}
