package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Cart;
import com.project.terabit.model.SaveCart;
import com.project.terabit.model.User;
import com.project.terabit.repository.CartRepository;
import com.project.terabit.repository.UserRepository;

public class CartServiceTest extends TerabitApplicationTests{
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	CartRepository cartRepository;
	
	@InjectMocks
	CartServiceImpl cartServiceImpl;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void isSaltStringNullSave() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.no_saltstring");
		
		String saltString=null;
		SaveCart cartToBeSaved=new SaveCart();
		
		cartServiceImpl.saveCart(saltString, cartToBeSaved);
	}
	@Test
	public void isSaltStringNullDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.no_saltstring");
		
		SaveCart userCart=new SaveCart();
		String saltString=null;
		
		cartServiceImpl.deleteCart(saltString,userCart);
	}
	@Test
	public void isSaltStringNullGet() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.no_saltstring");
		
		String saltString=null;
		User userCart=new User();
		userCart.setUserId(null);
		
		cartServiceImpl.getCart(saltString, userCart.getUserId());
	}
	
	@Test
	public void isSaveCartIsNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.no_Cart");
		
		SaveCart cartToBeSaved=null;
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.saveCart(saltTSring, cartToBeSaved);
	}
	@Test
	public void isUserIsNullDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("Cart.No_User_Id");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(null);
		
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.deleteCart(saltTSring,userCart);
	}
	
	@Test
	public void isUserIsNullGet() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.no_Cart");
		
		User userCart=new User();
		userCart.setUserId(null);
		
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.getCart(saltTSring, userCart.getUserId());
	}
	
	@Test
	public void isUserIdNullSave() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("Cart.No_User_Id");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(null);
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.saveCart(saltTSring, userCart);
	}
	
	@Test
	public void isUserIdIsNulldelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("Cart.No_User_Id");
		
		SaveCart userCart=new SaveCart();
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.deleteCart(saltTSring, userCart);
	}
	
	@Test
	public void isUserIdIsNullGet() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.no_Cart");
		
		User userCart=new User();
		userCart.setUserId(null);
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.getCart(saltTSring, userCart.getUserId());
	}
	
	@Test
	public void isUserPropertyIdNullSave() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SAVECART.No_Property_Id");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		String saltTSring="aaaaaaaaaaa";
		
		cartServiceImpl.saveCart(saltTSring, userCart);
	}
	
	@Test
	public void IsUserNotExistSave() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.NO_USER_EXIST");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userCart.setCartPropertyId(BigInteger.valueOf(1));
		String saltTSring="aaaaaaaaaaa";
		
		Mockito.when(userRepository.findUserByUserId(userCart.getCartUserId())).thenReturn(null);
		cartServiceImpl.saveCart(saltTSring, userCart);
	}
	
	@Test
	public void IsUserNotExistDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.NO_USER_EXIST");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		
		List<Cart> cartList=new ArrayList<>();
		Cart cart =new Cart();
		cartList.add(cart);
		User user = new User();
		user.setUserCarts(cartList);
		
		String saltTSring="aaaaaaaaaaa";
		
		
		Mockito.when(userRepository.findUserByUserId(user.getUserId())).thenReturn(null);
		cartServiceImpl.deleteCart(saltTSring, userCart);
	}
	@Test
	public void IsUserNotExistGet() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.NO_USER_EXIST");
		
		User userCart=new User();
		userCart.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		String saltTSring="aaaaaaaaaaa";
		
		Mockito.when(userRepository.findUserByUserId(userCart.getUserId())).thenReturn(null);
		cartServiceImpl.getCart(saltTSring, userCart.getUserId());
	}
	
	
	@Test
	public void IsSaltStringMissMatchSave() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.SALTSTRING_MISSMATCH");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userCart.setCartPropertyId(BigInteger.valueOf(1));
		String saltTSring="aaaaaaaaaaa";
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userEntity.setSaltString("bbbbbbbbb");
		
		Mockito.when(userRepository.findUserByUserId(userCart.getCartUserId())).thenReturn(userEntity);
		cartServiceImpl.saveCart(saltTSring, userCart);
	}
	
	@Test
	public void IsSaltStringMissMatchDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.SALTSTRING_MISSMATCH");
		
		User user=new User();
		SaveCart userCart = new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		user.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		List<Cart> cartList=new ArrayList<>();
		Cart cart =new Cart();
		cartList.add(cart);
		
		user.setUserCarts(cartList);
		
		String saltTSring="aaaaaaaaaaa";
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userEntity.setSaltString("bbbbbbbbb");
		
		
		Mockito.when(userRepository.findUserByUserId(userCart.getCartUserId())).thenReturn(userEntity);
		cartServiceImpl.deleteCart(saltTSring, userCart);
	}
	@Test
	public void isSaltStringMissMatchGet() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.SALTSTRING_MISSMATCH");
		
		User userCart=new User();
		userCart.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		String saltTSring="aaaaaaaaaaa";
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userEntity.setSaltString("bbbbbbbbb");
		
		Mockito.when(userRepository.findUserByUserId(userCart.getUserId())).thenReturn(userEntity);
		cartServiceImpl.getCart(saltTSring, userCart.getUserId());
	}
	
	
	@Test
	public void isCartListIsNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.USER_CART_IS_NULL");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userCart.setCartPropertyId(BigInteger.valueOf(1));
		
		String saltTSring="aaaaaaaaaaa";
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userEntity.setUserCarts(null);
		userEntity.setSaltString(saltTSring);
		
		Mockito.when(userRepository.findUserByUserId(userCart.getCartUserId())).thenReturn(userEntity);
		cartServiceImpl.saveCart(saltTSring, userCart);
	}
	
	
	@Test
	public void isCartListIsEmptyDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.USER_CART_IS_EMPTY");
		
		SaveCart userCart=new SaveCart();
		userCart.setCartUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		
		String saltstring="aaaaaaaaaaa";
		
		List<CartEntity> cartEntityList=new ArrayList<>();
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userEntity.setSaltString(saltstring);
		userEntity.setUserCarts(cartEntityList);
		
		Mockito.when(userRepository.findUserByUserId(userCart.getCartUserId())).thenReturn(userEntity);
		cartServiceImpl.deleteCart(saltstring, userCart);
	}
	@Test
	public void isCartListIsEmptyGet() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CART.USER_CART_IS_EMPTY");
		
		User userCart=new User();
		userCart.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		
		String saltTSring="aaaaaaaaaaa";
		
		List<CartEntity> cartEntityList=new ArrayList<>();
		
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("74319495-788b-4daf-befc-add07cadd910"));
		userEntity.setUserCarts(cartEntityList);
		userEntity.setSaltString(saltTSring);
		
		Mockito.when(userRepository.findUserByUserId(userCart.getUserId())).thenReturn(userEntity);
		cartServiceImpl.getCart(saltTSring, userCart.getUserId());

	}
}
