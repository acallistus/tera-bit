package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.CreateSeller;
import com.project.terabit.model.SaveSeller;
import com.project.terabit.model.User;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;

public class SellerServiceTest extends TerabitApplicationTests{
	
	@Mock 
	UserRepository userRepository;
	
	@Mock 
	SellerRepository sellerRepository;
	
	@Mock 
	AdminRepository adminRepository;
	
	@InjectMocks
	SellerServiceImpl service;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void createSellerInvalidUserTest() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.No_User_Exists");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		String saltstring = "aaaaaaaa";
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		

		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(adminEntity);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(uuid))).thenReturn(null);
		
		service.createSeller(saltstring, createseller);
	}
	
	@Test
	public void isSaltStringIsNullCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERSERVICE.no_saltstring");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		
		String saltstring = null;
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		
		service.createSeller(saltstring, createseller);
		
	}
	
	
	@Test
	public void isUserIdNullCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.invalid_user_id");
		
		String saltstring = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(null);
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		
		service.createSeller(saltstring, createseller);
	}
	
	@Test
	public void isAdminIdNullCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.no_admin_id");
		
		String saltstring = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb"));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		
		service.createSeller(saltstring, createseller);
	}
	
	@Test
	public void isAdminNullCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.no_admin");
		
		String saltstring = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb"));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(null);
		service.createSeller(saltstring, createseller);
	}
	
	
	@Test
	public void isUserNotExistCreate() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.No_User_Exists");
		
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		
		String saltString = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		UsersEntity userEntitySellerCreate=new UsersEntity();
		userEntitySellerCreate.setUserId(UUID.fromString(uuid));
		userEntitySellerCreate.setSaltString(saltString);
		userEntitySellerCreate.setUserIsSeller(true);
		userEntitySellerCreate.setUserSellerId(sellerEntity);
		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(adminEntity);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(uuid))).thenReturn(null);
		
		service.createSeller(saltString, createseller);
	
	}
	
	@Test
	public void isUrlMissMatch() throws Exception {
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.invalid_saltstring");
		
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		
		String saltString = "aaaaaaaa";
		
		CreateSeller createSeller = new CreateSeller();
		createSeller.setUserId(UUID.fromString(uuid));
		createSeller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createSeller.setSellerRightBy(BigInteger.valueOf(1l));
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		UsersEntity userEntitySellerCreate=new UsersEntity();
		userEntitySellerCreate.setUserId(UUID.fromString(uuid));
		userEntitySellerCreate.setSaltString(saltString+"a");
		userEntitySellerCreate.setUserIsSeller(true);
		userEntitySellerCreate.setUserSellerId(sellerEntity);
		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createSeller.getSellerRightBy())).thenReturn(adminEntity);
		
		Mockito.when(userRepository.findActiveUsers(createSeller.getUserId())).thenReturn(userEntitySellerCreate);
		
		service.createSeller(saltString, createSeller);
	}
	
	@Test
	public void isUserIsSellerCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.Seller_already_exists");
		
		String uuid = "c38fe5d1-8efc-4d7e-9366-2a297839bc08";
		
		String saltString = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		UsersEntity userEntitySellerCreate=new UsersEntity();
		userEntitySellerCreate.setUserId(UUID.fromString(uuid));
		userEntitySellerCreate.setSaltString(saltString);
		userEntitySellerCreate.setUserIsSeller(false);
		userEntitySellerCreate.setUserSellerId(sellerEntity);
		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(adminEntity);
		
		Mockito.when(userRepository.findActiveUsers(UUID.fromString(uuid))).thenReturn(userEntitySellerCreate);
		
		service.createSeller(saltString, createseller);
	}
	
	@Test
	public void isUserIsActiveSellerCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.Seller_already_exists");
		
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		
		String saltString = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		UsersEntity userEntitySellerCreate=new UsersEntity();
		userEntitySellerCreate.setUserId(UUID.fromString(uuid));
		userEntitySellerCreate.setSaltString(saltString);
		userEntitySellerCreate.setUserIsSeller(true);
		userEntitySellerCreate.setUserSellerId(sellerEntity);
		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(adminEntity);
		
		Mockito.when(userRepository.findActiveUsers(UUID.fromString(uuid))).thenReturn(userEntitySellerCreate);
		
		service.createSeller(saltString, createseller);
	}
	
	@Test
	public void isUserSellerAlreadyExists() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.Seller_already_exists");
		
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		
		String saltString = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999959999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		UsersEntity userEntitySellerCreate=new UsersEntity();
		userEntitySellerCreate.setUserId(UUID.fromString(uuid));
		userEntitySellerCreate.setSaltString(saltString);
		userEntitySellerCreate.setUserIsSeller(true);
		userEntitySellerCreate.setUserSellerId(sellerEntity);
		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(adminEntity);
		
		Mockito.when(userRepository.findActiveUsers(UUID.fromString(uuid))).thenReturn(userEntitySellerCreate);
		Mockito.when(sellerRepository.findSellerByContactNumber(BigInteger.valueOf(9999999999L))).thenReturn(sellerEntity);
		
		service.createSeller(saltString, createseller);
		
	}
	
	
//	@Test
//	public void createSellerSuccessCreate() throws Exception{
//		String saltstring = null;
//		CreateSeller createseller = new CreateSeller();
//		String uuid = "8705b317-3bda-4ef7-abf7-2d951edc4ee7";
//		createseller.setUserId(UUID.fromString(uuid));
//		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
//		UsersEntity userEntity = new UsersEntity();
//		userEntity.setUserId(UUID.fromString(uuid));
//		Mockito.when(userRepository.findActiveUsers(createseller.getUserId())).thenReturn(userEntity);
//		service.createSeller(saltstring, createseller);
//		}
	
	@Test
	public void createSellerInvalidContactNo() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SELLERVALIDATOR.invalid_phone_no");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		String saltString = "aaaaaaaa";
		
		CreateSeller createseller = new CreateSeller();
		createseller.setUserId(UUID.fromString(uuid));
		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		createseller.setSellerRightBy(BigInteger.valueOf(1l));
		
		SellerEntity sellerEntity = new SellerEntity();
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
		
		UsersEntity userEntitySellerDelete=new UsersEntity();
		userEntitySellerDelete.setUserId(UUID.fromString(uuid));
		userEntitySellerDelete.setSaltString(saltString);
		userEntitySellerDelete.setUserIsSeller(false);
		userEntitySellerDelete.setUserSellerId(null);
		AdminEntity adminEntity = new AdminEntity();
		
		Mockito.when(adminRepository.getAdminByAdminId(createseller.getSellerRightBy())).thenReturn(adminEntity);
		
		Mockito.when(userRepository.findActiveUsers(createseller.getUserId())).thenReturn(userEntitySellerDelete);
		Mockito.when(sellerRepository.findSellerByContactNumber(BigInteger.valueOf(9999999999L))).thenReturn(sellerEntity);
		service.createSeller(saltString, createseller);
		}
	
	
	
	@Test
	public void isValidUserTestDelete() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.No_User_Exists");
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
		String saltString="aaaa";
		
		User user = new User();
		user.setUserId(UUID.fromString(uuid));
		
		
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		
		UsersEntity userEntitySellerDelete=new UsersEntity();
		userEntitySellerDelete.setUserId(UUID.fromString(uuid));
		userEntitySellerDelete.setSaltString(saltString);
		userEntitySellerDelete.setUserIsSeller(true);
		userEntitySellerDelete.setUserSellerId(sellerEntity);
		
		Mockito.when(userRepository.findActiveUsers(user.getUserId())).thenReturn(null);
		service.deleteSeller(user,saltString);
		
	}
	
	@Test
	public void isUrlMisMatchDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATESELLERSERVICE.invalid_saltstring");
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
		String saltString="aaaa";
		
		User user = new User();
		user.setUserId(UUID.fromString(uuid));
		
		
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		
		UsersEntity userEntitySellerDelete=new UsersEntity();
		userEntitySellerDelete.setUserId(UUID.fromString(uuid));
		userEntitySellerDelete.setSaltString(saltString);
		userEntitySellerDelete.setUserIsSeller(true);
		userEntitySellerDelete.setUserSellerId(sellerEntity);
		
		Mockito.when(userRepository.findActiveUsers(user.getUserId())).thenReturn(userEntitySellerDelete);
		service.deleteSeller(user,saltString+"a");
	}
	
	@Test
	public void isUserIsSellerTestDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("DELETESELLERSERVICE.User_is_not_seller");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
		String saltString="aaaa";
		
		User user = new User();
		user.setUserId(UUID.fromString(uuid));
		
		
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		
		UsersEntity userEntitySellerDelete=new UsersEntity();
		userEntitySellerDelete.setUserId(UUID.fromString(uuid));
		userEntitySellerDelete.setSaltString(saltString);
		userEntitySellerDelete.setUserIsSeller(false);
		userEntitySellerDelete.setUserSellerId(sellerEntity);
		
		
		Mockito.when(userRepository.findActiveUsers(user.getUserId())).thenReturn(userEntitySellerDelete);
		service.deleteSeller(user,saltString);
		
	}
	
	@Test
	public void isValidSellerTestDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("DELETESELLERSERVICE.Not_Active_seller");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
		String saltString="aaa";
		
		User user = new User();
		user.setUserId(UUID.fromString(uuid));
		
		
		
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(false);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		
		UsersEntity userEntitySellerDelete=new UsersEntity();
		userEntitySellerDelete.setUserId(UUID.fromString(uuid));
		userEntitySellerDelete.setSaltString(saltString);
		userEntitySellerDelete.setUserIsSeller(true);
		userEntitySellerDelete.setUserSellerId(sellerEntity);
		
		
		Mockito.when(userRepository.findActiveUsers(user.getUserId())).thenReturn(userEntitySellerDelete);
		service.deleteSeller(user,saltString);
		
	}
	
//	@Test
//	public void deleteSellerTestDelete() throws Exception
//	{
//		
//		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
//		String saltString="aaaa";
//		
//		User user = new User();
//		user.setUserId(UUID.fromString(uuid));
//		
//		
//		
//		SellerEntity sellerEntity =new SellerEntity();
//		sellerEntity.setSellerId(BigInteger.valueOf(1));
//		sellerEntity.setSellerIsActive(true);
//		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
//		
//		
//		UsersEntity userEntitySellerDelete=new UsersEntity();
//		userEntitySellerDelete.setUserId(UUID.fromString(uuid));
//		userEntitySellerDelete.setSaltString(saltString);
//		userEntitySellerDelete.setUserIsSeller(true);
//		userEntitySellerDelete.setUserSellerId(sellerEntity);
//		
//		
//		Mockito.when(userRepository.findActiveUsers(user.getUserId())).thenReturn(userEntitySellerDelete);
//		service.deleteSeller(user,saltString);
//	}
	
	@Test
	public void isSaltStringNullDelete() throws Exception{
		expectedException.expect(Exception.class);
        expectedException.expectMessage("SELLERSERVICE.no_saltstring");
        
        String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
     	String saltString=null;
     
     	User user = new User();
		user.setUserId(UUID.fromString(uuid));
     
		
		service.deleteSeller(user,saltString);
	}
	
	@Test
	public void isUserIdNullDelete() throws Exception{
		expectedException.expect(Exception.class);
        expectedException.expectMessage("CREATESELLERSERVICE.invalid_user_id");
        
      
     	String saltString="aaaaaaaa";
     
    	User user = new User();
		user.setUserId(null);
        
		SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		service.deleteSeller(user,saltString);
	}
	
	
	@Test
	public void isSaltStringNullUpdate() throws Exception{
		expectedException.expect(Exception.class);
        expectedException.expectMessage("SELLERSERVICE.no_saltstring");
        
        String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
     	String saltString=null;
     
        SaveSeller updateToSeller=new SaveSeller();
        updateToSeller.setUserId(UUID.fromString(uuid));
        updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(1234567890l));
     
        SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		service.updateSeller(updateToSeller,saltString);
	}
	
	@Test
	public void isUserIdNullUpdate() throws Exception{
		expectedException.expect(Exception.class);
        expectedException.expectMessage("CREATESELLERSERVICE.invalid_user_id");
        
     	String saltString="aaaaaaaa";
     
        SaveSeller updateToSeller=new SaveSeller();
        updateToSeller.setUserId(null);
        
        updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(1234567890l));
     
        SellerEntity sellerEntity =new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1));
		sellerEntity.setSellerIsActive(true);
		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
		
		service.updateSeller(updateToSeller,saltString);
	}
	@Test
	 public void isValidUserTestUpdate() throws Exception{
		    expectedException.expect(Exception.class);
	        expectedException.expectMessage("CREATESELLERSERVICE.No_User_Exists");
	     
	     	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
	     	String saltString="aaa";
	     
	        SaveSeller updateToSeller=new SaveSeller();
	        updateToSeller.setUserId(UUID.fromString(uuid));
	        updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(1234567890l));
	     
	        SellerEntity sellerEntity =new SellerEntity();
			sellerEntity.setSellerId(BigInteger.valueOf(1));
			sellerEntity.setSellerIsActive(true);
			sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
			
			
			UsersEntity userEntitySellerUpdate=new UsersEntity();
			userEntitySellerUpdate.setUserId(UUID.fromString(uuid));
			userEntitySellerUpdate.setUserIsSeller(true);
			userEntitySellerUpdate.setUserSellerId(sellerEntity);
	    
			Mockito.when(userRepository.findActiveUsers(updateToSeller.getUserId())).thenReturn(null);
        	service.updateSeller(updateToSeller,saltString);   
	 }
	 
	 @Test
	 public void isUrlMissMatchUpdate() throws Exception
	 {
		    expectedException.expect(Exception.class);
	        expectedException.expectMessage("CREATESELLERSERVICE.invalid_saltstring");
	     
	     	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
	     	String saltString="aaa";
	     
	     	SaveSeller updateToSeller=new SaveSeller();
	        updateToSeller.setUserId(UUID.fromString(uuid));
	        updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(1234567890l));
	     
	        SellerEntity sellerEntity =new SellerEntity();
			sellerEntity.setSellerId(BigInteger.valueOf(1));
			sellerEntity.setSellerIsActive(true);
			sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
			
			
			UsersEntity userEntitySellerUpdate=new UsersEntity();
			userEntitySellerUpdate.setUserId(UUID.fromString(uuid));
			userEntitySellerUpdate.setUserIsSeller(true);
			userEntitySellerUpdate.setUserSellerId(sellerEntity);
			userEntitySellerUpdate.setSaltString(saltString);
	    
			Mockito.when(userRepository.findActiveUsers(updateToSeller.getUserId())).thenReturn(userEntitySellerUpdate);
			service.updateSeller(updateToSeller,saltString+"a");
	 }
	 
	 @Test
	 public void IsUserNotSellerUpdate() throws Exception
	 {
		 	expectedException.expect(Exception.class);
	        expectedException.expectMessage("UPDATESELLERSERVICE.ServiceException_NotA_Seller");
	     
	     	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
	     	String saltString="aaa";
	     
	     	 SaveSeller updateToSeller=new SaveSeller();
		     updateToSeller.setUserId(UUID.fromString(uuid));
		     updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(1234567890l));
	     
	        SellerEntity sellerEntity =new SellerEntity();
			sellerEntity.setSellerId(BigInteger.valueOf(1));
			sellerEntity.setSellerIsActive(true);
			sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
			
			
			UsersEntity userEntitySellerUpdate=new UsersEntity();
			userEntitySellerUpdate.setUserId(UUID.fromString(uuid));
			userEntitySellerUpdate.setUserIsSeller(false);
			userEntitySellerUpdate.setUserSellerId(null);
			userEntitySellerUpdate.setSaltString(saltString);
	    
			Mockito.when(userRepository.findActiveUsers(updateToSeller.getUserId())).thenReturn(userEntitySellerUpdate);
			service.updateSeller(updateToSeller,saltString);
	 }
	 
	 @Test
	 public void isSeller() throws Exception
	 {

		 	expectedException.expect(Exception.class);
	        expectedException.expectMessage("UPDATESELLERSERVICE.ServiceException_UserIsNotSeller");
	     
	     	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
	     	String saltString="aaa";
	     
	     	SaveSeller updateToSeller=new SaveSeller();
		    updateToSeller.setUserId(UUID.fromString(uuid));
		    updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(1234567890l));
		     
	        SellerEntity sellerEntity =new SellerEntity();
			sellerEntity.setSellerId(BigInteger.valueOf(1));
			sellerEntity.setSellerIsActive(true);
			sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
			
			
			UsersEntity userEntitySellerUpdate=new UsersEntity();
			userEntitySellerUpdate.setUserId(UUID.fromString(uuid));
			userEntitySellerUpdate.setUserIsSeller(false);
			userEntitySellerUpdate.setUserSellerId(sellerEntity);
			userEntitySellerUpdate.setSaltString(saltString);
	    
			Mockito.when(userRepository.findActiveUsers(updateToSeller.getUserId())).thenReturn(userEntitySellerUpdate);
			service.updateSeller(updateToSeller,saltString);
	 }
	 
//	 @Test
//	 public void isUserSeller() throws Exception
//	 {
//		 expectedException.expect(Exception.class);
//	        expectedException.expectMessage("SERVICE.ServiceException_NotA_ActiveSeller");
//	     
//	     	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
//	     	String saltString="aaa";
//	     
//	     	SaveSeller updateToSeller=new SaveSeller();
//	        updateToSeller.setSellerUserId(UUID.fromString(uuid));
//	     
//	        SellerEntity sellerEntity =new SellerEntity();
//			sellerEntity.setSellerId(BigInteger.valueOf(1));
//			sellerEntity.setSellerIsActive(false);
//			sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
//			
//			
//			UserEntity userEntitySellerUpdate=new UserEntity();
//			userEntitySellerUpdate.setUserId(UUID.fromString(uuid));
//			userEntitySellerUpdate.setUserIsSeller(true);
//			userEntitySellerUpdate.setUserSellerId(sellerEntity);
//			userEntitySellerUpdate.setSaltString(saltString);
//	    
//			Mockito.when(sellerRepository.findUserByUserId(updateToSeller.getSellerUserId())).thenReturn(userEntitySellerUpdate);
//			updateSellerService.updateSeller(updateToSeller,saltString);
//		 
//	 }
	 
	
	
	 @Test
	 public void sellerUpdateSuccess() throws Exception
	 {
		
		 	
		 	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a445";
		 	String saltString="aaa";
	     
		 	SaveSeller updateToSeller=new SaveSeller();
		 	updateToSeller.setUserId(UUID.fromString(uuid));
		 	updateToSeller.setSellerPrivateContactNumber(BigInteger.valueOf(9234567890l));
	     
	     
	        SellerEntity sellerEntity =new SellerEntity();
			sellerEntity.setSellerId(BigInteger.valueOf(1));
			sellerEntity.setSellerIsActive(true);
			sellerEntity.setSellerPrivateContact(BigInteger.valueOf(5555555555l));
			
			
			UsersEntity userEntitySellerUpdate=new UsersEntity();
			userEntitySellerUpdate.setUserId(UUID.fromString(uuid));
			userEntitySellerUpdate.setUserIsSeller(true);
			userEntitySellerUpdate.setUserSellerId(sellerEntity);
			userEntitySellerUpdate.setSaltString(saltString);
			
			
			Mockito.when(userRepository.findActiveUsers(updateToSeller.getUserId())).thenReturn(userEntitySellerUpdate);
			service.updateSeller(updateToSeller,saltString);
	 }
	 
	
}
