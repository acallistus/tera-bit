package com.project.terabit.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.FilterProperty;
import com.project.terabit.repository.FilterRepository;
import com.project.terabit.repository.UserRepository;

public class FilterPropertyServiceTest extends TerabitApplicationTests{

	@Mock
	FilterRepository filterRepository;
	
	@Mock 
	UserRepository userRepository;
	
	@InjectMocks
	FilterServiceImpl service;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	
	@Test
	public void isSaltstringNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.no_saltstring");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		FilterProperty filterProperty = new FilterProperty();
		filterProperty.setUserId(UUID.fromString(uuid));
		String saltstring = null;
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.getPropertiesForTheFilter(filterProperty, saltstring);
	}
	
	@Test
	public void isSaltstringMissmatch() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.nouserid");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		FilterProperty filterProperty = new FilterProperty();
		filterProperty.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.getPropertiesForTheFilter(filterProperty, saltstring);
	}
	
	@Test
	public void isUserIdNotPresent() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.nouserid");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		FilterProperty filterProperty = new FilterProperty();
		filterProperty.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.getPropertiesForTheFilter(filterProperty, saltstring);
	}
	
	@Test
	public void invalidCity() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_city");
		List<String> city = new ArrayList<>();
		city.add("THENI1");
		city.add("CHENNAI");
		service.filterByCity(city);
	}
	
	@Test
	public void invalidCountry() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_country");
		List<String> country = new ArrayList<>();
		country.add("INDIA1");
		country.add("INDIA");
		service.filterByCountry(country);
	}
	
	@Test
	public void invalidPropertyType() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_property_type");
		List<String> propertyType = new ArrayList<>();
		propertyType.add("FLAT1");
		propertyType.add("FLAT");
		service.filterByPropertyType(propertyType);
	}
	@Test
	public void invalidState() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_state");
		List<String> state = new ArrayList<>();
		state.add("TAMIL NADU1");
		state.add("TAMIL NADU");
		service.filterByState(state);
	}
	@Test
	public void invalidSellerName() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_seller_name");
		List<String> sellerName = new ArrayList<>();
		sellerName.add("DEEPIK$");
		sellerName.add("DEEPIK");
		service.filterBySellerName(sellerName);
	}
	@Test
	public void invalidLandMark() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_landmark");
		List<String> landMark = new ArrayList<>();
		landMark.add("DEEPIK$");
		landMark.add("DEEPIK");
		service.filterByLandMark(landMark);
	}
	@Test
	public void invalidPrice() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_price");
		String fromPrice = "10s00";
		String toPrice = "30000";
		
		service.filterByPrice(fromPrice,toPrice);
	}
	
	@Test
	public void invalidArea() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.invalid_area");
		String fromArea = "10s00";
		String toArea = "30000";
		service.filterByArea(fromArea, toArea);
	}
	
	@Test
	public void EmptyPropertyList() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("FILTERSERVICE.no_property_for_filter");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		FilterProperty filterProperty = new FilterProperty();
		filterProperty.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		UsersEntity userEntity = new UsersEntity();
		userEntity.setUserId(UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb"));
		userEntity.setSaltString("aaa");
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(userEntity);
		filterProperty.setUserId(UUID.fromString(uuid));
		service.getPropertiesForTheFilter(filterProperty, saltstring);
	}
	

	
}
