package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.UUID;



/**
 * The Class Feedback.
 */
public class Feedback {

	/** The feedback id. */
	private BigInteger feedbackId;

	/** The feedback rating. */
	private int feedbackRating;

	/** The feedback description. */
	private String feedbackDescription;

	/** The feedback given by. */
	private UUID feedbackGivenBy;

	private LocalDateTime feedbackModifiedTime;

	/** The feedback created by. */
	private String feedbackCreatedBy;
	

	//for the use of saving feedback
	private BigInteger propertyId;
	
	private BigInteger sellerId;
	
	private BigInteger adminId;
	


	private String message;
	
	
	public BigInteger getAdminId() {
		return adminId;
	}

	public void setAdminId(BigInteger adminId) {
		this.adminId = adminId;
	}


	


	public BigInteger getSellerId() {
		return sellerId;
	}

	public void setSellerId(BigInteger sellerId) {
		this.sellerId = sellerId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the feedback id.
	 *
	 * @return the feedback id
	 */
	public BigInteger getFeedbackId() {
		return feedbackId;
	}

	/**
	 * Sets the feedback id.
	 *
	 * @param feedbackId the new feedback id
	 */
	public void setFeedbackId(BigInteger feedbackId) {
		this.feedbackId = feedbackId;
	}

	/**
	 * Gets the feedback rating.
	 *
	 * @return the feedback rating
	 */
	public int getFeedbackRating() {
		return feedbackRating;
	}

	/**
	 * Sets the feedback rating.
	 *
	 * @param feedbackRating the new feedback rating
	 */
	public void setFeedbackRating(int feedbackRating) {
		this.feedbackRating = feedbackRating;
	}

	/**
	 * Gets the feedback description.
	 *
	 * @return the feedback description
	 */
	public String getFeedbackDescription() {
		return feedbackDescription;
	}

	/**
	 * Sets the feedback description.
	 *
	 * @param feedbackDescription the new feedback description
	 */
	public void setFeedbackDescription(String feedbackDescription) {
		this.feedbackDescription = feedbackDescription;
	}

	/**
	 * Gets the feedback created by.
	 *
	 * @return the feedback created by
	 */
	public String getFeedbackCreatedBy() {
		return feedbackCreatedBy;
	}

	/**
	 * Sets the feedback created by.
	 *
	 * @param feedbackCreatedBy the new feedback created by
	 */
	public void setFeedbackCreatedBy(String feedbackCreatedBy) {
		this.feedbackCreatedBy = feedbackCreatedBy;
	}

	/**
	 * Gets the feedback given by.
	 *
	 * @return the feedback given by
	 */
	public UUID getFeedbackGivenBy() {
		return feedbackGivenBy;
	}


	/**
	 * Sets the feedback given by.
	 *
	 * @param feedbackGivenBy the new feedback given by
	 */

	public void setFeedbackGivenBy(UUID feedbackGivenBy) {
		this.feedbackGivenBy = feedbackGivenBy;
	}
	
	public LocalDateTime getFeedbackModifiedTime() {
		return feedbackModifiedTime;
	}

	public void setFeedbackModifiedTime(LocalDateTime feedbackModifiedTime) {
		this.feedbackModifiedTime = feedbackModifiedTime;
	}

	public BigInteger getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(BigInteger propertyId) {
		this.propertyId = propertyId;
	}	
	

}
