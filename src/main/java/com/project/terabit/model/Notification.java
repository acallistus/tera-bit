package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.UUID;


/**
 * The Class Notification.
 */
public class Notification {

	/** The notification id. */
	private BigInteger notificationId;

	/** The notification content. */
	private String notificationContent;

	/** The notification to. */
	private UUID notificationTo;

	/** The notification notified time. */
	private LocalDateTime notificationNotifiedTime;

	/** The notification viewed time. */
	private LocalDateTime notificationViewedTime;

	/** The notification is active. */
	private boolean notificationIsActive;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the notification id.
	 *
	 * @return the notification id
	 */
	public BigInteger getNotificationId() {
		return notificationId;
	}

	/**
	 * Sets the notification id.
	 *
	 * @param notificationId the new notification id
	 */
	public void setNotificationId(BigInteger notificationId) {
		this.notificationId = notificationId;
	}

	/**
	 * Gets the notification content.
	 *
	 * @return the notification content
	 */
	public String getNotificationContent() {
		return notificationContent;
	}

	/**
	 * Sets the notification content.
	 *
	 * @param notificationContent the new notification content
	 */
	public void setNotificationContent(String notificationContent) {
		this.notificationContent = notificationContent;
	}



	public UUID getNotificationTo() {
		return notificationTo;
	}

	public void setNotificationTo(UUID notificationTo) {
		this.notificationTo = notificationTo;
	}

	/**
	 * Gets the notification notified time.
	 *
	 * @return the notification notified time
	 */
	public LocalDateTime getNotificationNotifiedTime() {
		return notificationNotifiedTime;
	}

	/**
	 * Sets the notification notified time.
	 *
	 * @param notificationNotifiedTime the new notification notified time
	 */
	public void setNotificationNotifiedTime(LocalDateTime notificationNotifiedTime) {
		this.notificationNotifiedTime = notificationNotifiedTime;
	}

	/**
	 * Gets the notification viewed time.
	 *
	 * @return the notification viewed time
	 */
	public LocalDateTime getNotificationViewedTime() {
		return notificationViewedTime;
	}

	/**
	 * Sets the notification viewed time.
	 *
	 * @param notificationViewedTime the new notification viewed time
	 */
	public void setNotificationViewedTime(LocalDateTime notificationViewedTime) {
		this.notificationViewedTime = notificationViewedTime;
	}

	/**
	 * Checks if is notification is active.
	 *
	 * @return true, if is notification is active
	 */
	public boolean isNotificationIsActive() {
		return notificationIsActive;
	}

	/**
	 * Sets the notification is active.
	 *
	 * @param notificationIsActive the new notification is active
	 */
	public void setNotificationIsActive(boolean notificationIsActive) {
		this.notificationIsActive = notificationIsActive;
	}



}
