package com.project.terabit.model;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

public class FilterProperty {

	private UUID userId;
	private List<String> cities;
	private List<String> countries;
	private List<String> landmarks;
	private List<String> states;
	private List<String> propertyTypes;
	private String fromAmount;
	private String toAmount;
	private BigInteger fromArea;
	private BigInteger toArea;
	private List<String> sellerNames;
	private List<String> propertyDescription;
	private String message;
	
	public List<String> getPropertyDescription() {
		return propertyDescription;
	}
	public void setPropertyDescription(List<String> propertyDescription) {
		this.propertyDescription = propertyDescription;
	}
	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
	
	public List<String> getSellerNames() {
		return sellerNames;
	}
	public void setSellerNames(List<String> sellerNames) {
		this.sellerNames = sellerNames;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getCities() {
		return cities;
	}
	public void setCities(List<String> cities) {
		this.cities = cities;
	}
	public List<String> getCountries() {
		return countries;
	}
	public void setCountries(List<String> countries) {
		this.countries = countries;
	}
	public List<String> getLandmarks() {
		return landmarks;
	}
	public void setLandmarks(List<String> landmarks) {
		this.landmarks = landmarks;
	}
	public List<String> getStates() {
		return states;
	}
	public void setStates(List<String> states) {
		this.states = states;
	}
	
	
	public List<String> getPropertyTypes() {
		return propertyTypes;
	}
	public void setPropertyTypes(List<String> propertyTypes) {
		this.propertyTypes = propertyTypes;
	}
	public String getFromAmount() {
		return fromAmount;
	}
	public void setFromAmount(String fromAmount) {
		this.fromAmount = fromAmount;
	}
	public String getToAmount() {
		return toAmount;
	}
	public void setToAmount(String toAmount) {
		this.toAmount = toAmount;
	}
	public BigInteger getFromArea() {
		return fromArea;
	}
	public void setFromArea(BigInteger fromArea) {
		this.fromArea = fromArea;
	}
	public BigInteger getToArea() {
		return toArea;
	}
	public void setToArea(BigInteger toArea) {
		this.toArea = toArea;
	}
}
