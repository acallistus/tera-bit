package com.project.terabit.model;


import java.math.BigInteger;

import java.util.UUID;

public class Search {

	private UUID userId;
	private String keyword;

	private BigInteger propertyId;	
	public BigInteger getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(BigInteger propertyId) {
		this.propertyId = propertyId;
	}

	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	
}
