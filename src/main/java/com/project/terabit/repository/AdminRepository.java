package com.project.terabit.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.AdminEntity;

@Repository
public interface AdminRepository extends CrudRepository<AdminEntity, BigInteger> {


	@Query(value = "SELECT * FROM admin a WHERE a.admin_id = :adminId and a.admin_is_active='t'", nativeQuery =  true)
	public AdminEntity getAdminByAdminId(@Param(value = "adminId") BigInteger adminId);
	
	@Query(value = "SELECT cast(user_id as varchar) FROM users u WHERE u.admin_id = :adminId and u.user_is_active='t'", nativeQuery = true)
	public String getUserByAdminId(@Param(value = "adminId") BigInteger adminId);
}
