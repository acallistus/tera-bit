package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.PropertyEntity;

@Repository
public interface FilterRepository extends CrudRepository<PropertyEntity, BigInteger>{
	
	

	@Query(value="SELECT * FROM property p WHERE p.property_id = :propertyid", nativeQuery=true)
	public PropertyEntity getfilteredProperty(@Param (value="propertyid") BigInteger propertyid);
	
	@Query(value="SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND UPPER(p.property_city) IN :city", nativeQuery=true)
	public List<BigInteger> getPropertyByFilteredCity(@Param (value="city") List<String> city);
	
	@Query(value="SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND UPPER(p.property_country) IN :country", nativeQuery=true)
	public List<BigInteger> getPropertyByFilteredCountry(@Param (value="country") List<String> country);
	
	@Query(value = "SELECT min(cast(p.property_esteemated_amount AS numeric(19))) FROM property p WHERE p.property_is_active = 't' ", nativeQuery=true)
	public String getMinimumPriceOfProperty();
	
	@Query(value = "SELECT max(cast(p.property_esteemated_amount AS numeric(19))) FROM property p WHERE p.property_is_active = 't' ", nativeQuery=true)
	public String getMaximumPriceOfProperty();
	
	@Query(value = "SELECT min(p.property_cent) FROM property p WHERE p.property_is_active = 't' ", nativeQuery=true)
	public BigInteger getMinimumSizeOfProperty();
	
	@Query(value = "SELECT max(p.property_cent) FROM property p WHERE p.property_is_active = 't' ", nativeQuery=true)
	public BigInteger getMaximumSizeOfProperty();
	
	@Query(value = "SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND cast(p.property_esteemated_amount AS numeric(19)) >= :from_price AND cast(p.property_esteemated_amount AS numeric(19)) <= :to_price", nativeQuery=true)
	public  List<BigInteger> getPropertyByFilteredPrice(@Param (value="from_price") BigInteger fromPrice, @Param (value="to_price") BigInteger toPrice);
	
	@Query(value = "SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND p.property_cent >=:from_area AND p.property_cent <= :to_area", nativeQuery=true)
	public  List<BigInteger> getPropertyByFilteredArea(@Param (value="from_area") BigInteger fromArea, @Param (value="to_area") BigInteger toArea);
	
	@Query(value="SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND UPPER(p.property_type) IN :propertyType", nativeQuery=true)
	public List<BigInteger> getPropertyByFilteredPropertyType(@Param (value="propertyType") List<String> propertyType);
	
	@Query(value="SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND UPPER(p.property_state) IN (:state)", nativeQuery=true)
	public List<BigInteger> getPropertyByFilteredState(@Param (value="state") List<String> state);
	
	@Query(value="SELECT p.property_id FROM property p WHERE p.property_is_active = 't' AND p.property_landmark IN (:landMark)", nativeQuery=true)
	public List<BigInteger> getPropertyByFilteredLandMark(@Param (value="landMark") List<String> landMark);
	
	@Query(value="SELECT p.property_id FROM property p INNER JOIN seller_seller_property_ids se ON se.seller_property_ids_property_id = p.property_id\r\n" + 
			" INNER JOIN seller s ON s.seller_id=se.seller_entity_seller_id INNER JOIN users u ON s.seller_id=u.seller_id\r\n" + 
			" WHERE p.property_is_active  = 't'	AND (s.seller_company_name IN (:sellerName) OR 	CONCAT(u.user_first_name||' '||u.user_last_name) IN (:sellerName))", nativeQuery=true)
	public List<BigInteger> getPropertyByFilteredSellerName(@Param (value="sellerName") List<String> sellerName);
	
	@Query(value = "SELECT p.property_id FROM property p WHERE p.property_description ilike :propertyDescription AND p.property_is_active = 't'", nativeQuery=true)
	public List<BigInteger> getPropertyByPropertyDescription(@Param (value="propertyDescription") String propertyDescription);
	
	@Query(value = "SELECT DISTINCT p.property_state FROM property p and p.property_is_active = 't'",nativeQuery = true)
	public List<String> getStateFromProperty();
	
	@Query(value = "SELECT p.property_city FROM property p WHERE p.property_state = :state AND p.property_is_active = 't' ",nativeQuery = true)
	public List<String> getCityByState(@Param (value="state") String state);
	
}
