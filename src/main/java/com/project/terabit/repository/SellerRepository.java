package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.SellerEntity;
@Repository
public interface SellerRepository extends CrudRepository<SellerEntity, BigInteger>{

	
	@Query(value = "SELECT * FROM seller s WHERE s.seller_private_contact = :contactNo", nativeQuery = true)
	public SellerEntity findSellerByContactNumber(@Param(value="contactNo") BigInteger contactNo);
	

	
	@Query(value = "SELECT * FROM seller s WHERE s.seller_id = :sellerId and s.seller_is_active='t'", nativeQuery = true)
	public SellerEntity findSellerBySellerId(@Param (value = "sellerId") BigInteger sellerId);
	
	@Query(value = "SELECT * FROM seller s INNER JOIN seller_seller_property_ids sp ON s.seller_id = sp.seller_entity_seller_id WHERE sp.seller_property_ids_property_id = :propertyId AND s.seller_is_active = 't' limit 1", nativeQuery=true)
	public SellerEntity findSellerByPropertyId(@Param (value = "propertyId") BigInteger propertyId);
	
	
	@Query(value = "SELECT * FROM seller s WHERE s.seller_id = :sellerId AND s.seller_is_active = 't' AND s.seller_right_by is not null", nativeQuery=true)
	public SellerEntity checkSellerAlreadyApproved(@Param(value = "sellerId") BigInteger sellerId);
	
	@Query(value = "SELECT * FROM seller s WHERE s.seller_id = :sellerId", nativeQuery = true)
	public SellerEntity getSellerBySellerId(@Param (value = "sellerId") BigInteger sellerId);
	

	@Query(value = "SELECT * FROM seller s WHERE s.seller_right_by = :sellerRightBy and seller_is_active='t'", nativeQuery = true)
	List<SellerEntity> getSellerByAdmin(@Param(value = "sellerRightBy") BigInteger adminId); 
	
	@Query(value = "SELECT * FROM seller s INNER JOIN users u ON s.seller_id = u.seller_id WHERE s.seller_right_by = :sellerRightBy and s.seller_is_active='f' and u.user_is_seller='t' ", nativeQuery = true)
	List<SellerEntity> getApprovalPendingSeller(@Param(value = "sellerRightBy") BigInteger adminId); 
	

}
