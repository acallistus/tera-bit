package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.NotificationEntity;

@Repository("notificationRepository")
public interface NotificationRepository extends CrudRepository<NotificationEntity,BigInteger> {
	
	@Query(value="select * from notification n where n.notification_id=:notificationId AND n.notification_to = :userId",nativeQuery=true)
	public NotificationEntity findNotificationByNotificationId(@Param(value="notificationId") BigInteger notificationId,@Param(value="userId") UUID userId);
	
	@Query(value="select * from notification n where n.notification_to = :userId AND n.notification_is_active='t'",nativeQuery=true)
	public List<NotificationEntity> findNotificationByUserId(@Param(value="userId") UUID userId);

}
