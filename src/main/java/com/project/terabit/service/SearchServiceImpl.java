package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Property;
import com.project.terabit.model.Search;

import com.project.terabit.model.User;

import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SearchRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.SearchValidator;

/**@author deepik sriram 
**/
@Service
@Transactional(readOnly=true)
public class SearchServiceImpl implements SearchService{

	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	


	/** The propertyrepository. */
	@Autowired
	PropertyRepository propertyrepository;
	
	@Autowired
	SearchRepository searchRepository;
	 
	private static final String SERVICEEXCEPTION_URLMISSMATCH="SEARCHSERVICE.saltstring_not_provided";
	 
	private static final String SERVICEEXCEPTION_USERIDEXCEPTION = "SEARCHSERVICE.invalid_user";
	
	private static final String SERVICEEXCEPTION_KEYWORDEXCEPTION = "SEARCHSERVICE.invalid_keyword";
	

	private static final String SERVICEEXCEPTION_NULLUSERIDEXCEPTION = "SEARCHFORSELLERSERVICE.invalid_user_id";
	
	private static final String SERVICEEXCEPTION_INPUTEXCEPTION = "SEARCHFORSELLERSERVICE.invalid_inputs";
	
	@Override
	public List<Property> search(Search search, String saltstring) throws Exception {
		Set<PropertyEntity> finalPropertySet = new HashSet<>();

		List<Property> returnProperty = new ArrayList<>();
		try {
			if(search.getKeyword()==null) {
				throw new ServiceException(SERVICEEXCEPTION_KEYWORDEXCEPTION);
			}
			SearchValidator.validate(search.getKeyword());
			//searching the property which matches the keyword

			
			List<BigInteger> propertyListBySearch = new ArrayList<>();
			if(search.getPropertyId().compareTo(BigInteger.valueOf(0l))>=0) {
				propertyListBySearch = searchRepository.getPropertyIdBySearch(search.getKeyword(),search.getPropertyId());
			}

			//if user has logged in checking for salt string match
			if(search.getUserId()!=null) {
				if(saltstring==null) {
					throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
				}
				UsersEntity userEntity = userRepository.findUserById(search.getUserId(), saltstring);
				if(userEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
				}
				//searching for properties that the user viewed already
				List<BigInteger> propertyListByViewedProperty = searchRepository.getPropertyIdByViewedProperty(search.getUserId());
				//populating the already viewed property first
				for(BigInteger propertyId: propertyListBySearch) {
					if(propertyListByViewedProperty.contains(propertyId)) {
						finalPropertySet.add(propertyrepository.getPropertyByPropertyId(propertyId));

					}
				}
			}
			//populating the list with values of property

			if(propertyListBySearch!=null && !propertyListBySearch.isEmpty()) {
				for(BigInteger propertyId: propertyListBySearch) {
					finalPropertySet.add(propertyrepository.getPropertyByPropertyId(propertyId));
				}
			}
			//calling login to set property entity to property model
			LoginUserServiceImpl login = new LoginUserServiceImpl();
			
			returnProperty = login.propertysetting(finalPropertySet.stream().collect(Collectors.toList()));

		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("Search "+exception.getMessage());
			throw exception;
		}

		
		return returnProperty;
	}
	
	@Override
	public User searchForSeller(User user, String saltstring) throws Exception {
		User returnUser = new User();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			if(user.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLUSERIDEXCEPTION);
			}
			UsersEntity userEntity = userRepository.findUserById(user.getUserId(), saltstring);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}
			UsersEntity returnUserEntity = new UsersEntity();
			if(user.getUserEmailId()==null && user.getUserContactNo()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INPUTEXCEPTION);
			}
			if(user.getUserEmailId()!=null) {
				returnUserEntity = userRepository.findUserByEmailID(user.getUserEmailId());
			}
			else if(user.getUserContactNo()!=null) {
				returnUserEntity = userRepository.findUserByContactNumber(user.getUserContactNo());
			}
			returnUser.setUserId(returnUserEntity.getUserId());
			returnUser.setUserEmailId(returnUserEntity.getUserEmailId());
			returnUser.setUserContactNo(returnUserEntity.getUserContactNo());
			returnUser.setUserFirstName(returnUserEntity.getUserFirstName());
			returnUser.setUserLastName(returnUserEntity.getUserLastName());
			returnUser.setUserIsActive(returnUserEntity.isUserIsActive());
			returnUser.setUserIsSeller(returnUserEntity.isUserIsSeller());
			returnUser.setUserCreatedTime(returnUserEntity.getUserCreatedTime());
			returnUser.setUserModifiedTime(returnUserEntity.getUserModifiedTime());
			
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			if(returnUserEntity.getUserAdminId()!=null) {
				returnUser.setUserAdminId(loginservice.adminForUser(returnUserEntity));
			}
			
			
			if(returnUserEntity.getUserImage()!=null) {
				returnUser.setUserImage(loginservice.imageForUser(returnUserEntity.getUserImage()));
			}
			
			if(returnUserEntity.getUserSellerId()!=null) {
				returnUser.setUserSellerId(loginservice.sellerForAdmin(returnUserEntity.getUserSellerId()));
			}
			
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("Search "+exception.getMessage());
			throw exception;
		}
		
		return returnUser;
	}


	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}
}
