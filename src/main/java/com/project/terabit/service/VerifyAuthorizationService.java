package com.project.terabit.service;

import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Component;

@Component
public interface VerifyAuthorizationService {
	
	public void userVerification(String authorizationid) throws ServiceException,Exception;

}
