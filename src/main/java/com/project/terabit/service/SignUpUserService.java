package com.project.terabit.service;

import org.springframework.stereotype.Component;

import com.project.terabit.model.User;




/**@author deepik sriram & joshua shakespeare
/**
 * The Interface SignUpService.
 */
@Component
public interface SignUpUserService {
	
	
	/**
	 * Creates the user.
	 *
	 * @param user the user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public User saveUser(User user) throws Exception;
	
	/**
	 * Update user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	public User updateUser(User user, String saltstring) throws Exception;
	/**
	 * Delete user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws Exception the exception
	 */
	public User deleteUser(String saltstring,User user) throws Exception;
	
	public void forgetPassword(String user) throws Exception;
	
	public User changePassword(User user) throws Exception;
}
