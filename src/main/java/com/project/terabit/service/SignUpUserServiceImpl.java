package com.project.terabit.service;

import java.time.LocalDateTime;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.controller.ControllerException;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.User;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.utility.MailVerification;
import com.project.terabit.utility.PasswordHashing;
import com.project.terabit.utility.RandomStringGenerator;
import com.project.terabit.validator.DeleteUserValidator;
import com.project.terabit.validator.LoginUserValidator;
import com.project.terabit.validator.SignUpUserValidator;
import com.project.terabit.validator.UpdateUserValidator;


/**@author deepik sriram & joshua shakespeare & solai ganesh
/**
 * The Class SignUpServiceImpl.
 */
@Service
public class SignUpUserServiceImpl implements SignUpUserService{
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;
	
	/** The seller repository. */
	@Autowired
	private SellerRepository sellerRepository;
	
	/** The password hasher. */
	PasswordHashing passwordHasher = new PasswordHashing();
	
	RandomStringGenerator saltstring = new RandomStringGenerator();
	

	

	

	
	/** The Constant SERVICEEXCEPTION_USERIDEXCEPTION. */
	private static final String SERVICEEXCEPTION_USERIDEXCEPTION = "UTILITYSERVICE.invalid_user";
	
	private static final String SERVICEEXCEPTION_PASSKEYMISSMATCHEXCEPTION = "UPDATESERVICE.invalid_password";
	
	private static final String SERVICEEXCEPTION_SALTSTRINGMISSMATCH = "UTILITYSERVICE.invalid_saltstring";
	
    private static final String SERVICEEXCEPTION_URLMISSMATCH="UTILITYSERVICE.saltstring_not_provided";
	private static final String SERVICEEXCEPTION_NOUSERID = "UTILITYSERVICE.no_userid_provided";
	
	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_USERALREADYEXISTS = "SIGNUPSERVICE.invalid_signUp";
	

	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS = "UPDATESERVICE.invalid_contactNumber";
	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_EMAILALREADYEXISTS = "UPDATESERVICE.invalid_mailId";
	
	private static final String SERVICEEXCEPTION_PASSKEYVALIDATIONFAILED = "UPDATESERVICE.invalid_passkey_validation";
	
	private static final String SERVICEEXCEPTION_NOEMAILANDPHONE="SERVICEEXCEPTION.no_mail_or_contactno_provided";

	private static final String WELCOMENOTIFICATION = "Welcome to Terabit!";
	
	private static final String PASSKEYCHANGE = "Click on the link to change password";
	
	private static final String SERVICEEXCEPTIONSAMEPASSKEY="CHANGEPASSWORD.password__is_same_as_oldone";
	

	
	private static final String SERVICEEXCEPTIONNOAUTHORIZATIONID="CHANGEPASSWORD.authorization_id_is_Empty";
	
	/** The Constant userAlreadyExists. */
	
	private static final String SERVICEEXCEPTIONNOPASSKEY="CHANGEPASSWORD.password_id_is_Empty";

	private static final String SERVICEEXCEPTION_VERIFICATIONTIMEOUT="SERVICE.User_Cannot_Be_Verified";

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SignUpService#createUser(com.project.terabit.model.User)
	 */
	@Override
	public User saveUser(User user) throws Exception {
		UsersEntity userEntityToBeAdded=new UsersEntity();
		UsersEntity userEntityToBeReturned;
		try {
		SignUpUserValidator.validate(user);
		UsersEntity userEntity = userRepository.checkUser(user.getUserEmailId(),user.getUserContactNo());
		if(userEntity!=null) {
			throw new ServiceException(SERVICEEXCEPTION_USERALREADYEXISTS);
		}
		userEntityToBeAdded.setIsuserAuthenticated(user.isUserAuthenticated());
		userEntityToBeAdded.setUserPassword(passwordHasher.hashing(user.getUserPassword()));
		userEntityToBeAdded.setUserContactNo(user.getUserContactNo());
		userEntityToBeAdded.setUserCreatedTime(LocalDateTime.now());
		userEntityToBeAdded.setUserEmailId(user.getUserEmailId());
		userEntityToBeAdded.setUserFirstName(user.getUserFirstName());
		userEntityToBeAdded.setUserIsActive(true);
		userEntityToBeAdded.setUserIsSeller(user.isUserIsSeller());
		userEntityToBeAdded.setUserLastName(user.getUserLastName());
		userEntityToBeAdded.setUserModifiedTime(LocalDateTime.now());
		userEntityToBeAdded.setSaltString(saltstring.randomString());
		userEntityToBeAdded.setUserPin(user.getUserPin());
		userEntityToBeAdded.setUserVerifed(user.isUserVerifed());
		userEntityToBeAdded.setAuthorizationid(RandomStringGenerator.authorizationIdGenerator());
		userEntityToBeReturned=userRepository.save(userEntityToBeAdded);
		
		NotificationEntity notificationEntity = new NotificationEntity();
		notificationEntity.setNotificationTo(userEntityToBeReturned.getUserId());
		notificationEntity.setNotificationContent(WELCOMENOTIFICATION);
		notificationEntity.setNotificationIsActive(true);
		notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
		notificationEntity.setNotificationViewedTime(LocalDateTime.now());
		userEntityToBeAdded.getUserNotificationIds().add(notificationEntity);
		userRepository.save(userEntityToBeAdded);
		
		MailVerification.sendMailForLogIn(user.getUserEmailId(), userEntityToBeAdded.getAuthorizationid(),WELCOMENOTIFICATION);
		user.setUserIsActive(true);
		user.setUserId(userEntityToBeReturned.getUserId());
		user.setUserCreatedTime(LocalDateTime.now());
		} 
		catch(ServiceException exception){
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("SignUp"+exception.getMessage());
			throw exception;
		}
		return user;
		
	}

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SignUpService#verifyUser(com.project.terabit.entity.UserEntity)
	 */
	@Override
	public User updateUser(User user, String saltstring) throws Exception {
		
		try {
			UsersEntity userEntityToBeUpdated;
			LocalDateTime currenttime=LocalDateTime.now();
			if(user.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			userEntityToBeUpdated=userRepository.findUserByUserId(user.getUserId());
			if(userEntityToBeUpdated==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}else if(!userEntityToBeUpdated.getSaltString().equals(saltstring)) {
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRINGMISSMATCH);
			}
			
			if(user.getUserEmailId()!=null) {
				UsersEntity userEntityForMail = userRepository.checkUserMailId(user.getUserEmailId());
				if(userEntityForMail!=null) {
					throw new ServiceException(SERVICEEXCEPTION_EMAILALREADYEXISTS);
				}
			}
			if(user.getUserContactNo()!=null) {
				UsersEntity userEntityForPhone = userRepository.checkUserContactNumber(user.getUserContactNo());
				if(userEntityForPhone!=null) {
					throw new ServiceException(SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS);
				}
			}
			if(user.getUserPassword()!=null && user.getNewUserPassword()!=null) {
				if(userEntityToBeUpdated.getUserPassword().equals(passwordHasher.hashing(user.getUserPassword()))) {
					Boolean flag = true;
					if(!(flag.equals(LoginUserValidator.validatePassword(user.getNewUserPassword())))) {
						throw new ServiceException(SERVICEEXCEPTION_PASSKEYVALIDATIONFAILED);
					}
					userEntityToBeUpdated.setUserPassword(passwordHasher.hashing(user.getNewUserPassword()));
				}
				else {
					throw new ServiceException(SERVICEEXCEPTION_PASSKEYMISSMATCHEXCEPTION);
				}
			}
			
			if(user.getUserEmailId()==null) user.setUserEmailId(userEntityToBeUpdated.getUserEmailId());
			if(user.getUserContactNo()==null) user.setUserContactNo(userEntityToBeUpdated.getUserContactNo());
			if(user.getUserFirstName()==null) user.setUserFirstName(userEntityToBeUpdated.getUserFirstName());
			if(user.getUserLastName()==null) user.setUserLastName(userEntityToBeUpdated.getUserLastName());
			
			UpdateUserValidator.updateInfoValidate(user);

			
			
			userEntityToBeUpdated.setUserAdminId(userEntityToBeUpdated.getUserAdminId());
			userEntityToBeUpdated.setIsuserAuthenticated(userEntityToBeUpdated.isIsuserAuthenticated());
			userEntityToBeUpdated.setUserCarts(userEntityToBeUpdated.getUserCarts());
			
			userEntityToBeUpdated.setUserCreatedTime(userEntityToBeUpdated.getUserCreatedTime());
			
			userEntityToBeUpdated.setUserFeedbacks(userEntityToBeUpdated.getUserFeedbacks());
			if(user.getUserFirstName()!=null) {
				userEntityToBeUpdated.setUserFirstName(user.getUserFirstName());
			}
			if(user.getUserLastName()!=null) {
				userEntityToBeUpdated.setUserLastName(user.getUserLastName());
			}
			if(user.getUserContactNo()!=null) {
				userEntityToBeUpdated.setUserContactNo(user.getUserContactNo());
			}
			if(user.getUserEmailId()!=null) {
				userEntityToBeUpdated.setUserEmailId(user.getUserEmailId());
			}
			userEntityToBeUpdated.setUserImage(userEntityToBeUpdated.getUserImage());
			userEntityToBeUpdated.setUserIsActive(userEntityToBeUpdated.isUserIsActive());
			userEntityToBeUpdated.setUserIsSeller(userEntityToBeUpdated.isUserIsSeller());
			
			userEntityToBeUpdated.setUserModifiedTime(currenttime);
			userEntityToBeUpdated.setUserNotificationIds(userEntityToBeUpdated.getUserNotificationIds());
			userEntityToBeUpdated.setUserPin(userEntityToBeUpdated.getUserPin());
			userEntityToBeUpdated.setUserSellerId(userEntityToBeUpdated.getUserSellerId());
			userEntityToBeUpdated.setUserVerifed(userEntityToBeUpdated.isUserVerifed());
			userEntityToBeUpdated.setUserViewedProperties(userEntityToBeUpdated.getUserViewedProperties());
			userEntityToBeUpdated.setUserPassword(userEntityToBeUpdated.getUserPassword());
			userRepository.save(userEntityToBeUpdated);
			
			user.setUserVerifed(userEntityToBeUpdated.isUserVerifed());
			user.setUserAuthenticated(userEntityToBeUpdated.isIsuserAuthenticated());
			user.setSaltstring(userEntityToBeUpdated.getSaltString());
			user.setUserIsSeller(userEntityToBeUpdated.isUserIsSeller());
			user.setUserIsActive(userEntityToBeUpdated.isUserIsActive());
			user.setUserCreatedTime(userEntityToBeUpdated.getUserCreatedTime());
			user.setUserModifiedTime(userEntityToBeUpdated.getUserModifiedTime());
			
			user.setUserPassword(null);
			user.setNewUserPassword(null);
		
			return user;
		}catch(ServiceException exception) {
			logg("Update:"+exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg(exception.getMessage());
			throw exception;
		}
		
		
	}
	
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User deleteUser(String saltstring,User user) throws Exception{
		UsersEntity userEntity = new UsersEntity();
		try {
			if(user.getUserId()==null) {
				throw new ControllerException(SERVICEEXCEPTION_NOUSERID);
			}
			if(saltstring==null){
				throw new ControllerException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			DeleteUserValidator.validate(user.getUserId().toString());
			userEntity = userRepository.findUserByUserId(user.getUserId());
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}else if(!userEntity.getSaltString().equals(saltstring)) {
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRINGMISSMATCH);
			}
			userEntity.setUserIsActive(false);
			if((userEntity.isUserIsSeller()) && (userEntity.getUserSellerId()!=null)) {
				SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(userEntity.getUserSellerId().getSellerId());
				sellerEntity.setSellerIsActive(false);
				if(!sellerEntity.getSellerPropertyId().isEmpty()) {
					for(int index=0;index<sellerEntity.getSellerPropertyId().size();index++) {
						sellerEntity.getSellerPropertyId().get(index).setPropertyIsActive(false);
					}
				}
			}
			if((userEntity.getUserAdminId()!=null) && (userEntity.getUserAdminId().getAdminRightsBy()!=null)) {
				userEntity.getUserAdminId().setAdminIsActive(false);
			}
			userRepository.save(userEntity);
			return user;
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("deleteUser "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public void forgetPassword(String mailid) throws Exception
	{
		try {
		if(mailid==null )
			throw new ServiceException(SERVICEEXCEPTION_NOEMAILANDPHONE);
		
		UsersEntity passwordToBeChangeUserEntity=userRepository.findUserByEmailID(mailid);
		
		
		if(passwordToBeChangeUserEntity==null)
			throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
		
		passwordToBeChangeUserEntity.setUserModifiedTime(LocalDateTime.now());
		
		passwordToBeChangeUserEntity.setAuthorizationid(RandomStringGenerator.authorizationIdGenerator());
		
		userRepository.save(passwordToBeChangeUserEntity);
		MailVerification.sendMailForPasskeyChange(mailid, passwordToBeChangeUserEntity.getAuthorizationid(),PASSKEYCHANGE);
		}
		catch(ServiceException serviceexception)
		{
			logg(serviceexception.getMessage());
			
			throw serviceexception;
			
		}
		catch(Exception exception)
		{
			logg(exception.getMessage());
			
			throw exception;
		}
	}
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User changePassword(User user) throws Exception {
			try
			{
				if(user.getAuthorizationstring()==null)
				{
					throw new ServiceException(SERVICEEXCEPTIONNOAUTHORIZATIONID);
				}
				
				if(user.getUserPassword()==null)
				{
					throw new ServiceException(SERVICEEXCEPTIONNOPASSKEY);
				}
				
				UsersEntity passwordToBeChangeUserEntity=userRepository.findUserFromAuthorizationId(user.getAuthorizationstring());
				
				if(passwordToBeChangeUserEntity==null)
					throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
				
				if(passwordToBeChangeUserEntity.getUserModifiedTime().compareTo(LocalDateTime.now().minusMinutes(15))<0)
				{
					throw new ServiceException(SERVICEEXCEPTION_VERIFICATIONTIMEOUT);
				}
				user.setUserEmailId(passwordToBeChangeUserEntity.getUserEmailId());
				user.setUserContactNo(passwordToBeChangeUserEntity.getUserContactNo());
			
				LoginUserValidator.validate(user);
				
				
				if(passwordHasher.hashing(user.getUserPassword()).equals(passwordToBeChangeUserEntity.getUserPassword()))
					throw new ServiceException(SERVICEEXCEPTIONSAMEPASSKEY);
				passwordToBeChangeUserEntity.setUserPassword(passwordHasher.hashing(user.getUserPassword()));
				
				userRepository.save(passwordToBeChangeUserEntity);
				
				return user;
			}
			catch(ServiceException serviceexception)
			{
				logg(serviceexception.getMessage());
				
				throw serviceexception;
				
			}
			catch(Exception exception)
			{
				logg(exception.getMessage());
				
				throw exception;
			}
			
		
	}
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}


	
	
}
