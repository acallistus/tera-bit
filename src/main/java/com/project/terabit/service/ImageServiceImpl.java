package com.project.terabit.service;

import java.time.LocalDateTime;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Image;
import com.project.terabit.repository.ImageRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;

@Service
@Transactional(readOnly=true)
public class ImageServiceImpl  implements ImageService{

	
	/** The log. */
	//***** Declaring the logger for logging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The user repository. */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	
	/** The admin repository. */
	//***** Autowiring the AdminRepository # # #
	@Autowired
	SellerRepository sellerRepository;
	
	/** The propertyrepository. */
	@Autowired
	PropertyRepository propertyrepository;
	
	/** The propertyrepository. */
	@Autowired
	ImageRepository imageRepository;
	
	
	//*****Defining exception Strings:
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "IMAGESERVICE.saltString_null";
	
	private static final String SERVICEEXCEPTION_USERIDEXCEPTION = "IMAGESERVICE.invalid_user";
	
	private static final String SERVICEEXCEPTION_NOUSERIDEXCEPTION = "IMAGESERVICE.no_userid";
	
	private static final String SERVICEEXCEPTION_SELLEREXCEPTION = "IMAGESERVICE.invalid_seller";
	
	private static final String SERVICEEXCEPTION_SELLERNOTAUTHORIZED = "IMAGESERVICE.seller_not_authorised";
	
	private static final String SERVICEEXCEPTION_PROPERTYEXCEPTION = "IMAGESERVICE.invalid_property";
	
	private static final String SERVICEEXCEPTION_IMAGEPATHEXCEPTION = "IMAGESERVICE.invalid_image_path";
	
	private static final String SERVICEEXCEPTION_IMAGEIDEXCEPTION = "IMAGESERVICE.invalid_image_id";
	
	private static final String SERVICEEXCEPTION_INPUTEXCEPTION = "IMAGESERVICE.invalid_input";
	
	private static final String SERVICEEXCEPTION_IMAGEALREADYEXISTS = "CREATEIMAGESERVICE.invalid_image";
	
	private static final String SERVICEEXCEPTION_PROPERTYIDINPUTEXCEPTION = "UPDATEIMAGESERVICE.invalid_property_id";
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Image createImage(String saltstring, Image image) throws Exception {
		Image imageToBeReturned = new Image();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			ImageEntity imageEntity = imageRepository.getImageByPath(image.getImagePath());
			if(imageEntity!=null) {
				throw new ServiceException(SERVICEEXCEPTION_IMAGEPATHEXCEPTION);
			}
			ImageEntity imageEntityToBeSaved = new ImageEntity();
			imageEntityToBeSaved.setImageCreatedTime(LocalDateTime.now());
			imageEntityToBeSaved.setImageDescription(image.getImageDescription());
			imageEntityToBeSaved.setImageIsActive(true);
			imageEntityToBeSaved.setImagePath(image.getImagePath());
			if(image.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERIDEXCEPTION);
			}
			UsersEntity userEntity = userRepository.findUserById(image.getUserId(), saltstring);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}
			
			if(image.getSellerId()==null && image.getPropertyId()==null) {
				
				if(userEntity.getUserImage()!=null && userEntity.getUserImage().isImageIsActive() ) {
					throw new ServiceException(SERVICEEXCEPTION_IMAGEALREADYEXISTS);
				}
				userEntity.setUserImage(imageEntityToBeSaved);
				
				imageToBeReturned.setImageId(userEntity.getUserImage().getImageId());
			}
			
			if(image.getSellerId()!=null) {
				if((userEntity.getUserSellerId()==null) || !(userEntity.isUserIsSeller())) {
					throw new ServiceException(SERVICEEXCEPTION_SELLEREXCEPTION);
				}
				SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(image.getSellerId());
				if(sellerEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_SELLERNOTAUTHORIZED);
				}
				if(image.getPropertyId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_INPUTEXCEPTION);
				}
				if(sellerEntity.getSellerCompanyLogoId()!=null && sellerEntity.getSellerCompanyLogoId().isImageIsActive()) {
					throw new ServiceException(SERVICEEXCEPTION_IMAGEALREADYEXISTS);
				}
				sellerEntity.setSellerCompanyLogoId(imageEntityToBeSaved);
				imageToBeReturned.setImageId(sellerEntity.getSellerCompanyLogoId().getImageId());
			}
			if(image.getPropertyId()!=null) {
				PropertyEntity propertyEntity = propertyrepository.getPropertyByPropertyId(image.getPropertyId());
				if(propertyEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_PROPERTYEXCEPTION);
				}
				propertyEntity.getPropertyImageId().add(imageEntityToBeSaved);
			}
			imageRepository.save(imageEntityToBeSaved);
			imageToBeReturned.setImageCreatedTime(imageEntityToBeSaved.getImageCreatedTime());
			imageToBeReturned.setImageDescription(imageEntityToBeSaved.getImageDescription());
			imageToBeReturned.setImageIsActive(imageEntityToBeSaved.isImageIsActive());
			imageToBeReturned.setImagePath(imageEntityToBeSaved.getImagePath());
			imageToBeReturned.setPropertyId(image.getPropertyId());
			imageToBeReturned.setSellerId(image.getSellerId());
			imageToBeReturned.setUserId(image.getUserId());
			imageToBeReturned.setImageId(imageEntityToBeSaved.getImageId());
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("CreateImage "+exception.getMessage());
			throw exception;
		}
		return imageToBeReturned;
	}
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Image deleteImage(String saltstring,Image image) throws Exception{
		Image imageToBeReturned = new Image();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			ImageEntity imageEntity = imageRepository.getImageByImageId(image.getImageId());
			if(imageEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_IMAGEIDEXCEPTION);
			}
			imageEntity.setImageIsActive(false);
			if(image.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERIDEXCEPTION);
			}
			UsersEntity userEntity = userRepository.findUserById(image.getUserId(), saltstring);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}
			if(userEntity.getUserImage().getImageId()==image.getImageId()) {
				userEntity.setUserImage(imageEntity);
			}
			if(image.getSellerId()!=null) {
				if(image.getPropertyId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_INPUTEXCEPTION);
				}
				SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(image.getSellerId());
				if(sellerEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_SELLERNOTAUTHORIZED);
				}
				if((userEntity.getUserSellerId()==null) || !(userEntity.isUserIsSeller())) {
					throw new ServiceException(SERVICEEXCEPTION_SELLEREXCEPTION);
				}
				
				if(sellerEntity.getSellerCompanyLogoId().getImageId()==image.getImageId()) {
					sellerEntity.setSellerCompanyLogoId(imageEntity);
				}
			}
			if(image.getPropertyId()!=null) {
				PropertyEntity propertyEntity = propertyrepository.getPropertyByPropertyId(image.getPropertyId());
				if(propertyEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_PROPERTYEXCEPTION);
				}
				for (ImageEntity imageEntityIndex : propertyEntity.getPropertyImageId()) {
					if(imageEntityIndex.getImageId()==image.getImageId()) {
						imageEntityIndex.setImageIsActive(false);
					}
				}
			}
			imageRepository.save(imageEntity);
			imageToBeReturned.setImageCreatedTime(imageEntity.getImageCreatedTime());
			imageToBeReturned.setImageDescription(imageEntity.getImageDescription());
			imageToBeReturned.setImageIsActive(imageEntity.isImageIsActive());
			imageToBeReturned.setImagePath(imageEntity.getImagePath());
			imageToBeReturned.setPropertyId(image.getPropertyId());
			imageToBeReturned.setSellerId(image.getSellerId());
			imageToBeReturned.setUserId(image.getUserId());
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("DeleteImage "+exception.getMessage());
			throw exception;
		}
		return imageToBeReturned;
	}
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Image updateImage(String saltstring, Image image) throws Exception {
		Image imageToBeReturned = new Image();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(image.getPropertyId()!=null) {
				throw new ServiceException(SERVICEEXCEPTION_PROPERTYIDINPUTEXCEPTION);
			}
			ImageEntity imageEntity = imageRepository.getImageByPath(image.getImagePath());
			if(imageEntity!=null) {
				throw new ServiceException(SERVICEEXCEPTION_IMAGEPATHEXCEPTION);
			}
			imageEntity = imageRepository.getImageByImageId(image.getImageId());
			if(imageEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_IMAGEIDEXCEPTION);
			}
			imageEntity.setImagePath(image.getImagePath());
			UsersEntity userEntity = userRepository.findUserById(image.getUserId(), saltstring);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}
			if(image.getSellerId()==null) {
				if(userEntity.getUserImage().getImageId()==image.getImageId()) {
					userEntity.setUserImage(imageEntity);
				}
			}
			else {
				if((userEntity.getUserSellerId()==null) || !(userEntity.isUserIsSeller())) {
					throw new ServiceException(SERVICEEXCEPTION_SELLEREXCEPTION);
				}
				SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(image.getSellerId());
				if(sellerEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_SELLERNOTAUTHORIZED);
				}
				if(sellerEntity.getSellerCompanyLogoId().getImageId()==image.getImageId()) {
					sellerEntity.setSellerCompanyLogoId(imageEntity);
				}
			}
			imageRepository.save(imageEntity);
			imageToBeReturned.setImageCreatedTime(imageEntity.getImageCreatedTime());
			imageToBeReturned.setImageDescription(imageEntity.getImageDescription());
			imageToBeReturned.setImageIsActive(imageEntity.isImageIsActive());
			imageToBeReturned.setImagePath(imageEntity.getImagePath());
			imageToBeReturned.setPropertyId(image.getPropertyId());
			imageToBeReturned.setSellerId(image.getSellerId());
			imageToBeReturned.setUserId(image.getUserId());
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("UpdateImage "+exception.getMessage());
			throw exception;
		}
		return imageToBeReturned;
	}
	
	
	private void logg(String message) {
		// ***** Logging the message # # #
		log.error(message);
	}

	

}
