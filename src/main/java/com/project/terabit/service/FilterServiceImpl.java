package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.FilterProperty;
import com.project.terabit.model.Property;
import com.project.terabit.repository.FilterRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.FilterValidator;

@Component
@Transactional(readOnly=true)
public class FilterServiceImpl implements FilterService{
	
	
	/** The filter repository. */
	//***** Autowiring the AdminRepository # # #
	@Autowired
	FilterRepository filterRepository;
	
	@Autowired
	UserRepository userRepository;

	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	private static final Boolean FLAG = false;
	
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "FILTERSERVICE.no_saltstring";	
	
	private static final String SERVICEEXCEPTION_NOUSER = "FILTERSERVICE.nouserid";
	
	private static final String SERVICEEXCEPTION_NOPROPERTY = "FILTERSERVICE.no_property_for_filter";
	
	private static final String SERVICEEXCEPTION_INVALIDCITY = "FILTERSERVICE.invalid_city";
	
	private static final String SERVICEEXCEPTION_INVALIDCOUNTRY = "FILTERSERVICE.invalid_country";
	
	private static final String SERVICEEXCEPTION_INVALIDPRICE = "FILTERSERVICE.invalid_price";
	
	private static final String SERVICEEXCEPTION_INVALIDAREA = "FILTERSERVICE.invalid_area";
	
	private static final String SERVICEEXCEPTION_INVALIDSTATE = "FILTERSERVICE.invalid_state";
	
	private static final String SERVICEEXCEPTION_INVALIDLANDMARK = "FILTERSERVICE.invalid_landmark";
	
	private static final String SERVICEEXCEPTION_INVALIDPROPERTYTYPE = "FILTERSERVICE.invalid_property_type";
	
	private static final String SERVICEEXCEPTION_INVALIDSELLERNAMES = "FILTERSERVICE.invalid_seller_name";
	
	
	@Override
	public List<Property> getPropertiesForTheFilter(FilterProperty filters, String saltstring) throws Exception{
		List<BigInteger> propertyList = new ArrayList<>();
		List<Property> filteredList = new ArrayList<>();
		try {
			if(filters.getUserId()!=null) {
				if(saltstring==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
				}
				UsersEntity userEntity = userRepository.findUserById(filters.getUserId(), saltstring);
				if(userEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOUSER);
				}
			}
			
			
			if((filters.getCities()!=null) && (!filters.getCities().isEmpty())) {
				propertyList.addAll(this.filterByCity(filters.getCities()));
			}
			if((filters.getCountries()!=null) && (!filters.getCountries().isEmpty())) {
				propertyList.addAll(this.filterByCountry(filters.getCountries()));
			}
			if((filters.getLandmarks()!=null) && (!filters.getLandmarks().isEmpty())) {
				propertyList.addAll(this.filterByLandMark(filters.getLandmarks()));
			}
			if((filters.getStates()!=null) && (!filters.getStates().isEmpty())) {
				propertyList.addAll(this.filterByState(filters.getStates()));
			}
			if(((filters.getFromAmount()!=null) && (!filters.getFromAmount().isEmpty())) || ((filters.getToAmount()!=null) && (!filters.getToAmount().isEmpty()))) {
				if(filters.getFromAmount()==null) {
					filters.setFromAmount(filterRepository.getMinimumPriceOfProperty());
				}
				if(filters.getToAmount()==null) {
					filters.setToAmount(filterRepository.getMaximumPriceOfProperty());
				}
				propertyList.addAll(this.filterByPrice(filters.getFromAmount(), filters.getToAmount()));
			}
			if((filters.getFromArea()!=null) || (filters.getToArea()!=null)) {
				if(filters.getFromArea()==null) {
					filters.setFromArea(filterRepository.getMinimumSizeOfProperty()) ;
				}
				if(filters.getToArea()==null) {
					filters.setToArea(filterRepository.getMaximumSizeOfProperty()) ;
				}
				propertyList.addAll(this.filterByArea(filters.getFromArea().toString(),filters.getToArea().toString()));
			}
			if((filters.getPropertyTypes()!=null) && (!filters.getPropertyTypes().isEmpty())) {
				propertyList.addAll(this.filterByPropertyType(filters.getPropertyTypes()));
			}
			if((filters.getSellerNames()!=null) && (!filters.getSellerNames().isEmpty())) {
				propertyList.addAll(this.filterBySellerName(filters.getSellerNames()));
			}
			if(propertyList==null || propertyList.isEmpty() ) {
				throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
			}
			filteredList.addAll(this.filterAlgo(propertyList));
			
			
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("Filter "+exception.getMessage());
			throw exception;
		}
		return filteredList;
	}
	
	@Override
	public List<Property> filterAlgo(List<BigInteger> propertyList) throws Exception{
		Map<BigInteger, Integer> propertyMap = new HashMap<>();
		List<PropertyEntity> sortedPropertyEntity = new ArrayList<>();
		List<Property> finalList = new ArrayList<>();
		try {
			//getting  number of count's for each propertyId
			for (BigInteger property : propertyList) {
				Integer count = propertyMap.getOrDefault(property, 0);
				propertyMap.put(property, count+1);
			}
			//sorting property based on count 
			List<BigInteger> sortedList = propertyMap.entrySet().stream()
			.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
			.map(Map.Entry::getKey)
			.collect(Collectors.toList());
			//getting sorted propertyid's to a list
			for (BigInteger bigInteger : sortedList) {
				sortedPropertyEntity.add(filterRepository.getfilteredProperty(bigInteger)) ;
			}
		  	//calling propertyService method to populate list's in filtered properties
		  	PropertyServiceImpl propertyService = new PropertyServiceImpl();
		  	for (PropertyEntity propertyEntity : sortedPropertyEntity) {
			  	Property newProperty = new Property();
			  	newProperty.setPropertyCent(propertyEntity.getPropertyCent());
			  	newProperty.setPropertyCity(propertyEntity.getPropertyCity());
				newProperty.setPropertyCountry(propertyEntity.getPropertyCountry());
				newProperty.setPropertyDescription(propertyEntity.getPropertyDescription());
				newProperty.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
				newProperty.setPropertyId(propertyEntity.getPropertyId());
				newProperty.setPropertyLandmark(propertyEntity.getPropertyLandmark());
				newProperty.setPropertyLatitude(propertyEntity.getPropertyLatitude());
				newProperty.setPropertyLongitude(propertyEntity.getPropertyLongitude());
				newProperty.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
				newProperty.setPropertyState(propertyEntity.getPropertyState());
				newProperty.setPropertyType(propertyEntity.getPropertyType());
				newProperty.setPropertyCreatedTime(propertyEntity.getPropertyCreatedTime());
				newProperty.setPropertyFeedbackIds(propertyService.feedbackForReturnEntity(propertyEntity.getPropertyFeedbackId()));
				newProperty.setPropertyViewedIds(propertyService.viewedproperty(propertyEntity.getPropertyViewedId()));
				newProperty.setPropertyImageIds(propertyService.imageForReturnProperty(propertyEntity.getPropertyImageId()));
				finalList.add(newProperty);
		  	}
			}catch(ServiceException exception) {
				logg(exception.getMessage());
				throw exception;
			//***** Catch for Exception # # #
			}catch(Exception exception) {
				logg("Filter "+exception.getMessage());
				throw exception;
			}
			return finalList;
	}
	@Override
	public List<BigInteger> filterByCity(List<String> city) throws Exception {
		
		try {
			for(int index=0;index<city.size();index++) {
				if(FLAG.equals(FilterValidator.validateText(city.get(index))))
						throw new ServiceException(SERVICEEXCEPTION_INVALIDCITY);
			}
			return filterRepository.getPropertyByFilteredCity(city);
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("FilterByCity "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public List<BigInteger> filterByCountry(List<String> country) throws ServiceException, Exception {
		
		try {
			for(int index=0;index<country.size();index++) {
				if(FilterValidator.validateText(country.get(index)).equals(FLAG)) {
					throw new ServiceException(SERVICEEXCEPTION_INVALIDCOUNTRY);
				}
			}
			return filterRepository.getPropertyByFilteredCountry(country);
			
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("FilterByCountry "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public List<BigInteger> filterByPrice(String fromPrice, String toPrice) throws ServiceException, Exception {
		
		try {
			if(FLAG.equals(FilterValidator.validateNumber(fromPrice))) 
				throw new ServiceException(SERVICEEXCEPTION_INVALIDPRICE);
			if(FLAG.equals(FilterValidator.validateNumber(toPrice)))
				throw new ServiceException(SERVICEEXCEPTION_INVALIDPRICE);
			return filterRepository.getPropertyByFilteredPrice(BigInteger.valueOf(Integer.parseInt(fromPrice)), BigInteger.valueOf(Integer.parseInt(toPrice)));
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("FilterByPrice "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public List<BigInteger> filterByArea(String fromArea, String toArea) throws ServiceException, Exception {
		
		try {
			if(FLAG.equals(FilterValidator.validateNumber(fromArea))) 
				throw new ServiceException(SERVICEEXCEPTION_INVALIDAREA);
			if(FLAG.equals(FilterValidator.validateNumber(toArea))) 
				throw new ServiceException(SERVICEEXCEPTION_INVALIDAREA);
			return filterRepository.getPropertyByFilteredArea(BigInteger.valueOf(Integer.parseInt(fromArea)), BigInteger.valueOf(Integer.parseInt(toArea)));
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("FilterByArea "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public List<BigInteger> filterByPropertyType(List<String> propertyType) throws ServiceException, Exception {
		
		try {
			for(int index=0;index<propertyType.size();index++) {
				if(FLAG.equals(FilterValidator.validateText(propertyType.get(index))))
					throw new ServiceException(SERVICEEXCEPTION_INVALIDPROPERTYTYPE);
			}
			return filterRepository.getPropertyByFilteredPropertyType(propertyType);
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("filterByPropertyType "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public List<BigInteger> filterByState(List<String> state) throws Exception {
		
		try {
			for(int index=0;index<state.size();index++) {
				if(FLAG.equals(FilterValidator.validateText(state.get(index))))
					throw new ServiceException(SERVICEEXCEPTION_INVALIDSTATE);
			}
			return filterRepository.getPropertyByFilteredState(state);
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("filterByState "+exception.getMessage());
			throw exception;
		}
	}
	@Override
	public List<BigInteger> filterByLandMark(List<String> landMark) throws ServiceException, Exception {
		
		try {
			for(int index=0;index<landMark.size();index++) {
				if(FLAG.equals(FilterValidator.validateTextWithNumber(landMark.get(index))))
					throw new ServiceException(SERVICEEXCEPTION_INVALIDLANDMARK);
			}
			return filterRepository.getPropertyByFilteredLandMark(landMark);
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("filterByLandMark "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	public List<BigInteger> filterBySellerName(List<String> sellerNames) throws ServiceException, Exception {
		try {
			for(int index=0;index<sellerNames.size();index++) {
				if(FLAG.equals(FilterValidator.validateTextWithNumber(sellerNames.get(index).toUpperCase())))
					throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLERNAMES);
			}
			return filterRepository.getPropertyByFilteredSellerName(sellerNames); 
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("filterBySellerNames "+exception.getMessage());
			throw exception;
		}
	}
	
	
	
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		// ***** Logging the message # # #
		log.error(message);
	}

	@Override
	public Map<String, List<String>> getCityByState(String saltstring, UUID userId) throws Exception {
		Map<String, List<String>> finalMap = new HashMap<>();
		try {
			if(userId!=null) {
				UsersEntity userEntity = userRepository.findUserById(userId, saltstring);
				if(userEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOUSER);
				}
			}
			List<String> stateList = filterRepository.getStateFromProperty();
			for (String state : stateList) {
				List<String> cityList = filterRepository.getCityByState(state);
				finalMap.put(state, cityList);
			}
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("getCityByState "+exception.getMessage());
			throw exception;
		}
		return finalMap;
	}
	
	@Override
	public Map<String, List<String>> getSellerNameByCity() throws Exception{
		Map<String, List<String>> finalMap = new HashMap<>();
		try {
			
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("getCityByState "+exception.getMessage());
			throw exception;
		}
		return finalMap;
	}


}
