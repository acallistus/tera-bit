package com.project.terabit.service;



import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Cart;
import com.project.terabit.model.Property;
import com.project.terabit.model.SaveCart;
import com.project.terabit.repository.CartRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.CartValidator;

@Service
@Transactional(readOnly=true)
public class CartServiceImpl implements CartService
{
	
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	CartRepository cartRepository;
	
	@Autowired
	PropertyRepository propertyRepository;
	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	private static final String SERVICEEXCEPTION_CARTEXISTS="CREATECARTSERVICE.CART_ALREADY_EXISTS";
	
	//****** The Constant SERVICEEXCEPTION_NOSALTSTRING. # # # 
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "CART.no_saltstring";
	private static final String SERVICEEXCEPTION_NOCART="CART.no_Cart";
	private static final String SERVICEEXCEPTION_NO_USERID="Cart.No_User_Id";
	private static final String SERVICEEXCEPTION_NO_PROPERTYID="SAVECART.No_Property_Id";
	
	private static final String SERVICEEXCEPTION_NO_USER_EXIST="CART.NO_USER_EXIST";
	private static final String SERVICEEXCEPTION_SALTSTRING_MISSMATCH="CART.SALTSTRING_MISSMATCH";
	private static final String SERVICEEXCEPTION_USER_CARTS_IS_NULL="CART.USER_CART_IS_NULL";
	private static final String SERVICEEXCEPTION_USER_CARTS_IS_EMPTY="CART.USER_CART_IS_EMPTY";
	
	
	@Override
	@Transactional(readOnly=false)
	public List<Cart> saveCart(String saltString, SaveCart cartToBeSaved) throws ServiceException, Exception {
		try {
			
				if(saltString == null)	
				{
					throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
				}
				if(cartToBeSaved==null)
				{
					throw new ServiceException(SERVICEEXCEPTION_NOCART);
				}
				if(cartToBeSaved.getCartUserId()==null)
				{
					throw new ServiceException(SERVICEEXCEPTION_NO_USERID);
				}
				if(cartToBeSaved.getCartPropertyId()==null)
				{
					throw new ServiceException(SERVICEEXCEPTION_NO_PROPERTYID);
				}
			
				UsersEntity cartUserEntity=userRepository.findUserByUserId(cartToBeSaved.getCartUserId());
				if(cartUserEntity==null)
				{
					throw new ServiceException(SERVICEEXCEPTION_NO_USER_EXIST);
				}
				if(!cartUserEntity.getSaltString().equals(saltString))
				{
					
					throw new ServiceException(SERVICEEXCEPTION_SALTSTRING_MISSMATCH);
				}
				if(cartUserEntity.getUserCarts()==null)
				{
					throw new ServiceException(SERVICEEXCEPTION_USER_CARTS_IS_NULL);
				}
		
				List<CartEntity> cartEntityList=cartUserEntity.getUserCarts();
		
				if(!cartEntityList.isEmpty())
					for(CartEntity cartEntity:cartEntityList) {
						if((cartEntity.getCartProperty().getPropertyId()==cartToBeSaved.getCartPropertyId()) && cartEntity.isCartIsActive())
						{
							throw new ServiceException(SERVICEEXCEPTION_CARTEXISTS);
						}
					}
		
				CartEntity cartEntityToBeSaved=new CartEntity();
		 
				
				cartEntityToBeSaved.setCartCreatedTime(LocalDateTime.now());
				cartEntityToBeSaved.setCartIsActive(true);
				
				PropertyEntity propertyEntity = propertyRepository.getPropertyByPropertyId(cartToBeSaved.getCartPropertyId());
				cartEntityToBeSaved.setCartProperty(propertyEntity);
				cartEntityToBeSaved.setCartUserId(cartToBeSaved.getCartUserId());
				cartEntityToBeSaved.setCartModifiedTime(LocalDateTime.now());
				cartRepository.save(cartEntityToBeSaved);
			
	
				cartUserEntity.getUserCarts().add(cartEntityToBeSaved);
				userRepository.save(cartUserEntity);
		
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return this.getCart(saltString,cartToBeSaved.getCartUserId());
	}

	@Override
	@Transactional(readOnly=false)
	public List<Cart> deleteCart(String saltString, SaveCart userCartToBeDeleted) throws ServiceException, Exception {
		
		try {
			if(saltString ==null)	
			{
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(userCartToBeDeleted==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOCART);
			}
			if(userCartToBeDeleted.getCartUserId()==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NO_USERID);
			}
			CartValidator.validate(userCartToBeDeleted.getCartUserId().toString());
			
			
			
			UsersEntity cartUserEntity=userRepository.findUserByUserId(userCartToBeDeleted.getCartUserId());
			
			if(cartUserEntity==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NO_USER_EXIST);
			}
			if(!cartUserEntity.getSaltString().equals(saltString))
			{
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRING_MISSMATCH);
				
			}
			if(cartUserEntity.getUserCarts()==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_USER_CARTS_IS_NULL);
			}
			if(cartUserEntity.getUserCarts().isEmpty()) {
				throw new ServiceException(SERVICEEXCEPTION_USER_CARTS_IS_EMPTY);
				
			}
			if(cartUserEntity.getUserCarts().isEmpty())
			{
				throw new ServiceException(SERVICEEXCEPTION_USER_CARTS_IS_EMPTY);
			}
			for (CartEntity cartEntity : cartUserEntity.getUserCarts()) {
				if(cartEntity.getCartProperty().getPropertyId()==userCartToBeDeleted.getCartPropertyId()) {
					cartEntity.setCartIsActive(false);
				}
			}
			userRepository.save(cartUserEntity);
			 	
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		
		
		return this.getCart(saltString, userCartToBeDeleted.getCartUserId());
	}

	@Override
	@Transactional(readOnly=false)
	public List<Cart> getCart(String saltString, UUID userCart) throws ServiceException, Exception {
		List<Cart> userCartsFetched=new ArrayList<>();
		try {
			
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(userCart==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOCART);
			}
			CartValidator.validate(userCart.toString());
			UsersEntity cartUserEntity=userRepository.findUserByUserId(userCart);
			if(cartUserEntity==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NO_USER_EXIST);
			}

			if(!cartUserEntity.getSaltString().equals(saltString))
			{
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRING_MISSMATCH);
			}
			if(cartUserEntity.getUserCarts()==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_USER_CARTS_IS_NULL);
			}
			if(cartUserEntity.getUserCarts().isEmpty()) {
				throw new ServiceException(SERVICEEXCEPTION_USER_CARTS_IS_EMPTY);
				}
			
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			
			for(CartEntity cartEntity:cartUserEntity.getUserCarts())
			{
				if(cartEntity.isCartIsActive()) {
					PropertyEntity cartPropertyFetchedEntity=propertyRepository.getPropertyByPropertyId(cartEntity.getCartProperty().getPropertyId());
					
					Property cartPropertyFetched=new Property();
					cartPropertyFetched.setPropertyCent(cartPropertyFetchedEntity.getPropertyCent());
					cartPropertyFetched.setPropertyCity(cartPropertyFetchedEntity.getPropertyCity());
					cartPropertyFetched.setPropertyCountry(cartPropertyFetchedEntity.getPropertyCountry());
					cartPropertyFetched.setPropertyState(cartPropertyFetchedEntity.getPropertyState());
					cartPropertyFetched.setPropertyCreatedTime(cartPropertyFetchedEntity.getPropertyCreatedTime());
					cartPropertyFetched.setPropertyModifiedTime(cartPropertyFetchedEntity.getPropertyModifiedTime());
					cartPropertyFetched.setPropertyDescription(cartPropertyFetchedEntity.getPropertyDescription());
					cartPropertyFetched.setPropertyEsteematedAmount(cartPropertyFetchedEntity.getPropertyEsteematedAmount());
					cartPropertyFetched.setPropertyId(cartPropertyFetchedEntity.getPropertyId());
					cartPropertyFetched.setPropertyIsActive(cartPropertyFetchedEntity.isPropertyIsActive());
					cartPropertyFetched.setPropertyLandmark(cartPropertyFetchedEntity.getPropertyLandmark());
					cartPropertyFetched.setPropertyLatitude(cartPropertyFetchedEntity.getPropertyLatitude());
					cartPropertyFetched.setPropertyLongitude(cartPropertyFetchedEntity.getPropertyLongitude());
					cartPropertyFetched.setPropertyType(cartPropertyFetchedEntity.getPropertyType());
					cartPropertyFetched.setPropertyViewedCount(cartPropertyFetchedEntity.getPropertyViewedCount());
					cartPropertyFetched.setPropertyFeedbackIds(loginservice.feedback(cartPropertyFetchedEntity.getPropertyFeedbackId()));
				
					Cart cart=new Cart();
					cart.setCartCreatedTime(cartEntity.getCartCreatedTime());
					cart.setCartId(cartEntity.getCartId());
					cart.setCartIsActive(cartEntity.isCartIsActive());
					cart.setCartUserId(cartEntity.getCartUserId());
					cart.setCartProperty(cartPropertyFetched);
					userCartsFetched.add(cart);
				}
			}
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		return userCartsFetched;
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}
	
	
}



