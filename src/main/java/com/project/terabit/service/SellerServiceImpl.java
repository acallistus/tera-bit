package com.project.terabit.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.CreateSeller;
import com.project.terabit.model.SaveSeller;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.NotificationRepository;

import com.project.terabit.repository.PropertyRepository;

import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.SellerValidator;



/**@author deepik sriram & solai ganesh
/**
 * The Class SellerServiceImpl.
 * CRUD operations for SELLER is implemented in this class
 */
@Service
@Transactional(readOnly=true)

public class SellerServiceImpl implements SellerService{
	

	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	
	/** Autowiring the seller repository. */
	@Autowired
	private SellerRepository sellerRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	NotificationRepository notificationRepository;

	
	@Autowired
	PropertyRepository propertyRepository;

	
	//****UTILITY SERVICE EXCEPTION
	
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "SELLERSERVICE.no_saltstring";	

	
	/** The Constant SERVICEEXCEPTION_NotA_Seller. */
	//***** Message related to Service Exception
	private static final String SERVICEEXCEPTION_INVALIDSELLER = "UPDATESELLERSERVICE.ServiceException_NotA_Seller";
	
	/** The Constant SERVICEEXCEPTION_UserIsNotActiveSeller. */
	private static final String SERVICEEXCEPTION_USERNOTSELLER="UPDATESELLERSERVICE.ServiceException_UserIsNotSeller";
	


	
	/** The Constant SERVICEEXCEPTION_NOUSER. */
	private static final String SERVICEEXCEPTION_NOUSER = "CREATESELLERSERVICE.No_User_Exists";
	
	/** The Constant SERVICEEXCEPTION_EXISTINGSELLER. */
	private static final String SERVICEEXCEPTION_EXISTINGSELLER = "CREATESELLERSERVICE.Seller_already_exists";
	
	/** The Constant SERVICEEXCEPTION_EXISTINGCONTACTNO. */
	private static final String SERVICEEXCEPTION_EXISTINGCONTACTNO = "CREATESELLERSERVICE.ContactNo_already_exists";
	

	/** The Constant SERVICEEXCEPTION_URLMISSMATCH. */
	private static final String SERVICEEXCEPTION_URLMISSMATCH = "CREATESELLERSERVICE.invalid_saltstring";
	
	
	/** The Constant SERVICEEXCEPTION_NOUSERID. */
	private static final String SERVICEEXCEPTION_NOUSERID = "CREATESELLERSERVICE.invalid_user_id";
	
	
	/** The Constant SERVICEEXCEPTION_SELLERISNOTACTIVE. */
	private static final String SERVICEEXCEPTION_SELLERISNOTACTIVE = "DELETESELLERSERVICE.Not_Active_seller";
	
	/** The Constant SERVICEEXCEPTION_USER_IS_NOT_A_SELLER. */
	private static final String SERVICEEXCEPTION_USER_IS_NOT_A_SELLER = "DELETESELLERSERVICE.User_is_not_seller";
	
	private static final String SERVICEEXCEPTION_NOSELLERFORPROPERTY = "GETSELLERBYPROPERTY.no_seller";

	private static final String SERVICEEXCEPTION_NOADMINID = "CREATESELLERSERVICE.no_admin_id";
	
	private static final String SERVICEEXCEPTION_NOADMIN = "CREATESELLERSERVICE.no_admin";
	
	private static final String SERVICEEXCEPTION_INVALIDADMIN = "CREATESELLERSERVICE.invalid_adminid";

	
	private static final String SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS = "UPDATESERVICE.invalid_contactNumber";
	
	private static final String SERVICEEXCEPTION_NOUSERFORSELLER = "GETSELLERBYPROPERTY.seller_not_seller";

	
	private static final String SELLERCREATIONNOTIFICATION = " has requested to be a seller. His mail id is ";
	
	private static final String CONTACTSELLERNOTIFICATION = " has tried to contact you. His mail id is ";
	

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SellerService#createSeller(java.lang.String, com.project.terabit.model.CreateSeller)
	 */
	/** Over riding createSeller of service method.
	 * Creating SELLER for USER
	 */
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User createSeller(String saltstring,CreateSeller createseller) throws Exception {
		//creating sellerentity object to set in the db
		SellerEntity sellerEntityToBeAdded = new SellerEntity();
		
		//declaring sellerentity to retrive value from db 
		SellerEntity sellerEntity;
		User user = new User();
		try {
			
			//checking if saltstring is null
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			//checking if user id is null
			if(createseller.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);

			}
			if(createseller.getSellerRightBy()==null) {
				throw new  ServiceException(SERVICEEXCEPTION_NOADMINID);
			}
			AdminEntity adminEntity = adminRepository.getAdminByAdminId(createseller.getSellerRightBy());
			if(adminEntity==null) {
				throw new  ServiceException(SERVICEEXCEPTION_NOADMIN);
			}

			//retriving user data from user entity
			UsersEntity userEntity = userRepository.findActiveUsers(createseller.getUserId());
			
			//checking is there is no user for given user id
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			
			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			
			//checking if it is a existing seller
			if((userEntity.getUserSellerId()!=null)||(userEntity.isUserIsSeller())){ 
	            throw new ServiceException(SERVICEEXCEPTION_EXISTINGSELLER);
			}


			//checking if the private contact number is there
			if(createseller.getSellerPrivateContact()!=null) {
				
				//validating the details
				SellerValidator.validate(createseller);
				
				//retriving data from seller repository
				sellerEntity = sellerRepository.findSellerByContactNumber(createseller.getSellerPrivateContact());
				
				//checking if there is contact number already exists
				if(sellerEntity!=null) {
					throw new ServiceException(SERVICEEXCEPTION_EXISTINGCONTACTNO);
				}
			}
			//populating value in the sellerentity
			sellerEntityToBeAdded.setSellerCreatedTime(LocalDateTime.now());
			sellerEntityToBeAdded.setSellerIsActive(false);
			sellerEntityToBeAdded.setSellerModifiedTime(LocalDateTime.now());
			sellerEntityToBeAdded.setSellerRightBy(createseller.getSellerRightBy());
			sellerEntityToBeAdded.setSellerPropertyCount(BigInteger.valueOf(0));
			
			//checking if the private contact number is there
			//setting the contact number from front end if passed
			if(createseller.getSellerPrivateContact()==null) {
				sellerEntityToBeAdded.setSellerPrivateContact(userEntity.getUserContactNo());
			}
			
			//setting the contact number from user if not passed from front end
			else if(createseller.getSellerPrivateContact()!=null) {
				SellerValidator.validatePrivateContactNumber(createseller.getSellerPrivateContact());
				sellerEntityToBeAdded.setSellerPrivateContact(createseller.getSellerPrivateContact());
			}
			if(createseller.getSellerCompanyName()==null) {
				sellerEntityToBeAdded.setSellerCompanyName(userEntity.getUserFirstName()+" "+userEntity.getUserLastName());
			}
			else {
				sellerEntityToBeAdded.setSellerCompanyName(createseller.getSellerCompanyName());
			}
			//populating sellerentity in user entity
			userEntity.setUserSellerId(sellerEntityToBeAdded);	
			userEntity.setUserIsSeller(true);
			sellerRepository.save(sellerEntityToBeAdded);
			userRepository.save(userEntity);
			
			String userid = adminRepository.getUserByAdminId(createseller.getSellerRightBy());
			if(userid==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMIN);
			}

			UsersEntity userEntityToNotify = userRepository.findUserByUserId(UUID.fromString(userid));
			NotificationEntity notificationEntity = new NotificationEntity();
			notificationEntity.setNotificationTo(userEntityToNotify.getUserId());
			notificationEntity.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+SELLERCREATIONNOTIFICATION+userEntity.getUserEmailId());
			notificationEntity.setNotificationIsActive(true);
			notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntity.setNotificationViewedTime(LocalDateTime.now());
			notificationRepository.save(notificationEntity);
			
			userEntityToNotify.getUserNotificationIds().add(notificationEntity);
			userRepository.save(userEntityToNotify);
			//creating the seller to be returned
			Seller seller = new Seller();
			
			//populating values from db
			seller.setSellerCreatedTime(sellerEntityToBeAdded.getSellerCreatedTime());
			seller.setSellerId(sellerEntityToBeAdded.getSellerId());
			seller.setSellerPrivateContact(sellerEntityToBeAdded.getSellerPrivateContact());
			seller.setSellerPropertyCount(sellerEntityToBeAdded.getSellerPropertyCount());
			seller.setSellerRightBy(sellerEntityToBeAdded.getSellerRightBy());
			
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			
			user.setUserId(userEntity.getUserId());
			user.setUserSellerId(loginservice.sellerForUser(userEntity));
			user.setUserAuthenticated(userEntity.isIsuserAuthenticated());
			user.setUserCarts(loginservice.cartForUser(userEntity.getUserCarts()));
			user.setUserContactNo(userEntity.getUserContactNo());
			user.setUserEmailId(userEntity.getUserEmailId());
			user.setUserCreatedTime(userEntity.getUserCreatedTime());
			user.setUserFeedbacks(loginservice.feedback(userEntity.getUserFeedbacks()));
			user.setUserFirstName(userEntity.getUserFirstName());
			user.setUserLastName(userEntity.getUserLastName());
			if(userEntity.getUserImage()!=null)
				user.setUserImage(loginservice.imageForUser(userEntity.getUserImage()));
			user.setUserNotificationIds(loginservice.notificationlist(userEntity.getUserNotificationIds()));
			user.setUserIsActive(userEntity.isUserIsActive());
			user.setUserIsSeller(userEntity.isUserIsSeller());
			user.setUserVerifed(userEntity.isUserVerifed());
			user.setUserViewedProperties(loginservice.viewedproperty(userEntity.getUserViewedProperties(), userEntity.getUserId()));
			
			
			//returning the user
			return user;
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
			
			//***** Throwing the ServiceException
			throw serviceException;
		}
		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
			
			//***** Throwing the Exception
			throw exception;
		}
		
	}


	//***** OverRiding the updateSeller method of SellerService
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.SellerService#updateSeller(com.project.terabit.model.SaveSeller, java.lang.String)
	 */
	/** Over riding createSeller of service method.
	 * Updating existing SELLER
	 */
	@Override
	@Transactional(readOnly=false,propagation =Propagation.REQUIRES_NEW)
	public Seller updateSeller(SaveSeller sellerToUpdated,String saltString) throws ServiceException,Exception
	{
	
		try {
			//**** Checking if the saltstring passed is empty
			if(saltString==null) 
			{
				
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
				
			}
			if(sellerToUpdated.getUserId()==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			
			
			Seller sellerUpdated =new Seller();
			
			//***** Getting the UserEntity from User repository JPA # # # 
			UsersEntity sellerToBeUpdatedEntity =userRepository.findActiveUsers(sellerToUpdated.getUserId());
		
			//***** Checking if the user Exist or Not if yes throe an exxception to the user # # #
			if(sellerToBeUpdatedEntity==null)
			{
				
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
		
			//***** if saltString does not match throws a exception # # #
			if(!sellerToBeUpdatedEntity.getSaltString().equals(saltString))
			{
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
		
			//***** if user is not having a sellerObject throw an exception # # #
			if(sellerToBeUpdatedEntity.getUserSellerId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLER);
			}
			//**** throw exception when user is not an active seller # # #
			if(!sellerToBeUpdatedEntity.isUserIsSeller())
			{
				throw new ServiceException(SERVICEEXCEPTION_USERNOTSELLER);
			}

			
			// ***** Validating the Seller Phone Number # # # 
			
			//***** updating the Seller contact # # #
			if(sellerToUpdated.getSellerPrivateContactNumber()!=null) {
				UsersEntity userEntity = userRepository.checkUserContactNumber(sellerToUpdated.getSellerPrivateContactNumber());
				if(userEntity!=null) {
					throw new ServiceException(SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS);
				}
				SellerValidator.validatePrivateContactNumber(sellerToUpdated.getSellerPrivateContactNumber());
				sellerToBeUpdatedEntity.getUserSellerId().setSellerPrivateContact(sellerToUpdated.getSellerPrivateContactNumber());
			}
			if(sellerToUpdated.getCompanyName()!=null) {
				sellerToBeUpdatedEntity.getUserSellerId().setSellerCompanyName(sellerToUpdated.getCompanyName());
			}
			
			//***** saving the changes into the dataBase # # #
			userRepository.save(sellerToBeUpdatedEntity);
			
			sellerUpdated.setSellerCompanyName(sellerToBeUpdatedEntity.getUserSellerId().getSellerCompanyName());
			sellerUpdated.setSellerCreatedTime(sellerToBeUpdatedEntity.getUserSellerId().getSellerCreatedTime());
			sellerUpdated.setSellerId(sellerToBeUpdatedEntity.getUserSellerId().getSellerId());
			sellerUpdated.setSellerPrivateContact(sellerToBeUpdatedEntity.getUserSellerId().getSellerPrivateContact());
			sellerUpdated.setSellerIsActive(sellerToBeUpdatedEntity.getUserSellerId().isSellerIsActive());
			sellerUpdated.setSellerRightBy(sellerToBeUpdatedEntity.getUserSellerId().getSellerRightBy());
			
			
			return sellerUpdated;
			
			
			
			
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.SellerService#deleteSeller(com.project.terabit.model.User, java.lang.String)
	 */
	/** Over riding createSeller of service method.
	 * Deleting the existing SELLER 
	 */	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRES_NEW)
		
	public User deleteSeller(User user, String saltString) throws Exception {
			User userToBeReturned = new User();
			UsersEntity userEntity = new UsersEntity();
			Seller sellerToBeReturned = new Seller();
			try {
					if(saltString==null) 
					{
						throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
					}
					if(user.getUserId()==null) {
						throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
					}
						
					//*****Obtaining UserEntiy Object from User repository # # #
					UsersEntity sellerToBeDeletedEntity=userRepository.findActiveUsers(user.getUserId());
					
					
					
					//***** Checking if the user Exist or Not if yes throw an exception to the user # # #
					if(sellerToBeDeletedEntity==null)
					{
							throw new ServiceException(SERVICEEXCEPTION_NOUSER);
					}
					
					//****** Checking if the saltstring passed matches with the db
					if(!(sellerToBeDeletedEntity.getSaltString().equals(saltString)))
					{
							throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
					}
					
					//***** Check if the user is an existing seller
					if(!sellerToBeDeletedEntity.isUserIsSeller())
					{
						throw new ServiceException(SERVICEEXCEPTION_USER_IS_NOT_A_SELLER);
					}
					
					
					//****** Checking if the seller is active
					if(!sellerToBeDeletedEntity.getUserSellerId().isSellerIsActive())
					{
						throw new ServiceException(SERVICEEXCEPTION_SELLERISNOTACTIVE);
					}
					
					sellerToBeDeletedEntity.setUserIsSeller(false);
					sellerToBeDeletedEntity.getUserSellerId().setSellerIsActive(false);
					List<PropertyEntity> propertyToBeDeleted = propertyRepository.findPropertyBySeller(sellerToBeDeletedEntity.getUserSellerId().getSellerId());

					for(PropertyEntity propertyEntity: propertyToBeDeleted) {
						propertyEntity.setPropertyIsActive(false);
					}
					userEntity = userRepository.save(sellerToBeDeletedEntity);
					sellerToBeReturned.setSellerId(userEntity.getUserSellerId().getSellerId());
					sellerToBeReturned.setSellerIsActive(userEntity.getUserSellerId().isSellerIsActive());
					sellerToBeReturned.setSellerRightBy(userEntity.getUserSellerId().getSellerRightBy());
					userToBeReturned.setUserId(userEntity.getUserId());
					userToBeReturned.setUserSellerId(sellerToBeReturned);

					return userToBeReturned;
					
			}
			//***** Catch for ServiceException # # #
			catch(ServiceException serviceException)
			{
				//***** Logging the ServiceException
				logg(serviceException.getMessage());
						
				//***** Throwing the ServiceException
				throw serviceException;
			}		
			//***** Catch for Exception # # #
			catch(Exception exception)
			{
				//***** Logging the Exception
				logg(exception.getMessage());
						
				//***** Throwing the Exception
				throw exception;
			}
			
					
	}
	
	
	@Override

	public User getSellerByProperty(AddPropertyUser property, String saltstring) throws Exception{

		
		try {
			Seller sellerToBeReturned = new Seller();
			User userToBeReturned = new User();
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			if(saltstring==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}

			if(property.getUserid()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			UsersEntity userEntity = userRepository.findUserById(property.getUserid(),saltstring);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			SellerEntity sellerEntity = sellerRepository.findSellerByPropertyId(property.getReturnPropertyId());
			

			if(sellerEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOSELLERFORPROPERTY);
			}

			UsersEntity returnUserEntity = userRepository.findUserFromSellerId(sellerEntity);
			if(returnUserEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOUSERFORSELLER);
			}

			if(returnUserEntity.getUserAdminId()!=null)
				userToBeReturned.setUserAdminId(loginservice.adminForUser(returnUserEntity));
			if(returnUserEntity.getUserImage()!=null)
				userToBeReturned.setUserImage(loginservice.imageForUser(returnUserEntity.getUserImage()));
			userToBeReturned.setUserEmailId(returnUserEntity.getUserEmailId());
			userToBeReturned.setUserContactNo(returnUserEntity.getUserContactNo());
			userToBeReturned.setUserFirstName(returnUserEntity.getUserFirstName());
			userToBeReturned.setUserLastName(returnUserEntity.getUserLastName());
			userToBeReturned.setUserId(returnUserEntity.getUserId());
			userToBeReturned.setUserVerifed(returnUserEntity.isUserVerifed());
			userToBeReturned.setUserIsSeller(returnUserEntity.isUserIsSeller());
			userToBeReturned.setUserIsActive(returnUserEntity.isUserIsActive());
			userToBeReturned.setUserCreatedTime(returnUserEntity.getUserCreatedTime());
			userToBeReturned.setUserModifiedTime(returnUserEntity.getUserModifiedTime());


			sellerToBeReturned.setSellerCompanyName(sellerEntity.getSellerCompanyName());
			sellerToBeReturned.setSellerId(sellerEntity.getSellerId());
			sellerToBeReturned.setSellerPrivateContact(sellerEntity.getSellerPrivateContact());
			sellerToBeReturned.setSellerPropertyCount(sellerEntity.getSellerPropertyCount());
			sellerToBeReturned.setSellerRightBy(sellerEntity.getSellerRightBy());
			sellerToBeReturned.setSellerCreatedTime(sellerEntity.getSellerCreatedTime());
			if((sellerEntity.getSellerCompanyLogoId())!=null)
				sellerToBeReturned.setSellerCompanyLogoId(loginservice.imageForUser(sellerEntity.getSellerCompanyLogoId()));
			if((sellerEntity.getSellerFeedbackId())!=null)
				sellerToBeReturned.setSellerFeedbackIds(loginservice.feedback(sellerEntity.getSellerFeedbackId()));
			if((sellerEntity.getSellerNotificationId())!=null)
				sellerToBeReturned.setSellerNotificationIds(loginservice.notificationlist(sellerEntity.getSellerNotificationId()));
			if((sellerEntity.getSellerPropertyId())!=null)
				sellerToBeReturned.setSellerPropertyIds(loginservice.propertysetting(sellerEntity.getSellerPropertyId()));


			userToBeReturned.setUserSellerId(sellerToBeReturned);


			NotificationEntity notificationEntity = new NotificationEntity();
			notificationEntity.setNotificationTo(userEntity.getUserId());
			notificationEntity.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+CONTACTSELLERNOTIFICATION+userEntity.getUserEmailId());
			notificationEntity.setNotificationIsActive(true);
			notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntity.setNotificationViewedTime(LocalDateTime.now());
			notificationRepository.save(notificationEntity);


			userEntity.getUserNotificationIds().add(notificationEntity);
			userRepository.save(userEntity);


			return userToBeReturned;


		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
	}
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}

}