 package com.project.terabit.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Notification;
import com.project.terabit.repository.NotificationRepository;
import com.project.terabit.repository.UserRepository;

@Service
@Transactional(readOnly=true)

public class NotificationServiceImpl implements NotificationService {
	
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	NotificationRepository notificationRepository;
	
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	private static final  String SERVICEEXCEPTION_NOTIFICATION_IS_NULL ="NOTIFICATIONSERVICE.no_notification_provided";
	private static final  String SERVICEEXCEPTION_SALTSTRING_IS_NULL="NOTIFICATIONSERVICE.no_saltstring";
	private static final  String SERVICEEXCEPTION_CONTENT_IS_NULL="NOTIFICATIONSERVICE.no_notification_content";
	private static final  String SERVICEEXCEPTION_NOTIFIED_TO_IS_NULL="NOTIFICATIONSERVICE.notified_to_isempty";
	private static final String SERVICEEXCEPTION_URLMISSMATCH = "NOTIFICATIONSERVICE.saltString_mismatch";
	private static final String SERVICEEXCEPTION_NOUSER = "NOTIFICATIONSERVICE.No_User_Exists";
	private static final String SERVICEEXCEPTION_IS_NOT_ACTIVE="NOTIFICATIONSERVICE.NOT_AN_ACTIVE_NOTIFICATION";
	private static final String SERVICEEXCEPTION_NOUSERID = "NOTIFICATIONSERVICE.no_user_id";
	
	private static final String SERVICEEXCEPTION_NO_NOTIFICATION = "NOTIFICATIONSERVICE.no_notification";


	@Override
	@Transactional(readOnly=false)
	public List<Notification> saveNotification(String saltString, Notification notificationToBeSaved)
			throws ServiceException, Exception {
		try {
			
		if(notificationToBeSaved==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_NOTIFICATION_IS_NULL);
		}
		if(saltString==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_SALTSTRING_IS_NULL);
		}
		if(notificationToBeSaved.getNotificationContent()==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_CONTENT_IS_NULL);

		}
		if(notificationToBeSaved.getNotificationTo()==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_NOTIFIED_TO_IS_NULL);
		}
		UsersEntity notifiedUser=userRepository.findUserByUserId(notificationToBeSaved.getNotificationTo());
		
		if(notifiedUser==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_NOUSER);
		}

		if(!notifiedUser.getSaltString().equals(saltString))
		{
			throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
		}
		NotificationEntity notificationToBeAddedEntity=new NotificationEntity();
		
		notificationToBeAddedEntity.setNotificationContent(notificationToBeSaved.getNotificationContent());
		notificationToBeAddedEntity.setNotificationIsActive(true);
		notificationToBeAddedEntity.setNotificationNotifiedTime(LocalDateTime.now());
		notificationToBeAddedEntity.setNotificationViewedTime(LocalDateTime.now());
		notificationToBeAddedEntity.setNotificationTo(notificationToBeSaved.getNotificationTo());
		notificationRepository.save(notificationToBeAddedEntity);
		
		notifiedUser.getUserNotificationIds().add(notificationToBeAddedEntity);
		userRepository.save(notifiedUser);
		
		return this.getNotification(saltString,notificationToBeSaved.getNotificationTo());
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
							
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
							
			//***** Throwing the Exception
			throw exception;
		}
	}

	@Override
	@Transactional(readOnly=false)
	public List<Notification> deleteNotification(String saltString, Notification notificationToBeDeleted)
			throws ServiceException, Exception {
		List<Notification> notificationFetchedList=new ArrayList<>();
		try {
		if(notificationToBeDeleted==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_NOTIFICATION_IS_NULL);
		}
		if(saltString==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_SALTSTRING_IS_NULL);
		}
		
		if(notificationToBeDeleted.getNotificationTo()==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_NOTIFIED_TO_IS_NULL);
		}
		UsersEntity notifiedUserEntity=userRepository.findUserByUserId(notificationToBeDeleted.getNotificationTo());
		if(notifiedUserEntity==null)
		{
			throw new ServiceException(SERVICEEXCEPTION_NOUSER);
		}
		if(!notifiedUserEntity.getSaltString().equals(saltString))
		{
			throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
		}
		NotificationEntity notificationToBeDeletedEntity=notificationRepository.findNotificationByNotificationId(notificationToBeDeleted.getNotificationId(),notificationToBeDeleted.getNotificationTo());
		if(notificationToBeDeletedEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NO_NOTIFICATION);
		}
		if(!notificationToBeDeletedEntity.isNotificationIsActive())
		{
			throw new ServiceException(SERVICEEXCEPTION_IS_NOT_ACTIVE);
		}
		
		

		for (NotificationEntity notificationEntity : notifiedUserEntity.getUserNotificationIds()) {
			if(notificationEntity.getNotificationId().equals(notificationToBeDeleted.getNotificationId())) {
				notificationEntity.setNotificationIsActive(false);
				notificationRepository.save(notificationEntity);
			}
			
		}
		userRepository.save(notifiedUserEntity);
		
		
		for(NotificationEntity notificationEntity:notifiedUserEntity.getUserNotificationIds())
		{
			if(notificationEntity.isNotificationIsActive()) {
				Notification notificationFetched=new Notification();
				notificationFetched.setNotificationContent(notificationEntity.getNotificationContent());
				notificationFetched.setNotificationId(notificationEntity.getNotificationId());
				notificationFetched.setNotificationIsActive(notificationEntity.isNotificationIsActive());
				notificationFetched.setNotificationNotifiedTime(notificationEntity.getNotificationNotifiedTime());
				notificationFetched.setNotificationTo(notificationEntity.getNotificationTo());
				notificationFetched.setNotificationViewedTime(notificationEntity.getNotificationViewedTime());
				notificationFetchedList.add(notificationFetched);
			}
		}
		
		
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
							
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
							
			//***** Throwing the Exception
			throw exception;
		}
		return notificationFetchedList;

	}

	@Override
	public List<Notification> getNotification(String saltString, UUID userNotifications)
			throws ServiceException, Exception {
		List<Notification> notificationFetchedList=new ArrayList<>();
		try {
			if(userNotifications==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			if(saltString==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRING_IS_NULL);
			}
			UsersEntity notifiedUserEntity=userRepository.findUserByUserId(userNotifications);
			
			if(notifiedUserEntity==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			if(!notifiedUserEntity.getSaltString().equals(saltString))
			{
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			
			List<NotificationEntity> notificationEntityList=notificationRepository.findNotificationByUserId(userNotifications);
			
			
			for(NotificationEntity notificationEntity:notificationEntityList)
			{
				Notification notificationFetched=new Notification();
				notificationFetched.setNotificationContent(notificationEntity.getNotificationContent());
				notificationFetched.setNotificationId(notificationEntity.getNotificationId());
				notificationFetched.setNotificationIsActive(notificationEntity.isNotificationIsActive());
				notificationFetched.setNotificationNotifiedTime(notificationEntity.getNotificationNotifiedTime());
				notificationFetched.setNotificationTo(notificationEntity.getNotificationTo());
				notificationFetched.setNotificationViewedTime(notificationEntity.getNotificationViewedTime());
				notificationFetchedList.add(notificationFetched);
			}
		}
		catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("Notification "+exception.getMessage());
			throw exception;
		}
		
		return notificationFetchedList;
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		// ***** Logging the message # # #
		log.error(message);
	}

}
