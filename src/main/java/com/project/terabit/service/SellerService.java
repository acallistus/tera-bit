package com.project.terabit.service;

import org.springframework.stereotype.Service;

import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.CreateSeller;
import com.project.terabit.model.SaveSeller;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;

@Service
public interface SellerService {

	public Seller updateSeller(SaveSeller updateSeller,String saltString) throws Exception;

	public User createSeller(String saltstring,CreateSeller createseller) throws Exception;
	
	public User deleteSeller(User user,String saltString) throws Exception;
	

	public User getSellerByProperty(AddPropertyUser propertyId, String saltstring) throws Exception;


	}
