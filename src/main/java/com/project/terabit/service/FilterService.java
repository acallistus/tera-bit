package com.project.terabit.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import com.project.terabit.model.FilterProperty;
import com.project.terabit.model.Property;

@Service
public interface FilterService {

	public List<Property> getPropertiesForTheFilter(FilterProperty property, String saltstring) throws ServiceException, Exception;
	
	public List<Property> filterAlgo(List<BigInteger> propertyList) throws ServiceException, Exception;
	
	public List<BigInteger> filterByCity(List<String> city) throws ServiceException,Exception;
	
	public List<BigInteger> filterByCountry(List<String> country) throws ServiceException,Exception;
	
	public List<BigInteger> filterByPrice(String fromPrice, String toPrice) throws ServiceException,Exception;
	
	public List<BigInteger> filterByArea(String fromArea, String toArea) throws ServiceException,Exception;
	
	public List<BigInteger> filterByPropertyType(List<String> propertyType) throws ServiceException,Exception;
	
	public List<BigInteger> filterByState(List<String> state) throws ServiceException,Exception;
	
	public List<BigInteger> filterByLandMark(List<String> landMark) throws ServiceException, Exception;
	
	public List<BigInteger> filterBySellerName(List<String> sellerNames) throws Exception;
	
	public Map<String, List<String>> getCityByState(String saltstring,UUID userId) throws Exception;
	
	public Map<String, List<String>> getSellerNameByCity() throws Exception;
}
