package com.project.terabit.service;

import java.time.LocalDateTime;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.UsersEntity;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.AuthorizationIdValidator;



@Service
@Transactional(readOnly=true)
public class VerifyAuthorizationServiceImpl implements VerifyAuthorizationService {
	
	@Autowired
	UserRepository userrepository;
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	private static final String SERVICEEXCEPTIONNOAUTHORIZATIONID="VERIFYAUTHORIZATIONSERVICE.authorization_id_is_Empty";
	private static final String SERVICEEXCEPTION_NOUSER = "VERIFYAUTHORIZATIONSERVICE.no_user_exists";
	private static final String SERVICEEXCEPTION_VERIFIEDUSER = "VERIFYAUTHORIZATIONSERVICE.already_verified_user";
	private static final String SERVICEEXCEPTION_VERIFICATIONTIMEOUT="SERVICE.User_Cannot_Be_Verified";

	
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public void userVerification(String authorizationid) throws ServiceException,Exception{
			
			try {
					if(authorizationid==null)
						{
							throw new ServiceException(SERVICEEXCEPTIONNOAUTHORIZATIONID);
						}
					log.info(authorizationid);
					AuthorizationIdValidator.ValidateAuthorizationId(authorizationid);
					UsersEntity usertobeverifiedEntity=userrepository.findUserFromAuthorizationId(authorizationid);
					if(usertobeverifiedEntity==null)
						{
							throw new ServiceException(SERVICEEXCEPTION_NOUSER);
						}
					if(usertobeverifiedEntity.isUserVerifed())
						{
							throw new ServiceException(SERVICEEXCEPTION_VERIFIEDUSER);
						}
					if(usertobeverifiedEntity.getUserModifiedTime().compareTo(LocalDateTime.now().minusMinutes(15))<0)
						{
							throw new ServiceException(SERVICEEXCEPTION_VERIFICATIONTIMEOUT);
						}
					usertobeverifiedEntity.setUserVerifed(true);
					userrepository.save(usertobeverifiedEntity);
					
		
			
		}
		catch(ServiceException exception)
		{
			logg(exception.getMessage());
			
			throw exception;
		}
		catch(Exception exception) {
				logg("verify "+exception.getMessage());
				throw exception;
			}
	}
	
	private void logg(String message) {
		log.error(message);
	}


}
