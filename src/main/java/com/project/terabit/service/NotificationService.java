package com.project.terabit.service;

import java.util.List;
import java.util.UUID;

import com.project.terabit.model.Notification;


public interface NotificationService {
	public List<Notification> saveNotification(String saltString,Notification notification) throws ServiceException,Exception;
	public List<Notification> deleteNotification(String saltString ,Notification notification) throws ServiceException,Exception;
	public List<Notification> getNotification(String saltString,UUID userNotified) throws ServiceException,Exception;
}
