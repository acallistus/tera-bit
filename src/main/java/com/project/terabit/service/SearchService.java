package com.project.terabit.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.project.terabit.model.Property;
import com.project.terabit.model.Search;
import com.project.terabit.model.User;

@Component
public interface SearchService {

	public List<Property> search(Search search, String saltstring) throws Exception;
	
	public User searchForSeller(User user, String saltstring) throws Exception;

}
