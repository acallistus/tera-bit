package com.project.terabit.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.entity.ViewedPropertyEntity;
import com.project.terabit.model.Property;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.repository.NotificationRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.repository.ViewedPropertyRepository;
import com.project.terabit.utility.LoggerImpl;

@Service
@Transactional(readOnly=true)
public class ViewedPropertyServiceImpl implements ViewedPropertyService{
	
	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	LoggerImpl logger = new LoggerImpl();
	
	@Autowired
	PropertyRepository propertyRepository;
	
	@Autowired
	ViewedPropertyRepository viewedPropertyRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	SellerRepository sellerRepository;
	
	@Autowired
	NotificationRepository notificationRepository;
	
	/** The Constant SERVICEEXCEPTION_NOSALTSTRING. */
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "UTILITYSERVICE.no_saltstring";	
	
	private static final String SERVICEEXCEPTION_NOPROPERTYID = "CREATEVIEWEDPROPERTY.no_property_id";
	
	private static final String SERVICEEXCEPTION_NOUSERID = "CREATEVIEWEDPROPERTY.no_user_id";
	
	private static final String SERVICEEXCEPTION_NOPROPERTY = "UTILITYSERVICE.no_property";
	
	private static final String SERVICEEXCEPTION_NOSELLER = "UTILITYSERVICE.no_seller";

	
	private static final String VIEWEDPROPERTYNOTIFICATION = " viewed your property with address ";

	
	private static final String SERVICEEXCEPTION_USERIDEXCEPTION = "UTILITYSERVICE.invalid_user";
	

	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Property createViewedProperty(ViewedProperty viewedProperty, String saltString) throws Exception {
		Property propertyToBeReturned = new Property();
		ViewedPropertyEntity viewedPropertyEntity = new ViewedPropertyEntity();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(viewedProperty.getViewedUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			UsersEntity userEntity = userRepository.findUserById(viewedProperty.getViewedUserId(), saltString);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}
		
			if(viewedProperty.getViewedPropertyPropertyId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOPROPERTYID);
			}
			PropertyEntity propertyEntity = propertyRepository.getPropertyForViewedProperty(viewedProperty.getViewedPropertyPropertyId().getPropertyId());
			if(propertyEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
			}
			SellerEntity sellerEntity = sellerRepository.findSellerByPropertyId(viewedProperty.getViewedPropertyPropertyId().getPropertyId());
			if(sellerEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSELLER);
			}

			ViewedPropertyEntity alreadyViewedPropertyEntity = viewedPropertyRepository.findDistinctProperty(viewedProperty.getViewedPropertyPropertyId().getPropertyId(), viewedProperty.getViewedUserId());
			if(alreadyViewedPropertyEntity==null) {
				viewedPropertyEntity.setViewedTime(LocalDateTime.now());
				viewedPropertyEntity.setViewedSellerId(sellerEntity.getSellerId());
				viewedPropertyEntity.setViewedUserId(viewedProperty.getViewedUserId());
				viewedPropertyEntity.setViewedPropertyPropertyId(propertyEntity);
				viewedPropertyRepository.save(viewedPropertyEntity);
				
				userEntity.getUserViewedProperties().add(viewedPropertyEntity);
				userRepository.save(userEntity);
				
				propertyEntity.setPropertyViewedCount(propertyEntity.getPropertyViewedCount().add(BigInteger.valueOf(1l)));
				propertyRepository.save(propertyEntity);
				
				UsersEntity userForSellerId = userRepository.findUserFromSellerId(sellerEntity);
				NotificationEntity notificationEntity = new NotificationEntity();
				notificationEntity.setNotificationTo(userForSellerId.getUserId());
				notificationEntity.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+VIEWEDPROPERTYNOTIFICATION+propertyEntity.getPropertyLandmark()+", "+propertyEntity.getPropertyCity()+", "+propertyEntity.getPropertyState()+", "+propertyEntity.getPropertyCountry()+" . Viewed By "+userEntity.getUserEmailId());

				notificationEntity.setNotificationIsActive(true);
				notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
				notificationEntity.setNotificationViewedTime(LocalDateTime.now());
				notificationRepository.save(notificationEntity);
				
				userForSellerId.getUserNotificationIds().add(notificationEntity);
				userRepository.save(userForSellerId);

			}

						
			propertyToBeReturned.setPropertyId(propertyEntity.getPropertyId());
			propertyToBeReturned.setPropertyCent(propertyEntity.getPropertyCent());
			propertyToBeReturned.setPropertyCity(propertyEntity.getPropertyCity());
			propertyToBeReturned.setPropertyCountry(propertyEntity.getPropertyCountry());
			propertyToBeReturned.setPropertyDescription(propertyEntity.getPropertyDescription());
			propertyToBeReturned.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
			propertyToBeReturned.setPropertyLandmark(propertyEntity.getPropertyLandmark());
			propertyToBeReturned.setPropertyLatitude(propertyEntity.getPropertyLatitude());
			propertyToBeReturned.setPropertyLongitude(propertyEntity.getPropertyLongitude());
			propertyToBeReturned.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
			propertyToBeReturned.setPropertyState(propertyEntity.getPropertyState());
			propertyToBeReturned.setPropertyType(propertyEntity.getPropertyType());
			propertyToBeReturned.setPropertyViewedCount(propertyEntity.getPropertyViewedCount());
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logger.logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logger.logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		return propertyToBeReturned;
	}

	@Override
	public List<Property> getViewedPropertyForUser(UUID userId, String saltString) throws Exception {
		List<Property> propertyList = new ArrayList<>();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(userId==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			UsersEntity userEntity = userRepository.findUserById(userId, saltString);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}
			List<BigInteger> propertyidList = viewedPropertyRepository.getPropertyIdByUserId(userId);
			if(!propertyidList.isEmpty()) {
				List<PropertyEntity> propertyEntityForUser = propertyRepository.getPropertiesByPropertyId(propertyidList);
				LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
				for (PropertyEntity propertyEntity : propertyEntityForUser) {
						Property propertyToBeReturned = new Property();
						propertyToBeReturned.setPropertyId(propertyEntity.getPropertyId());
						propertyToBeReturned.setPropertyCent(propertyEntity.getPropertyCent());
						propertyToBeReturned.setPropertyCity(propertyEntity.getPropertyCity());
						propertyToBeReturned.setPropertyCountry(propertyEntity.getPropertyCountry());
						propertyToBeReturned.setPropertyDescription(propertyEntity.getPropertyDescription());
						propertyToBeReturned.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
						propertyToBeReturned.setPropertyLandmark(propertyEntity.getPropertyLandmark());
						propertyToBeReturned.setPropertyLatitude(propertyEntity.getPropertyLatitude());
						propertyToBeReturned.setPropertyLongitude(propertyEntity.getPropertyLongitude());
						propertyToBeReturned.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
						propertyToBeReturned.setPropertyState(propertyEntity.getPropertyState());
						propertyToBeReturned.setPropertyType(propertyEntity.getPropertyType());
						propertyToBeReturned.setPropertyViewedCount(propertyEntity.getPropertyViewedCount());
						propertyToBeReturned.setPropertyFeedbackIds(loginservice.feedback(propertyEntity.getPropertyFeedbackId()));
						propertyToBeReturned.setPropertyImageIds(loginservice.image(propertyEntity.getPropertyImageId()));
						propertyList.add(propertyToBeReturned);
					
				}
				}
				
			
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logger.logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logger.logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return propertyList;
	}
}
