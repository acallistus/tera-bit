package com.project.terabit.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.entity.ViewedPropertyEntity;
import com.project.terabit.model.Admin;
import com.project.terabit.model.Cart;
import com.project.terabit.model.Feedback;
import com.project.terabit.model.Image;
import com.project.terabit.model.Notification;
import com.project.terabit.model.Property;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.utility.PasswordHashing;
import com.project.terabit.utility.RandomStringGenerator;
import com.project.terabit.validator.LoginUserValidator;


/**
 * The Class LoginService.
 */
@Service
public class LoginUserServiceImpl implements LoginUserService {
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The userrepository. */
	@Autowired
	UserRepository userrepository;
	
	/** The propertyrepository. */
	@Autowired
	PropertyRepository propertyrepository;
	
	
	/** The Constant ServiceExceptionNoUser. */
	private static final String SERVICEEXCEPTION_NOUSER="SERVICE.No_User_Exists";
	
	/** The Constant SERVICEEXCEPTION_NODETAILSENTERED. */
	private static final String SERVICEEXCEPTION_NODETAILSENTERED="Service.Input_Data_Missing";
	
	/** The Constant ServiceExceptionUsernamePasswordMissMatch. */
	private static final String SERVICEEXCEPTION_USERNAMEMISMATCH="SERVICE.Username_Password_MissMatch";
	
	/** The Constant SERVICEEXCEPTION_USERNOTACTIVE. */
	private static final String SERVICEEXCEPTION_USERNOTACTIVE="SERVICE.User_Not_Active";
	
	/** The Constant SERVICEEXCEPTION_LOGINSUCCESS. */
	
	private static final String SERVICEEXCEPTION_VERIFICATIONNOTDONE="SERVICE.user_not_verified";
	
	private static final String SERVICEEXCEPTION_LOGG="login successful for user id:";

	
	
	/**
	 * Login.
	 *
	 * @param userModel the user model
	 * @return the user
	 * @throws Exception the exception
	 */
	@Transactional(readOnly=false)
	public User login(User userModel) throws Exception {
		User user = new User();
	
	try {
		
		if((userModel.getUserEmailId()!=null || userModel.getUserContactNo()!=null) && userModel.getUserPassword()!=null) {
		
		LoginUserValidator.validate(userModel);
		
		PasswordHashing passwordHasher = new PasswordHashing();
		
		RandomStringGenerator saltstringgenerator=new RandomStringGenerator();
		
		UsersEntity receivedUserEntity=null;
		
		if(userModel.getUserEmailId()!=null){
			receivedUserEntity=this.getUserByEmailId(userModel.getUserEmailId());
		}
		
		else {
			receivedUserEntity=this.getUserByContactNumber(userModel.getUserContactNo());
		}
		
		if(receivedUserEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOUSER);
		}
		
		if(!receivedUserEntity.isUserVerifed()) {
			throw new ServiceException(SERVICEEXCEPTION_VERIFICATIONNOTDONE);
		}
		
		String hashedInputpassword=passwordHasher.hashing(userModel.getUserPassword());
		if(!receivedUserEntity.isUserIsActive()) {
			throw new ServiceException (SERVICEEXCEPTION_USERNOTACTIVE);
		}
		if(!checkPassword(hashedInputpassword,receivedUserEntity.getUserPassword())){
			throw new ServiceException(SERVICEEXCEPTION_USERNAMEMISMATCH);
		}
				user.setSaltstring(saltstringgenerator.randomString());
				user.setUserAuthenticated(receivedUserEntity.isIsuserAuthenticated());
				user.setUserContactNo(receivedUserEntity.getUserContactNo());
				user.setUserCreatedTime(receivedUserEntity.getUserCreatedTime());
				user.setUserEmailId(receivedUserEntity.getUserEmailId());
				user.setUserFirstName(receivedUserEntity.getUserFirstName());
				user.setUserId(receivedUserEntity.getUserId());
				user.setUserIsActive(receivedUserEntity.isUserIsActive());
				user.setUserIsSeller(receivedUserEntity.isUserIsSeller());
				user.setUserLastName(receivedUserEntity.getUserLastName());
				user.setUserModifiedTime(receivedUserEntity.getUserModifiedTime());
				user.setUserPin(receivedUserEntity.getUserPin());
				user.setUserVerifed(receivedUserEntity.isUserVerifed());
				//admin for user
				if(receivedUserEntity.getUserAdminId()!=null) {
					user.setUserAdminId(this.adminForUser(receivedUserEntity));
				}
				//image to user
				if(receivedUserEntity.getUserImage()!=null) {
					user.setUserImage(this.imageForUser(receivedUserEntity.getUserImage()));
				}
				//cart to user
				if(!receivedUserEntity.getUserCarts().isEmpty()) {	
					user.setUserCarts(this.cartForUser(receivedUserEntity.getUserCarts()));
				}
				//seller to user
				if(receivedUserEntity.getUserSellerId()!=null) {
					//setting seller to user
					user.setUserSellerId(this.sellerForUser(receivedUserEntity));
				}
				if(!receivedUserEntity.getUserViewedProperties().isEmpty()) {
				// setting viewed property
				user.setUserViewedProperties(this.viewedproperty(receivedUserEntity.getUserViewedProperties(),receivedUserEntity.getUserId()));
				}
				
				//setting Feedback of user from user entity
				if(!receivedUserEntity.getUserFeedbacks().isEmpty()) {
					user.setUserFeedbacks(this.feedback(receivedUserEntity.getUserFeedbacks()));
				}

				
				if(!receivedUserEntity.getUserNotificationIds().isEmpty()) {
					user.setUserNotificationIds(this.notificationlist(receivedUserEntity.getUserNotificationIds()));
				}

				this.setSaltString(receivedUserEntity, user.getSaltstring());
				if(userModel.getUserEmailId()!=null) {
					log.info(SERVICEEXCEPTION_LOGG+userModel.getUserEmailId());
				}
				if(userModel.getUserContactNo()!=null) {
					log.info(SERVICEEXCEPTION_LOGG+userModel.getUserContactNo());
				}
		}
		else {
			throw new ServiceException(SERVICEEXCEPTION_NODETAILSENTERED);
		}
	}
	catch(ServiceException exception) {
		logg(exception.getMessage());
		throw exception;
	}catch(Exception exception) {
		logg("Login "+exception.getMessage());
		throw exception;
	}
	return user;
	
	}
	
	
	
	/**
	 * Admin for user.
	 *
	 * @param receivedUserEntity the received user entity
	 * @return the admin
	 */
	public Admin adminForUser(UsersEntity receivedUserEntity){
		try {
			if(receivedUserEntity.getUserAdminId().isAdminIsActive()) {
				Admin admin=new Admin();
				admin.setAdminCreatedTime(receivedUserEntity.getUserAdminId().getAdminCreatedTime());
				admin.setAdminId(receivedUserEntity.getUserAdminId().getAdminId());
				admin.setAdminIsActive(receivedUserEntity.getUserAdminId().isAdminIsActive());
				admin.setAdminRightsBy(receivedUserEntity.getUserAdminId().getAdminRightsBy());
				admin.setAdminSellerCount(receivedUserEntity.getUserAdminId().getAdminSellerCount());
				if(!receivedUserEntity.getUserAdminId().getAdminSellerIds().isEmpty()) {
					List<Seller> adminsellerList=new ArrayList<>();
					for (SellerEntity sellerentity : receivedUserEntity.getUserAdminId().getAdminSellerIds()) {
						adminsellerList.add(this.sellerForAdmin(sellerentity));
					}
					admin.setAdminSellerIds(adminsellerList);
				}
				if(!receivedUserEntity.getUserAdminId().getAdminFeedbackIds().isEmpty()) {
					admin.setAdminFeedbackIds(this.feedback(receivedUserEntity.getUserAdminId().getAdminFeedbackIds()));
				}
				if(!receivedUserEntity.getUserAdminId().getAdminNotificationIds().isEmpty()) {
					admin.setAdminNotificationIds(this.notificationlist(receivedUserEntity.getUserAdminId().getAdminNotificationIds()));
				}
				return admin;
			}return null;
		}catch(Exception exception) {
			logg("adminForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	
	/**
	 * Feedback.
	 *
	 * @param feedbackentitylist the feedbackentitylist
	 * @return the list
	 */
	public List<Feedback> feedback(List<FeedbackEntity> feedbackentitylist){
		try {
		List<Feedback> feedbacklist=new ArrayList<>();
		for (FeedbackEntity feedback1 : feedbackentitylist) {
			Feedback feedbackToAddToAdmin= new Feedback();
			feedbackToAddToAdmin.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
			feedbackToAddToAdmin.setFeedbackDescription(feedback1.getFeedbackDescription());
			feedbackToAddToAdmin.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
			feedbackToAddToAdmin.setFeedbackId(feedback1.getFeedbackId());
			feedbackToAddToAdmin.setFeedbackRating(feedback1.getFeedbackRating());
			feedbackToAddToAdmin.setFeedbackModifiedTime(feedback1.getFeedbackModifiedTime());
			feedbacklist.add(feedbackToAddToAdmin);
		}
		return feedbacklist;
		}catch(Exception exception) {
			logg("feedback "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Viewedproperty.
	 *
	 * @param viewedPropertyList the viewed property list
	 * @param vieweduserId the vieweduser id
	 * @return the list
	 */
	public List<ViewedProperty> viewedproperty (List<ViewedPropertyEntity> viewedPropertyList,UUID vieweduserId){
		try {
		List<ViewedProperty> viewedPropertyOfSeller=new ArrayList<>();
		for (ViewedPropertyEntity viewedproperty : viewedPropertyList) {
			ViewedProperty viewproperty=new ViewedProperty();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(this.propertyFromPropertyEntity(viewedproperty.getViewedPropertyPropertyId()));
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfSeller.add(viewproperty);
		}
		return viewedPropertyOfSeller;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Image for user.
	 *
	 * @param userImageEntity the user image entity
	 * @return the image
	 */
	public Image imageForUser(ImageEntity userImageEntity) {
		try {
			if(userImageEntity.isImageIsActive()) {
				Image image= new Image();
				image.setImageCreatedTime(userImageEntity.getImageCreatedTime());
				image.setImageDescription(userImageEntity.getImageDescription());
				image.setImageId(userImageEntity.getImageId());
				image.setImageIsActive(userImageEntity.isImageIsActive());
				image.setImagePath(userImageEntity.getImagePath());
				return image;
				}return null;
		}catch(Exception exception) {
			logg("imageForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Cart for user.
	 *
	 * @param cartentitylist the cartentitylist
	 * @return the list
	 */
	public List<Cart> cartForUser(List<CartEntity> cartentitylist){
		try {
		List<Cart> cartListToBeSet= new ArrayList<>();

		for (CartEntity cartEntity : cartentitylist) {
			if(cartEntity.isCartIsActive()) {
			Cart cart = new Cart();
			cart.setCartCreatedTime(cartEntity.getCartCreatedTime());
			cart.setCartId(cartEntity.getCartId());
			cart.setCartIsActive(cartEntity.isCartIsActive());
			if(cartEntity.getCartProperty()!=null) {
				PropertyEntity propertyentity=propertyrepository.findPropertyByPropertyId(cartEntity.getCartProperty().getPropertyId());
				cart.setCartProperty(this.propertysettingforreturnmodel(propertyentity));
				
			}
			cart.setCartUserId(cartEntity.getCartUserId());
			cartListToBeSet.add(cart);
			}

		}
		return cartListToBeSet;
		}catch(Exception exception) {
			logg("cartForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Notificationlist.
	 *
	 * @param notificationentitylist the notificationentitylist
	 * @return the list
	 */
	public List<Notification> notificationlist(List<NotificationEntity> notificationentitylist){
		try {
		List<Notification> notificationListToBeSetToSeller=new ArrayList<>();
		for (NotificationEntity notificationEntity : notificationentitylist) {
			if(notificationEntity.isNotificationIsActive()) {
				Notification notification = new Notification();
				notification.setNotificationContent(notificationEntity.getNotificationContent());
				notification.setNotificationId(notificationEntity.getNotificationId());
				notification.setNotificationIsActive(notificationEntity.isNotificationIsActive());
				notification.setNotificationNotifiedTime(notificationEntity.getNotificationNotifiedTime());
				notification.setNotificationTo(notificationEntity.getNotificationTo());
				notification.setNotificationViewedTime(notificationEntity.getNotificationViewedTime());
				notificationListToBeSetToSeller.add(notification);
			}
		}
		return notificationListToBeSetToSeller;
		}catch(Exception exception) {
			logg("notificationlist "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Seller for user.
	 *
	 * @param receivedUserEntity the received user entity
	 * @return the seller
	 */
	public Seller sellerForUser(UsersEntity receivedUserEntity) {
		try {
			if(receivedUserEntity.getUserSellerId().isSellerIsActive()) {
				Seller sellerToBeSetToUser = new Seller();
				sellerToBeSetToUser.setSellerCompanyName(receivedUserEntity.getUserSellerId().getSellerCompanyName());
				sellerToBeSetToUser.setSellerCreatedTime(receivedUserEntity.getUserSellerId().getSellerCreatedTime());
				sellerToBeSetToUser.setSellerId(receivedUserEntity.getUserSellerId().getSellerId());
				sellerToBeSetToUser.setSellerIsActive(receivedUserEntity.getUserSellerId().isSellerIsActive());
				sellerToBeSetToUser.setSellerPrivateContact(receivedUserEntity.getUserSellerId().getSellerPrivateContact());
				sellerToBeSetToUser.setSellerPropertyCount(receivedUserEntity.getUserSellerId().getSellerPropertyCount());
				sellerToBeSetToUser.setSellerRightBy(receivedUserEntity.getUserSellerId().getSellerRightBy());
				if(receivedUserEntity.getUserSellerId().getSellerCompanyLogoId()!=null) {
					sellerToBeSetToUser.setSellerCompanyLogoId(this.imageForUser(receivedUserEntity.getUserSellerId().getSellerCompanyLogoId()));
				}
				if((!receivedUserEntity.getUserSellerId().getSellerFeedbackId().isEmpty())){
					sellerToBeSetToUser.setSellerFeedbackIds(this.feedback(receivedUserEntity.getUserSellerId().getSellerFeedbackId()));
				}
				if(!receivedUserEntity.getUserSellerId().getSellerNotificationId().isEmpty()) {
					sellerToBeSetToUser.setSellerNotificationIds(this.notificationlist(receivedUserEntity.getUserSellerId().getSellerNotificationId()));
				}
				if(!receivedUserEntity.getUserSellerId().getSellerPropertyId().isEmpty()) {
					sellerToBeSetToUser.setSellerPropertyIds(this.propertysetting(receivedUserEntity.getUserSellerId().getSellerPropertyId()));
				}
				return sellerToBeSetToUser;
			}return null;
		}
		catch(Exception exception) {
			logg("sellerForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Seller for admin.
	 *
	 * @param receivedUserEntity the received user entity
	 * @return the seller
	 */
	public Seller sellerForAdmin(SellerEntity receivedUserEntity) {
		try {
			if(receivedUserEntity.isSellerIsActive()) {
				Seller sellerToBeSetToUser = new Seller();
				sellerToBeSetToUser.setSellerCompanyName(receivedUserEntity.getSellerCompanyName());
				sellerToBeSetToUser.setSellerCreatedTime(receivedUserEntity.getSellerCreatedTime());
				sellerToBeSetToUser.setSellerId(receivedUserEntity.getSellerId());
				sellerToBeSetToUser.setSellerIsActive(receivedUserEntity.isSellerIsActive());
				sellerToBeSetToUser.setSellerPrivateContact(receivedUserEntity.getSellerPrivateContact());
				sellerToBeSetToUser.setSellerPropertyCount(receivedUserEntity.getSellerPropertyCount());
				sellerToBeSetToUser.setSellerRightBy(receivedUserEntity.getSellerRightBy());
				if(receivedUserEntity.getSellerCompanyLogoId()!=null) {
					sellerToBeSetToUser.setSellerCompanyLogoId(this.imageForUser(receivedUserEntity.getSellerCompanyLogoId()));
				}
				if((!receivedUserEntity.getSellerFeedbackId().isEmpty())){
					sellerToBeSetToUser.setSellerFeedbackIds(this.feedback(receivedUserEntity.getSellerFeedbackId()));
				}
				if(!receivedUserEntity.getSellerNotificationId().isEmpty()) {
					sellerToBeSetToUser.setSellerNotificationIds(this.notificationlist(receivedUserEntity.getSellerNotificationId()));
				}
				if(!receivedUserEntity.getSellerPropertyId().isEmpty()) {
					sellerToBeSetToUser.setSellerPropertyIds(this.propertysetting(receivedUserEntity.getSellerPropertyId()));
				}
				return sellerToBeSetToUser;
			}return null;
		}
		catch(Exception exception) {
			logg("sellerForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Propertysetting.
	 *
	 * @param propertyEntityList the property entity list
	 * @return the list
	 */
	public List<Property> propertysetting(List<PropertyEntity> propertyEntityList){
		try {
		List<Property> propertyListToSetToSeller=new ArrayList<>();
		for (PropertyEntity property : propertyEntityList) {
			if(property.isPropertyIsActive()) {
				Property propertyToSetToSeller = new Property();
				propertyToSetToSeller.setPropertyCent(property.getPropertyCent());
				propertyToSetToSeller.setPropertyCity(property.getPropertyCity());
				propertyToSetToSeller.setPropertyCountry(property.getPropertyCountry());
				propertyToSetToSeller.setPropertyCreatedTime(property.getPropertyCreatedTime());
				propertyToSetToSeller.setPropertyDescription(property.getPropertyDescription());
				propertyToSetToSeller.setPropertyEsteematedAmount(property.getPropertyEsteematedAmount());
				propertyToSetToSeller.setPropertyId(property.getPropertyId());
				propertyToSetToSeller.setPropertyIsActive(property.isPropertyIsActive());
				propertyToSetToSeller.setPropertyLandmark(property.getPropertyLandmark());
				propertyToSetToSeller.setPropertyLatitude(property.getPropertyLongitude());
				propertyToSetToSeller.setPropertyModifiedTime(property.getPropertyModifiedTime());
				propertyToSetToSeller.setPropertyOwnedBy(property.getPropertyOwnedBy());
				propertyToSetToSeller.setPropertyState(property.getPropertyState());
				propertyToSetToSeller.setPropertyType(property.getPropertyType());
				propertyToSetToSeller.setPropertyViewedCount(property.getPropertyViewedCount());
				propertyToSetToSeller.setPropertyLongitude(property.getPropertyLongitude());
				if(!property.getPropertyFeedbackId().isEmpty()) {
					propertyToSetToSeller.setPropertyFeedbackIds(this.feedback(property.getPropertyFeedbackId()));
				}

				if(!property.getPropertyImageId().isEmpty()) {
					List<Image> imageOfPropertyListToSetToSeller=new ArrayList<>();
					for (ImageEntity image : property.getPropertyImageId() ) {
						if(image.isImageIsActive()) {
							Image image1=this.imageForUser(image);
							imageOfPropertyListToSetToSeller.add(image1);
						}
					}
					propertyToSetToSeller.setPropertyImageIds(imageOfPropertyListToSetToSeller);
				}
				if(!property.getPropertyViewedId().isEmpty()) {
					propertyToSetToSeller.setPropertyViewedIds(this.viewedPropertyForLogin(property.getPropertyViewedId()));
				}
				propertyListToSetToSeller.add(propertyToSetToSeller);

			}
		}
		return propertyListToSetToSeller;
		}catch(Exception exception) {
			logg("propertySetting "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Property from property entity.
	 *
	 * @param propertyEntity the property entity
	 * @return the property
	 */
	public Property propertyFromPropertyEntity(PropertyEntity propertyEntity){

		try {
			
			Property property=new Property();
			property.setPropertyCent(propertyEntity.getPropertyCent());
			property.setPropertyCity(propertyEntity.getPropertyCity());
			property.setPropertyCountry(propertyEntity.getPropertyCountry());
			property.setPropertyCreatedTime(propertyEntity.getPropertyCreatedTime());
			property.setPropertyDescription(propertyEntity.getPropertyDescription());
			property.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
			property.setPropertyId(propertyEntity.getPropertyId());
			property.setPropertyIsActive(propertyEntity.isPropertyIsActive());
			property.setPropertyLandmark(propertyEntity.getPropertyLandmark());
			property.setPropertyLatitude(propertyEntity.getPropertyLongitude());
			property.setPropertyLongitude(propertyEntity.getPropertyLongitude());
			property.setPropertyModifiedTime(propertyEntity.getPropertyModifiedTime());
			property.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
			property.setPropertyState(propertyEntity.getPropertyState());
			property.setPropertyType(propertyEntity.getPropertyType());
			property.setPropertyViewedCount(propertyEntity.getPropertyViewedCount());
			if(!propertyEntity.getPropertyFeedbackId().isEmpty()) {
				property.setPropertyFeedbackIds(this.feedback(propertyEntity.getPropertyFeedbackId()));
			}
			if(!propertyEntity.getPropertyImageId().isEmpty()) {
				property.setPropertyImageIds(this.image(propertyEntity.getPropertyImageId()));
			}
			if(!propertyEntity.getPropertyViewedId().isEmpty()) {
				property.setPropertyViewedIds(this.viewedPropertyForLogin(propertyEntity.getPropertyViewedId()));
			}
		return property;
		}catch(Exception exception) {
			logg("property "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Image.
	 *
	 * @param userImageEntity the user image entity
	 * @return the list
	 */

	
	/**
	 * Viewed property for login.
	 *
	 * @param viewedPropertyEntityList the viewed property entity list
	 * @return the list
	 */

	
	/**
	 * Propertysettingforreturnmodel.
	 *
	 * @param propertyEntity the property entity
	 * @return the property
	 */
	
	/**
	 * Image.
	 *
	 * @param userImageEntity the user image entity
	 * @return the list
	 */
	public List<Image> image(List<ImageEntity> userImageEntity) {
		try {
			List<Image> imagelist=new ArrayList<>();
			for (ImageEntity image : userImageEntity) {
				if(image.isImageIsActive()){
					Image image1= new Image();
					image1.setImageCreatedTime(image.getImageCreatedTime());
					image1.setImageDescription(image.getImageDescription());
					image1.setImageId(image.getImageId());
					image1.setImageIsActive(image.isImageIsActive());
					image1.setImagePath(image.getImagePath());
					imagelist.add(image1);
			}
		}
			return imagelist;
		}catch(Exception exception) {
			logg("imageForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Viewed property for login.
	 *
	 * @param viewedPropertyEntityList the viewed property entity list
	 * @return the list
	 */
	public List<ViewedProperty> viewedPropertyForLogin (List<ViewedPropertyEntity> viewedPropertyEntityList){
		try {
		List<ViewedProperty> viewedPropertyOfProperty=new ArrayList<>();
		for (ViewedPropertyEntity viewedproperty : viewedPropertyEntityList) {
			ViewedProperty viewproperty=new ViewedProperty();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(this.propertyFromPropertyEntity(viewedproperty.getViewedPropertyPropertyId()));
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfProperty.add(viewproperty);
		}
		return viewedPropertyOfProperty;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Propertysettingforreturnmodel.
	 *
	 * @param propertyEntity the property entity
	 * @return the property
	 */
	public Property propertysettingforreturnmodel(PropertyEntity propertyEntity){
		try {
			if(propertyEntity.isPropertyIsActive()) {
				Property propertyToSetToSeller = new Property();
				propertyToSetToSeller.setPropertyCent(propertyEntity.getPropertyCent());
				propertyToSetToSeller.setPropertyCity(propertyEntity.getPropertyCity());
				propertyToSetToSeller.setPropertyCountry(propertyEntity.getPropertyCountry());
				propertyToSetToSeller.setPropertyCreatedTime(propertyEntity.getPropertyCreatedTime());
				propertyToSetToSeller.setPropertyDescription(propertyEntity.getPropertyDescription());
				propertyToSetToSeller.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
				propertyToSetToSeller.setPropertyId(propertyEntity.getPropertyId());
				propertyToSetToSeller.setPropertyIsActive(propertyEntity.isPropertyIsActive());
				propertyToSetToSeller.setPropertyLandmark(propertyEntity.getPropertyLandmark());
				propertyToSetToSeller.setPropertyLatitude(propertyEntity.getPropertyLongitude());
				propertyToSetToSeller.setPropertyModifiedTime(propertyEntity.getPropertyModifiedTime());
				propertyToSetToSeller.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
				propertyToSetToSeller.setPropertyState(propertyEntity.getPropertyState());
				propertyToSetToSeller.setPropertyType(propertyEntity.getPropertyType());
				propertyToSetToSeller.setPropertyViewedCount(propertyEntity.getPropertyViewedCount());
				propertyToSetToSeller.setPropertyLongitude(propertyEntity.getPropertyLongitude());
				if(!propertyEntity.getPropertyFeedbackId().isEmpty()) {
					propertyToSetToSeller.setPropertyFeedbackIds(this.feedback(propertyEntity.getPropertyFeedbackId()));
				}

				if(!propertyEntity.getPropertyImageId().isEmpty()) {
					List<Image> imageOfPropertyListToSetToSeller=new ArrayList<>();
					for (ImageEntity image : propertyEntity.getPropertyImageId() ) {
						Image image1=this.imageForUser(image);
						imageOfPropertyListToSetToSeller.add(image1);
					}
					propertyToSetToSeller.setPropertyImageIds(imageOfPropertyListToSetToSeller);
				}
				if(!propertyEntity.getPropertyViewedId().isEmpty()) {
					propertyToSetToSeller.setPropertyViewedIds(this.viewedPropertyForLogin(propertyEntity.getPropertyViewedId()));
				}


			
			return propertyToSetToSeller;
			}return null;
		}catch(Exception exception) {
			logg("propertysettingforreturnmodel "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Gets the user by email id.
	 *
	 * @param emailId the email id
	 * @return the user entity
	 */
	public UsersEntity getUserByEmailId(String emailId) {
		try {
		
		return userrepository.findUserByEmailID(emailId);
		}catch(Exception exception) {
			logg("getUserByEmailId "+exception.getMessage());
			throw exception;
		}
		
		
	}
	
	/**
	 * Check password.
	 *
	 * @param hashedInputpassword the hashed inputpassword
	 * @param dbPassword the db password
	 * @return true, if successful
	 */
	public boolean checkPassword(String hashedInputpassword,String dbPassword) {
		try {
		boolean flag=false;
		if(dbPassword.equals(hashedInputpassword)) {
			
			flag=true;
		}
		
		return flag;
		}catch(Exception exception) {
			logg("checkPassword "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Gets the user by contact number.
	 *
	 * @param contactNumber the contact number
	 * @return the user entity
	 */
	public UsersEntity getUserByContactNumber(BigInteger contactNumber) {
		try {
		
		return userrepository.findUserByContactNumber(contactNumber);
		}catch(Exception exception) {
			logg("getUserByContactNumber "+exception.getMessage());
			throw exception;
		}
		
	}
	
	/**
	 * Sets the salt string.
	 *
	 * @param receivedUserEntity the received user entity
	 * @param saltstring the saltstring
	 */
	@Transactional(readOnly=false)
	public void setSaltString(UsersEntity receivedUserEntity, String saltstring) {
		try {
		receivedUserEntity.setSaltString(saltstring);
		userrepository.save(receivedUserEntity);
	}catch(Exception exception) {
		logg("setSaltString "+exception.getMessage());
		throw exception;
	}
	}
	
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}
	
}
