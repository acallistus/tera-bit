package com.project.terabit.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Feedback;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.FeedBackRepository;
import com.project.terabit.repository.NotificationRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;


@Service
@Transactional(readOnly=true)
public class FeedBackServiceImpl implements FeedBackService{

	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	
	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	FeedBackRepository feedbackRepository;
	
	@Autowired
	PropertyRepository propertyRepository;
	
	@Autowired
	SellerRepository sellerRepository;
	
	@Autowired
	AdminRepository adminRepository;
	
	@Autowired
	NotificationRepository notificationRepository;
	
	/** The Constant SERVICEEXCEPTION_NOSALTSTRING. */
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "UTILITYSERVICE.no_saltstring";	
	
	/** The Constant SERVICEEXCEPTION_NOUSERID. */
	private static final String SERVICEEXCEPTION_NOUSERID = "UTILITYSERVICE.invalid_user_id";
	
	/** The Constant SERVICEEXCEPTION_NOUSER. */
	private static final String SERVICEEXCEPTION_NOUSER = "UTILITYSERVICE.No_User_Exists";
	
	private static final String SERVICEEXCEPTION_NOFEEDBACK = "DELETEFEEDBACKSERVICE.No_feedback";
	
	private static final String SERVICEEXCEPTION_NOSELLER = "UTILITYSERVICE.No_seller";
	
	private static final String SERVICEEXCEPTION_MULTIPLEINPUT = "UTILITYSERVICE.multiple_input";
	
	private static final String SERVICEEXCEPTION_NOPROPERTY = "UTILITYSERVICE.No_property";
	
	private static final String SERVICEEXCEPTION_INVALIDFEEDBACKID = "UTILITYSERVICE.invalid_feedback_id";
	
	
	private static final String SERVICEEXCEPTION_NOFEEDBACKID = "UTILITYSERVICE.no_feedback_id";
	
	
	private static final String SELLERFEEDBACKNOTIFICATION = " provided feedback for your profile. His mail id is ";
	
	private static final String PROPERTYFEEDBACKNOTIFICATION = " provided feedback for your property. His mail id is ";

	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Feedback createFeedback(Feedback feedback, String saltString) throws Exception {
		Feedback feedbackToBeReturned = new Feedback();
		try {
			FeedbackEntity feedbackEntityToBeReturned;
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			if(feedback.getFeedbackGivenBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			UsersEntity userEntity = userRepository.findUserById(feedback.getFeedbackGivenBy(), saltString);
			
			if(userEntity==null)  {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			
			
			FeedbackEntity feedbackEntity = new FeedbackEntity();
			feedbackEntity.setFeedbackCreatedBy(userEntity.getUserFirstName()+" "+userEntity.getUserLastName());
			feedbackEntity.setFeedbackCreatedTime(LocalDateTime.now());
			feedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
			feedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
			feedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
			feedbackEntity.setFeedbackGivenBy(feedback.getFeedbackGivenBy());
			feedbackEntityToBeReturned=feedbackRepository.save(feedbackEntity);
			if(userEntity.getUserFeedbacks()!=null) {
				userEntity.getUserFeedbacks().add(feedbackEntityToBeReturned);
			}
			userRepository.save(userEntity);
			if(feedback.getSellerId()!=null) {
				if(feedback.getAdminId()!=null || feedback.getPropertyId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_MULTIPLEINPUT);
				}
				SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(feedback.getSellerId());
				UsersEntity userSellerEntity = userRepository.findUserFromSellerId(sellerEntity);
				if(sellerEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOSELLER);
				}
				if(sellerEntity.getSellerFeedbackId()!=null) {
					sellerEntity.getSellerFeedbackId().add(feedbackEntityToBeReturned);
				}
				sellerRepository.save(sellerEntity);
				NotificationEntity notificationEntity = new NotificationEntity();
				notificationEntity.setNotificationTo(userSellerEntity.getUserId());
				notificationEntity.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+SELLERFEEDBACKNOTIFICATION+userEntity.getUserEmailId());
				notificationEntity.setNotificationIsActive(true);
				notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
				notificationEntity.setNotificationViewedTime(LocalDateTime.now());
				notificationRepository.save(notificationEntity);
				userSellerEntity.getUserNotificationIds().add(notificationEntity);
				userRepository.save(userSellerEntity);
				
				feedbackToBeReturned.setSellerId(sellerEntity.getSellerId());
			}
			
			
			if(feedback.getPropertyId()!=null) {
				if(feedback.getAdminId()!=null || feedback.getSellerId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_MULTIPLEINPUT);
				}
				PropertyEntity propertyentity= propertyRepository.getPropertyByPropertyId(feedback.getPropertyId());
				
				if(propertyentity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
				}
				SellerEntity sellerEntity = sellerRepository.findSellerByPropertyId(propertyentity.getPropertyId());
				UsersEntity userSellerEntity = userRepository.findUserFromSellerId(sellerEntity);
				propertyentity.getPropertyFeedbackId().add(feedbackEntityToBeReturned);
				propertyRepository.save(propertyentity);
				NotificationEntity notificationEntity = new NotificationEntity();
				notificationEntity.setNotificationTo(userSellerEntity.getUserId());
				notificationEntity.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+PROPERTYFEEDBACKNOTIFICATION+userEntity.getUserEmailId());
				notificationEntity.setNotificationIsActive(true);
				notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
				notificationEntity.setNotificationViewedTime(LocalDateTime.now());
				notificationRepository.save(notificationEntity);
				userSellerEntity.getUserNotificationIds().add(notificationEntity);
				userRepository.save(userSellerEntity);
				
				feedbackToBeReturned.setPropertyId(propertyentity.getPropertyId());
			}
			if(feedback.getAdminId()!=null) {
				if(feedback.getSellerId()!=null || feedback.getPropertyId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_MULTIPLEINPUT);
				}
				AdminEntity adminentity= adminRepository.getAdminByAdminId(feedback.getAdminId());
				if(adminentity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
				}
				String userId = adminRepository.getUserByAdminId(adminentity.getAdminId());
				UsersEntity userAdminEntity = userRepository.findActiveUsers(UUID.fromString(userId));
				
				adminentity.getAdminFeedbackIds().add(feedbackEntityToBeReturned);
				adminRepository.save(adminentity);
				NotificationEntity notificationEntity = new NotificationEntity();
				notificationEntity.setNotificationTo(userAdminEntity.getUserId());
				notificationEntity.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+SELLERFEEDBACKNOTIFICATION+userEntity.getUserEmailId());
				notificationEntity.setNotificationIsActive(true);
				notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
				notificationEntity.setNotificationViewedTime(LocalDateTime.now());
				notificationRepository.save(notificationEntity);
				userAdminEntity.getUserNotificationIds().add(notificationEntity);
				userRepository.save(userAdminEntity);
				
				feedbackToBeReturned.setAdminId(adminentity.getAdminId());
			}
			
			feedbackToBeReturned.setFeedbackId(feedbackEntityToBeReturned.getFeedbackId());
			feedbackToBeReturned.setFeedbackDescription(feedbackEntityToBeReturned.getFeedbackDescription());
			feedbackToBeReturned.setFeedbackGivenBy(userEntity.getUserId());
			feedbackToBeReturned.setFeedbackRating(feedbackEntityToBeReturned.getFeedbackRating());
			feedbackToBeReturned.setFeedbackCreatedBy(feedbackEntityToBeReturned.getFeedbackCreatedBy());
			feedbackToBeReturned.setFeedbackModifiedTime(feedbackEntityToBeReturned.getFeedbackModifiedTime());
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return feedbackToBeReturned;
	}
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}


	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Feedback updateFeedback(Feedback feedback, String saltString) throws Exception {
		Feedback feedbackToBeReturned = new Feedback();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			if(feedback.getFeedbackGivenBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			
			UsersEntity userEntity = userRepository.findUserById(feedback.getFeedbackGivenBy(), saltString);
			
			if(userEntity==null)  {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			if(feedback.getFeedbackId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOFEEDBACKID);
			}
			FeedbackEntity feedbackEntity = feedbackRepository.checkUserFeedback(feedback.getFeedbackGivenBy(), feedback.getFeedbackId());
			if(feedbackEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDFEEDBACKID);
			}
			
			if(feedback.getSellerId()!=null) {
				if(feedback.getAdminId()!=null || feedback.getPropertyId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_MULTIPLEINPUT);
				}
				SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(feedback.getSellerId());
				if(sellerEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOSELLER);
				}
				for(FeedbackEntity sellerFeedbackEntity: sellerEntity.getSellerFeedbackId()) {
					
					if(sellerFeedbackEntity.getFeedbackId().equals(feedback.getFeedbackId())) {
						sellerFeedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
						sellerFeedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
						sellerFeedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
					}
					
				}
				sellerRepository.save(sellerEntity);
			}
			
			if(feedback.getPropertyId()!=null) {
				if(feedback.getAdminId()!=null || feedback.getSellerId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_MULTIPLEINPUT);
				}
				PropertyEntity propertyEntity = propertyRepository.getPropertyByPropertyId(feedback.getPropertyId());
				if(propertyEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
				}
				for(FeedbackEntity propertyFeedbackEntity: propertyEntity.getPropertyFeedbackId()) {
					if(propertyFeedbackEntity.getFeedbackId().equals(feedback.getFeedbackId())) {
						propertyFeedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
						propertyFeedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
						propertyFeedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
					}
					
				}
				propertyRepository.save(propertyEntity);
			}
			
			if(feedback.getAdminId()!=null) {
				if(feedback.getPropertyId()!=null || feedback.getSellerId()!=null) {
					throw new ServiceException(SERVICEEXCEPTION_MULTIPLEINPUT);
				}
				AdminEntity adminEntity = adminRepository.getAdminByAdminId(feedback.getAdminId());
				if(adminEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
				}
				for(FeedbackEntity adminFeedbackEntity: adminEntity.getAdminFeedbackIds()) {
					if(adminFeedbackEntity.getFeedbackId().equals(feedback.getFeedbackId())) {
						adminFeedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
						adminFeedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
						adminFeedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
					}
					
				}
				adminRepository.save(adminEntity);
			}
			feedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
			feedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
			feedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
			feedbackRepository.save(feedbackEntity);
			for(FeedbackEntity userFeedbackEntity: userEntity.getUserFeedbacks()) {
				if(userFeedbackEntity.getFeedbackId().equals(feedback.getFeedbackId())) {
					userFeedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
					userFeedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
					userFeedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
				}
			}
			userRepository.save(userEntity);
			
			feedbackToBeReturned.setFeedbackId(feedbackEntity.getFeedbackId());
			feedbackToBeReturned.setFeedbackDescription(feedbackEntity.getFeedbackDescription());
			feedbackToBeReturned.setFeedbackGivenBy(userEntity.getUserId());
			feedbackToBeReturned.setFeedbackRating(feedbackEntity.getFeedbackRating());
			feedbackToBeReturned.setFeedbackCreatedBy(feedbackEntity.getFeedbackCreatedBy());
			feedbackToBeReturned.setAdminId(feedback.getAdminId());
			feedbackToBeReturned.setSellerId(feedback.getSellerId());
			feedbackToBeReturned.setPropertyId(feedback.getPropertyId());
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return feedbackToBeReturned;
	}


	@Override
	public List<Feedback> retriveFeedback(Feedback feedback, String saltString) throws Exception {
		Feedback feedbackToBeRetrived = new Feedback();
		List<Feedback> feedbackListToBeReturned = new ArrayList<>();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			if(feedback.getFeedbackGivenBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			
			UsersEntity userEntity = userRepository.findUserById(feedback.getFeedbackGivenBy(), saltString);
			
			if(userEntity==null)  {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			List<FeedbackEntity> feedbackEntity = feedbackRepository.findFeedbacksByUserId(feedback.getFeedbackGivenBy());
			if(feedbackEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOFEEDBACK);
			}
		
			for(int index=0;index<feedbackEntity.size();index++) {
				feedbackToBeRetrived.setFeedbackDescription(feedbackEntity.get(index).getFeedbackDescription());
				feedbackToBeRetrived.setFeedbackGivenBy(feedbackEntity.get(index).getFeedbackGivenBy());
				feedbackToBeRetrived.setFeedbackId(feedbackEntity.get(index).getFeedbackId());
				feedbackToBeRetrived.setFeedbackRating(feedbackEntity.get(index).getFeedbackRating());
				feedbackListToBeReturned.add(feedbackToBeRetrived);
			}
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return feedbackListToBeReturned;
	}
	
	

}
