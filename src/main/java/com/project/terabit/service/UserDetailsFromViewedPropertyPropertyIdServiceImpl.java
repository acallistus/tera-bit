package com.project.terabit.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.terabit.controller.ControllerException;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Image;
import com.project.terabit.model.UserDetail;
import com.project.terabit.repository.UserRepository;

@Component
public class UserDetailsFromViewedPropertyPropertyIdServiceImpl {
	
	@Autowired
	UserRepository userrepository;
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The Constant SERVICEEXCEPTIONSALTSTRINGNOTPRESENT. */
	private static final String SERVICEEXCEPTIONSALTSTRINGNOTPRESENT = "getUserDetailsForPropertyId.saltstringmissing";
	
	/** The Constant CONTROLLEREXCEPTION_NOUSERID. */
    private static final String SERVICEEXCEPTIONNOPROPERTYID = "getUserDetailsForPropertyId.nopropertyid";
    
    /** The Constant CONTROLLEREXCEPTION_URLMISSMATCH. */
    private static final String SERVICEEXCEPTIONURLMISSMATCH = "getUserDetailsForPropertyId.saltstringmissmatch";
    
    /** The Constant CONTROLLEREXCEPTION_NOUSER. */
    private static final String SERVICEEXCEPTIONNOUSER = "getUserDetailsForPropertyId.nouserdetailsavailable";
    
    
    public List<UserDetail> getUserDetailsForPropertyId(String saltstring, UserDetail incomminguserdetail) throws Exception{
    	
    	try {
    		List<UserDetail> returnList=new ArrayList<>();
    		List<UsersEntity> returnListFromRepository=new ArrayList<>();
    		if(saltstring==null) {
   			   throw new ControllerException(SERVICEEXCEPTIONSALTSTRINGNOTPRESENT);
   		   }
    		
    		if(incomminguserdetail.getIncommingPropertyId()==null) {
    			throw new ControllerException(SERVICEEXCEPTIONNOPROPERTYID);
    		}
    		if(!(saltstring.equals(userrepository.saltstringComparisonForSellerId(incomminguserdetail.getIncommingSellerId())))) {
    			throw new ControllerException(SERVICEEXCEPTIONURLMISSMATCH);
    		}
    		returnListFromRepository=userrepository.userDetailsForViewedPropertyPropertyId(incomminguserdetail.getIncommingPropertyId());
    		if(!returnListFromRepository.isEmpty()) {
    		for (UsersEntity usersEntity : returnListFromRepository) {
				UserDetail userDetails=new UserDetail();
				userDetails.setUserContactNo(usersEntity.getUserContactNo());
				userDetails.setUserEmailId(usersEntity.getUserEmailId());
				userDetails.setUserFirstName(usersEntity.getUserFirstName());
				userDetails.setUserIsActive(usersEntity.isUserIsActive());
				userDetails.setUserLastName(usersEntity.getUserLastName());
					//image to be displayed
					Image image = new Image();
					image.setImageCreatedTime(usersEntity.getUserImage().getImageCreatedTime());
					image.setImageDescription(usersEntity.getUserImage().getImageDescription());
					image.setImageId(usersEntity.getUserImage().getImageId());
					image.setImageIsActive(usersEntity.getUserImage().isImageIsActive());
					image.setImagePath(usersEntity.getUserImage().getImagePath());
					userDetails.setUserImage(image);
				returnList.add(userDetails);
			}
    		}else {
    			throw new ServiceException(SERVICEEXCEPTIONNOUSER);
    		}
    		return returnList;
    	}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("getUserDetailsForPropertyId "+exception.getMessage());
			throw exception;
		}
    }
    
    private void logg(String message) {
		log.error(message);
	}

}
