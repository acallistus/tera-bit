package com.project.terabit.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.AdminEntity;

import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;

import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Admin;
import com.project.terabit.model.Image;
import com.project.terabit.model.Seller;
import com.project.terabit.repository.AdminRepository;

import com.project.terabit.repository.PropertyRepository;

import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;


@Service
@Transactional(readOnly=true)

public class SellerApprovalServiceImpl implements SellerApprovalService{


	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	private UserRepository userRepository;
	
	/** Autowiring the seller repository. */
	@Autowired
	private SellerRepository sellerRepository;
	
	/** Autowiring the seller repository. */
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	PropertyRepository propertyRepository;
	

	
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "SELLERAPPROVALSERVICE.no_saltstring";	
	

	/** The Constant SERVICEEXCEPTION_URLMISSMATCH. */
	private static final String SERVICEEXCEPTION_URLMISSMATCH = "SELLERAPPROVALSERVICE.invalid_saltstring";
	
	/** The Constant SERVICEEXCEPTION_NOUSER. */
	private static final String SERVICEEXCEPTION_NOUSER = "SELLERAPPROVALSERVICE.No_User_Exists";
	
	private static final String SERVICEEXCEPTION_SELLERIDNOTPROVIDED = "SELLERAPPROVALSERVICE.No_seller_id_provided";

	
	private static final String SERVICEEXCEPTION_NOSELLERID = "SELLERAPPROVALSERVICE.no_seller_id";
	
	private static final String SERVICEEXCEPTION_INVALIDSELLER = "SELLERAPPROVALSERVICE.seller_already_approved";
	
	private static final String SERVICEEXCEPTION_INVALIDSELLERID = "SELLERAPPROVALSERVICE.invalid_seller_id";
	
	private static final String SERVICEEXCEPTION_INVALIDADMINID = "SELLERAPPROVALSERVICE.invalid_admin_id";
	
	private static final String SERVICEEXCEPTION_NULLSELLERLIST = "SELLERAPPROVALSERVICE.invalid_null_sellerlist";
	
	private static final String SERVICEEXCEPTION_NULLUSERFORADMIN = "SELLERAPPROVALSERVICE.no_user_for_admin";

	private static final String SERVICEEXCEPTION_SELLERADMINMISSMATCH = "SELLERAPPROVALSERVICE.seller_admin_missmatch";

	private static final String SERVICEEXCEPTION_NULLADMINFORSELLER = "SELLERAPPROVALSERVICE.null_admin_for_seller";
	
	//seller has already existed. 
	private static final String SERVICEEXCEPTION_SELLEREXISTEDADLREADY = "SELLERAPPROVALSERVICE.seller_existed_already";
	
	private static final String SELLERAPPROVALNOTIFICATION = "Your request for seller has been approved. You can now post properties for sale";

	private static final String SELLERREJECTIONSUCCESS = "Seller Approval cancelled successfully";

	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRES_NEW)
	public Seller createSellerByAdmin(String saltstring, Seller seller) throws Exception {
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			//checking if user id is null
			if(seller.getSellerId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSELLERID);
			}
			SellerEntity sellerEntity = sellerRepository.getSellerBySellerId(seller.getSellerId());
			if(sellerEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLERID);
			}
			//retriving user data from user entity
			UsersEntity userEntity = userRepository.findUserFromSellerId(sellerEntity);
			
			//checking is there is no user for given user id
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			

			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			
			SellerEntity checkSellerApproved = sellerRepository.checkSellerAlreadyApproved(seller.getSellerId());
			if(checkSellerApproved!=null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLER);
			}
			
			AdminEntity adminEntity = adminRepository.getAdminByAdminId(seller.getSellerRightBy());

			
			if(adminEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMINID);
			}
			UsersEntity adminUserEntity = userRepository.findUserByAdminId(adminEntity);
			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(adminUserEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}


			sellerEntity.setSellerIsActive(true);
			sellerEntity.setSellerRightBy(seller.getSellerRightBy());
			
			adminEntity.setAdminSellerCount(adminEntity.getAdminSellerCount().add(BigInteger.valueOf(1l)));
			
			adminRepository.save(adminEntity);
			sellerRepository.save(sellerEntity);
			

			NotificationEntity notificationEntity = new NotificationEntity();
			notificationEntity.setNotificationTo(userEntity.getUserId());
			notificationEntity.setNotificationContent(SELLERAPPROVALNOTIFICATION);
			notificationEntity.setNotificationIsActive(true);
			notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntity.setNotificationViewedTime(LocalDateTime.now());
			userEntity.getUserNotificationIds().add(notificationEntity);
			userRepository.save(userEntity);

		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return seller;
	}

	@Override
	public List<Seller> getSellerByAdmin(String saltstring, Admin admin) throws Exception{
		List<Seller> sellerList = new ArrayList<>();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}

			if(admin.getAdminId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMINID);
			}

			String userid = adminRepository.getUserByAdminId(admin.getAdminId());
			if(userid==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLUSERFORADMIN);
			}

			UsersEntity userEntity = userRepository.findUserByUserId(UUID.fromString(userid));	
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}

			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}

			List<SellerEntity> sellerEntityList = sellerRepository.getSellerByAdmin(admin.getAdminId());
			if(sellerEntityList==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLSELLERLIST);
			}
			
			
			for(int index=0;index<sellerEntityList.size();index++) {
				LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
				Seller seller = new Seller();
				Image image = new Image();

				seller.setSellerId(sellerEntityList.get(index).getSellerId());
				seller.setSellerIsActive(sellerEntityList.get(index).isSellerIsActive());
				seller.setSellerCompanyName(sellerEntityList.get(index).getSellerCompanyName());
				seller.setSellerPrivateContact(sellerEntityList.get(index).getSellerPrivateContact());
				seller.setSellerPropertyCount(sellerEntityList.get(index).getSellerPropertyCount());
				seller.setSellerCreatedTime(sellerEntityList.get(index).getSellerCreatedTime());
				
				if((sellerEntityList.get(index).getSellerCompanyLogoId())!=null) {
					image.setImageId(sellerEntityList.get(index).getSellerCompanyLogoId().getImageId());
					image.setImageDescription(sellerEntityList.get(index).getSellerCompanyLogoId().getImageDescription());
					image.setImageIsActive(sellerEntityList.get(index).getSellerCompanyLogoId().isImageIsActive());
					image.setImagePath(sellerEntityList.get(index).getSellerCompanyLogoId().getImagePath());
					image.setImageCreatedTime(sellerEntityList.get(index).getSellerCompanyLogoId().getImageCreatedTime());
					seller.setSellerCompanyLogoId(image);
				}
				
				if((sellerEntityList.get(index).getSellerFeedbackId())!=null)
					seller.setSellerFeedbackIds(loginservice.feedback(sellerEntityList.get(index).getSellerFeedbackId()));
				if((sellerEntityList.get(index).getSellerNotificationId())!=null)
					seller.setSellerNotificationIds(loginservice.notificationlist(sellerEntityList.get(index).getSellerNotificationId()));
				if((sellerEntityList.get(index).getSellerPropertyId())!=null)
					seller.setSellerPropertyIds(loginservice.propertysetting(sellerEntityList.get(index).getSellerPropertyId()));
				
				sellerList.add(seller);
				
			}
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		return sellerList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRES_NEW)
	public List<Seller> deleteSellerByAdmin(String saltstring, Admin admin) throws Exception{
		List<Seller> sellerList = new ArrayList<>();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}

			if(admin.getAdminId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMINID);
			}

			String userid = adminRepository.getUserByAdminId(admin.getAdminId());
			if(userid==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLUSERFORADMIN);
			}

			UsersEntity userEntity = userRepository.findUserByUserId(UUID.fromString(userid));		
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}

			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			if(admin.getSellerId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_SELLERIDNOTPROVIDED);
			}

			SellerEntity sellerToBeDeletedEntity = sellerRepository.findSellerBySellerId(admin.getSellerId());
			if(sellerToBeDeletedEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLERID);
			}

			UsersEntity userSellerEntity = userRepository.findUserFromSellerId(sellerToBeDeletedEntity);
			if(userSellerEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			sellerToBeDeletedEntity.setSellerIsActive(false);
			userSellerEntity.setUserIsSeller(false);
			List<PropertyEntity> propertyToBeDeleted = propertyRepository.findPropertyBySeller(sellerToBeDeletedEntity.getSellerId());
			if(propertyToBeDeleted!=null && !propertyToBeDeleted.isEmpty()) {
				for(PropertyEntity propertyEntity: propertyToBeDeleted) {
					propertyEntity.setPropertyIsActive(false);
				}
			}
			sellerToBeDeletedEntity.setSellerModifiedTime(LocalDateTime.now());
			sellerRepository.save(sellerToBeDeletedEntity);
			userRepository.save(userSellerEntity);

			sellerList = this.getSellerByAdmin(saltstring, admin);
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return sellerList;
	}

	
	@Override
	public List<Seller> getApprovalPendingSeller(String saltstring, Admin admin) throws Exception{
		List<Seller> sellerList = new ArrayList<>();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(admin.getAdminId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMINID);
			}
			String userid = adminRepository.getUserByAdminId(admin.getAdminId());
			if(userid==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLUSERFORADMIN);
			}
			UsersEntity userEntity = userRepository.findUserByUserId(UUID.fromString(userid));			
			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			List<SellerEntity> sellerEntityList = sellerRepository.getApprovalPendingSeller(admin.getAdminId());
			if(sellerEntityList==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLSELLERLIST);
			}
			
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			for(int index=0;index<sellerEntityList.size();index++) {
				Seller seller = new Seller();
				Image image = new Image();
				seller.setSellerId(sellerEntityList.get(index).getSellerId());
				seller.setSellerIsActive(sellerEntityList.get(index).isSellerIsActive());
				seller.setSellerCompanyName(sellerEntityList.get(index).getSellerCompanyName());
				seller.setSellerPrivateContact(sellerEntityList.get(index).getSellerPrivateContact());
				seller.setSellerPropertyCount(sellerEntityList.get(index).getSellerPropertyCount());
				seller.setSellerCreatedTime(sellerEntityList.get(index).getSellerCreatedTime());
				seller.setSellerRightBy(admin.getAdminRightsBy());
				
				if((sellerEntityList.get(index).getSellerCompanyLogoId())!=null) {
					image.setImageId(sellerEntityList.get(index).getSellerCompanyLogoId().getImageId());
					image.setImageDescription(sellerEntityList.get(index).getSellerCompanyLogoId().getImageDescription());
					image.setImageIsActive(sellerEntityList.get(index).getSellerCompanyLogoId().isImageIsActive());
					image.setImagePath(sellerEntityList.get(index).getSellerCompanyLogoId().getImagePath());
					image.setImageCreatedTime(sellerEntityList.get(index).getSellerCompanyLogoId().getImageCreatedTime());
					seller.setSellerCompanyLogoId(image);
				}
				
				if((sellerEntityList.get(index).getSellerFeedbackId())!=null)
					seller.setSellerFeedbackIds(loginservice.feedback(sellerEntityList.get(index).getSellerFeedbackId()));
				if((sellerEntityList.get(index).getSellerNotificationId())!=null)
					seller.setSellerNotificationIds(loginservice.notificationlist(sellerEntityList.get(index).getSellerNotificationId()));
				if((sellerEntityList.get(index).getSellerPropertyId())!=null)
					seller.setSellerPropertyIds(loginservice.propertysetting(sellerEntityList.get(index).getSellerPropertyId()));
				
				sellerList.add(seller);
				
			}
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		return sellerList;
	}
	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRES_NEW)
	public String cancelSellerApproval(String saltstring, Admin admin) throws Exception{
		String message;
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			if(admin.getAdminId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMINID);
			}
			String userid = adminRepository.getUserByAdminId(admin.getAdminId());
			log.info("adminid:"+admin.getAdminId()+" corresponding user id is "+userid);
			if(userid==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLUSERFORADMIN);
			}
			UsersEntity userEntity = userRepository.findUserByUserId(UUID.fromString(userid));			
			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			SellerEntity sellerEntity = sellerRepository.findSellerBySellerId(admin.getSellerId());
			if(sellerEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLERID);
			}
			if(sellerEntity.getSellerRightBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NULLADMINFORSELLER);
			}
			if(!sellerEntity.getSellerRightBy().equals(admin.getAdminId())) {
				throw new ServiceException(SERVICEEXCEPTION_SELLERADMINMISSMATCH);
			}
			
			if(!sellerEntity.getSellerFeedbackId().isEmpty() || !sellerEntity.getSellerNotificationId().isEmpty() || !sellerEntity.getSellerPropertyId().isEmpty()) {
				throw new ServiceException(SERVICEEXCEPTION_SELLEREXISTEDADLREADY);
			}
			UsersEntity userSellerEntity = userRepository.findUserFromSellerId(sellerEntity.getSellerId());
			userSellerEntity.setUserSellerId(null);
			userSellerEntity.setUserIsSeller(false);
			userRepository.save(userSellerEntity);
			sellerRepository.deleteById(sellerEntity.getSellerId());
			
			message = SELLERREJECTIONSUCCESS;
			log.info(message+" for seller id:"+admin.getSellerId());
			
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		return message;
	}
	

	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}

}
