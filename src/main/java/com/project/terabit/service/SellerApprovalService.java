package com.project.terabit.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.project.terabit.model.Admin;
import com.project.terabit.model.Seller;

@Service
public interface SellerApprovalService {

	
	public Seller createSellerByAdmin(String saltstring,Seller seller) throws Exception;
	
	public List<Seller> getSellerByAdmin(String saltstring, Admin admin) throws Exception;
	
	public List<Seller> deleteSellerByAdmin(String saltstring, Admin admin) throws Exception;

	
	public List<Seller> getApprovalPendingSeller(String saltstring, Admin admin) throws Exception;
	
	public String cancelSellerApproval(String saltstring, Admin admin) throws Exception;


}
