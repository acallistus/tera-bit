package com.project.terabit.service;

import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Component;

import com.project.terabit.model.User;

@Component
public interface AdminService {
	
	
	public User deleteAdmin(User user,String saltString) throws ServiceException,Exception;
	
	public User createAdmin(String saltstring,UUID userId) throws Exception;



}
