package com.project.terabit.validator;

import java.util.UUID;

import com.project.terabit.model.Property;

// TODO: Auto-generated Javadoc
/**
 * The Class AddPropertyValidator.
 */
public class AddPropertyValidator {
	
	/**
	 * Instantiates a new adds the property validator.
	 */
	private AddPropertyValidator() {
	    throw new IllegalStateException("Utility class");
	}
	
	/** The Constant USERIDEXCEPTION. */
	public static final String USERIDEXCEPTION="VALIDATOR.invalid_user_id";
	
	/** The Constant PROPERTYOWNEDBYEXCEPTION. */
	public static final String PROPERTYOWNEDBYEXCEPTION="VALIDATOR.invalid_owned_by";
	
	/** The Constant PROPERTYCITYEXCEPTION. */
	public static final String PROPERTYCITYEXCEPTION="VALIDATOR.invalid_city";
	
	/** The Constant PROPERTYSTATEORCOUNTRYEXCEPTION. */
	public static final String PROPERTYSTATEORCOUNTRYEXCEPTION="VALIDATOR.invalid_state_or_country";
	
	/** The Constant PROPERTYCENTEXCEPTION. */
	public static final String PROPERTYCENTEXCEPTION="VALIDATOR.invalid_property_cent";
	
	/** The Constant PROPERTYLATITUDEORLONGITUDEEXCEPTION. */
	public static final String PROPERTYLATITUDEORLONGITUDEEXCEPTION="VALIDATOR.invalid_latitude_or_longitude";
	
	/** The Constant PROPERTYESTEEMATEDAMOUNTEXCEPTION. */
	public static final String PROPERTYESTEEMATEDAMOUNTEXCEPTION="VALIDATOR.invalid_esteemated_amount";
	
	
	  /**
  	 * Validate.
  	 *
  	 * @param property the property
  	 * @param userid the userid
  	 * @throws Exception the exception
  	 */
  	public static void validate (Property property, UUID userid) throws Exception {
		  if(!validateUserId(userid)) {
			  throw new Exception(USERIDEXCEPTION);
		  }
		  if(!validatePropertOwnBy(property)) {
			  throw new Exception(PROPERTYOWNEDBYEXCEPTION);
		  }
		  if(!validatePropertyCity(property)) {
			  throw new Exception(PROPERTYCITYEXCEPTION);
		  }
		  if(!validatePropertyStateAndCountry(property)) {
			  throw new Exception(PROPERTYSTATEORCOUNTRYEXCEPTION);
		  }
		  if(!validatePropertyCent(property)) {
			  throw new Exception(PROPERTYCENTEXCEPTION);
		  }
		  if(property.getPropertyLatitude()!=null && property.getPropertyLongitude()!=null) {
			  if(!validatePropertyLattitudeAndLongitude(property)) {
				  throw new Exception(PROPERTYLATITUDEORLONGITUDEEXCEPTION);	  
			  	}
		  }
		  if(!validatePropertyEsteematedAmount(property)) {
			  throw new Exception(PROPERTYESTEEMATEDAMOUNTEXCEPTION);	  
		  }
		  
	  }
	  
	  /**
  	 * Validate user id.
  	 *
  	 * @param userid the userid
  	 * @return true, if successful
  	 */
  	public static boolean validateUserId(UUID userid) {
			Boolean flag = false;
			if(userid.toString().matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")) {
				flag = true;
			}return flag;
	  }
	  
  	/**
  	 * Validate propert own by.
  	 *
  	 * @param property the property
  	 * @return true, if successful
  	 */
  	public static boolean validatePropertOwnBy(Property property) {
			Boolean flag = false;
			if( (property.getPropertyOwnedBy()==0 || property.getPropertyOwnedBy()==1)) {
				flag = true;
			}return flag;
	  }
	  
	  /**
  	 * Validate proeprty city.
  	 *
  	 * @param property the property
  	 * @return true, if successful
  	 */
  	public static boolean validatePropertyCity(Property property) {
		  Boolean flag = false;
			if(property.getPropertyCity()==null || property.getPropertyCity().matches("[A-Za-z_]*")) {
				flag = true;
			}return flag;
		}
	  
	  /**
  	 * Validate proeprty state and country.
  	 *
  	 * @param property the property
  	 * @return true, if successful
  	 */
  	public static boolean validatePropertyStateAndCountry(Property property) {
		  Boolean flag = false;
			if((property.getPropertyState()==null) ||(property.getPropertyState().matches("[A-Za-z ]*") && property.getPropertyCountry().matches("[A-Za-z ]*"))) {
				flag = true;
			}return flag;
	  }	
	  
	  /**
  	 * Validate proeprty lattitude and longitude.
  	 *
  	 * @param property the property
  	 * @return true, if successful
  	 */
  	public static boolean validatePropertyLattitudeAndLongitude(Property property) {
		  Boolean flag = false;
			if( (property.getPropertyLatitude().matches("[0-9.]*") && property.getPropertyLongitude().matches("[0-9.]*"))) {
				flag = true;
			}return flag;
	  }	
	  
	  /**
  	 * Validate proeprty cent.
  	 *
  	 * @param property the property
  	 * @return true, if successful
  	 */
  	public static boolean validatePropertyCent(Property property) {
		  Boolean flag = false;
			if((property.getPropertyCent()==null)||(property.getPropertyCent().toString().matches("[0-9.]*"))) {
				flag = true;
			}return flag;
	  }	
	  
	  /**
  	 * Validate proeprty esteemated amount.
  	 *
  	 * @param property the property
  	 * @return true, if successful
  	 */
  	public static boolean validatePropertyEsteematedAmount(Property property) {
		  Boolean flag = false;
			if((property.getPropertyEsteematedAmount()==null)||(property.getPropertyEsteematedAmount().matches("[0-9.,]*"))) {
				flag = true;
			}return flag;
	  }	
	  

}
