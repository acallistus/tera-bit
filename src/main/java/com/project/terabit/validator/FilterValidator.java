package com.project.terabit.validator;

public class FilterValidator {

	/**
	 * Instantiates a new delete user validator.
	 */
	private FilterValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	public static Boolean validateText(String inputText) {
		Boolean flag = false;
		if(inputText.matches("[A-Za-z][A-Za-z ]*")) {
			flag = true;
		}return flag;
	}
	
	
	public static Boolean validateNumber(String fromPrice) {
		Boolean flag = false;
		if(fromPrice.matches("[0-9]+")) {
			flag = true;
		}return flag;
	}
	

	
	
	
	public static Boolean validateTextWithNumber(String landMark) {
		Boolean flag = false;
		if(landMark.matches("[A-Za-z0-9][A-Za-z0-9 ]*")) {
			flag = true;
		}return flag;
	} 
}
