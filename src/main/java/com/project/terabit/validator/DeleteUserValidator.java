package com.project.terabit.validator;


/**@author deepik sriram
/**
 * The Class DeleteUserValidator.
 */
public class DeleteUserValidator {
	
	
	/**
	 * Instantiates a new delete user validator.
	 */
	private DeleteUserValidator() {
	    throw new IllegalStateException("Utility class");
	  }

	
	/** The Constant USERIDEXCEPTION. */
	private static final String USERIDEXCEPTION = "DELETEVALIDATOR.invalid_user_id";
	
	
	/**
	 * Validate.
	 *
	 * @param userId the user id
	 * @throws Exception the exception
	 */
	public static void validate(String userId) throws Exception {
		if(!validateUserId(userId))  throw new Exception(USERIDEXCEPTION);
	}

	/**
	 * Validate user id.
	 *
	 * @param userId the user id
	 * @return the boolean
	 */
	public static Boolean validateUserId(String userId) {
		Boolean flag = false;
		if(userId.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")) {
			flag = true;
		}return flag;
	}

}
