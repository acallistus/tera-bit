package com.project.terabit.validator;

public class AuthorizationIdValidator {
	
	/** The Constant authorizationIdException. */
	private static final String AUTHORIZATIONIDEEXCEPTION = "VALIDATOR.invalid_authorization_id";

	
	public static void ValidateAuthorizationId(String authorizationidtobevalidated) throws Exception
	{
		if(!isValidAuthorizationId(authorizationidtobevalidated))
			throw new Exception(AUTHORIZATIONIDEEXCEPTION);
	}

	public static Boolean isValidAuthorizationId(String authorizationidtobevalidated) {
		
		if(authorizationidtobevalidated.matches("[A-Za-z0-9]{32}"))
			return true;
		return false;
	}
}
