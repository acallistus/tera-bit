package com.project.terabit.utility;


import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;  

public class MailVerification {
	
	private static final String MAILFROM="solaiganesh60@gmail.com";
	private static final String PASSKEY="Solai&Ganesh1997";
	private static final String USERVERIFICATIONLINK="http://localhost:8080//terabit/api/v1/user/verify/";
	private static final String PASSKEYFORGOTLINK="http://localhost:8080//terabit/api/v1/user/changepassword/";

	private MailVerification() {
	    throw new IllegalStateException("Utility class");
	  }
	
	public static void sendMailForLogIn(String to,String authorizationid,String sub){ 
		
		String msg="";
		Random rnd=new Random();
		int otp=1000+rnd.nextInt(8999);
		String img="https://www.thebalance.com/thmb/hrlgAOeCK8hAnrSLb9af7g9qanA=/950x0/real-estate-what-it-is-and-how-it-works-3305882-FINAL-19013d1ad78f4d2d90ced41faa077a85.jpg";
		String link=USERVERIFICATIONLINK+authorizationid;
		

		msg="Hi User Welcome to the World of Terabit, your OTP is ";
		msg=msg+String.valueOf(otp);
		
		Properties props = new Properties();   
		props.put("mail.smtp.host", "smtp.gmail.com");  
		props.put("mail.smtp.socketFactory.port", "465");  
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
		props.put("mail.smtp.auth", "true");  
		props.put("mail.smtp.port", "465"); 
		props.put("mail.smtp.ssl.checkserveridentity", true);
		   Session session = Session.getInstance(props,    
		           new javax.mail.Authenticator() {   
			   	
			   		@Override
		           protected PasswordAuthentication getPasswordAuthentication() {  
		           return new PasswordAuthentication(MAILFROM,PASSKEY);  
		           }    
		          });  
		   try {    
			   MimeMessage message = new MimeMessage(session);    
	           message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));    
	           message.setSubject(sub); 
	           MimeMessageHelper mimemessagehelper=new MimeMessageHelper(message,true,"UTF-8");
	           mimemessagehelper.setText("<body >\r\n" + 
	           		"\r\n" + 
	           		"	<div style=\"background-color:lightblue\"><center><h2>Background Image</h2></center></div>\r\n" + 
	           		"\r\n" + 
	           		"	<img src='"+img+"'width=100% height=70%>\r\n" + 
	           		"<p style=\"background-color: white\">"+msg+"</p>\r\n" + 
	           		"<a href=\""+link+"\"> Click here</a>" + 
	           		"\r\n" + 
	           		"\r\n" + 
	           		"</body>", true);
	           
	          
	           Transport.send(message);     
	          } 
		   catch (MessagingException e)
		   {
			   throw new RuntimeException(e);
		   }  
	}
		   public static void sendMailForPasskeyChange(String to,String authorizationid,String sub){ 
				
				String msg="";
				Random rnd=new Random();
				int otp=1000+rnd.nextInt(8999);
				String img="https://www.thebalance.com/thmb/hrlgAOeCK8hAnrSLb9af7g9qanA=/950x0/real-estate-what-it-is-and-how-it-works-3305882-FINAL-19013d1ad78f4d2d90ced41faa077a85.jpg";
				
				String link=PASSKEYFORGOTLINK+authorizationid;

				msg="Hi User Welcome to the World of Terabit, your OTP is ";
				msg=msg+String.valueOf(otp);
				
				Properties props = new Properties();   
				props.put("mail.smtp.host", "smtp.gmail.com");  
				props.put("mail.smtp.socketFactory.port", "465");  
				props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
				props.put("mail.smtp.auth", "true");  
				props.put("mail.smtp.port", "465"); 
				props.put("mail.smtp.ssl.checkserveridentity", true);
				   Session session = Session.getInstance(props,    
				           new javax.mail.Authenticator() {   
					   	
					   		@Override
				           protected PasswordAuthentication getPasswordAuthentication() {  
				           return new PasswordAuthentication(MAILFROM,PASSKEY);  
				           }    
				          });  
				   try {    
					   MimeMessage message = new MimeMessage(session);    
			           message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));    
			           message.setSubject(sub); 
			           MimeMessageHelper mimemessagehelper=new MimeMessageHelper(message,true,"UTF-8");
			           mimemessagehelper.setText("<body >\r\n" + 
			           		"\r\n" + 
			           		"	<div style=\"background-color:lightblue\"><center><h2>Background Image</h2></center></div>\r\n" + 
			           		"\r\n" + 
			           		"	<img src='"+img+"'width=100% height=70%>\r\n" + 
			           		"<p style=\"background-color: white\">"+msg+"</p>\r\n" + 
			           		"<a href=\""+link+"\"> Click here</a>" + 
			           		"\r\n" + 
			           		"\r\n" + 
			           		"</body>", true);
			           
			          
			           Transport.send(message);     
			          } 
				   catch (MessagingException e)
				   {
					   throw new RuntimeException(e);
				   }  
	}
}
