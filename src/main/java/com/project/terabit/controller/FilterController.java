package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.FilterProperty;
import com.project.terabit.model.Property;
import com.project.terabit.service.FilterService;

@RestController
@RequestMapping(value="terabit/api/v1/filter")
@CrossOrigin(origins = "*")
public class FilterController {

	/** The admin service. */
	//*****AutoWiring: AdminService Object	# # #
	@Autowired							
	private FilterService filterService;
	
	
	/** The log. */
	//*****Initializing:	log with the object of the current class(AdminController)	# # #
	Logger log=LoggerFactory.getLogger(this.getClass());		
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties property =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	

	
	private static final String RESPONSE="ERROR : ";
	
	@PostMapping(value= {"/getfilter/","/getfilter/{saltstring}"})
	public List<Property> filteredProperties(@PathVariable(name="saltstring", required=false) String saltstring,@RequestBody FilterProperty filterProperty) throws ServiceException,IOException{
		List<Property> filteredProperties = new ArrayList<>();
		try {
			filteredProperties = filterService.getPropertiesForTheFilter(filterProperty,saltstring);
		}catch(IOException|ServiceException exception) {
			
			//getting message definition from application.properties
			property.load(inputStream); 
			
			String message=RESPONSE+property.getProperty(exception.getMessage());
			
			//logging the message
			log.error(message);
		}catch(Exception exception) {
			//getting message definition from application.properties
			property.load(inputStream); 
			
			String message=RESPONSE+property.getProperty(exception.getMessage());
			
			//logging the message
			log.error(message);
		}
		return filteredProperties;
	}
	
	
	@PostMapping(value = {"/getCity/","/getCity/{saltstring}"})
	public Map<String, List<String>> getCityByState(@PathVariable(name="saltstring", required=false) String saltstring, UUID userId) throws ServiceException,IOException{
		Map<String, List<String>> returnMap = new HashMap<>();
		try {
			returnMap = filterService.getCityByState(saltstring,userId);
		}
		catch(IOException|ServiceException exception) {
			
			//getting message definition from application.properties
			property.load(inputStream); 
			
			String message=RESPONSE+property.getProperty(exception.getMessage());
			
			//logging the message
			log.error(message);
		}catch(Exception exception) {
			//getting message definition from application.properties
			property.load(inputStream); 
			
			String message=RESPONSE+property.getProperty(exception.getMessage());
			
			//logging the message
			log.error(message);
		}
		return returnMap;
	}
}
