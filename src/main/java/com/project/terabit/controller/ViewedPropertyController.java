package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Property;
import com.project.terabit.model.User;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.service.ServiceException;
import com.project.terabit.service.ViewedPropertyService;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/terabit/api/v1/viewedproperty")
public class ViewedPropertyController {


	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The property. */
	Properties property =new Properties();
	
	/** The input stream. */
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/** The feedback service. */
	@Autowired
	private ViewedPropertyService viewedPropertyService;
	
	/** Setting the value to be added before the exception*/
	private static final String RESPONSE = "ERROR :";
	
	private static final String CREATEVIEWEDPROPERTYSUCCESSMESSAGE = "Property viewed successfully";
	

	
	
	@PostMapping(value= {"/add/","/add/{saltstring}"})
	public Property createViewedProperty(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody ViewedProperty viewedProperty) throws ServiceException,ControllerException,IOException{
		
		Property newProperty = new Property();
		try {
			property.load(inputStream); 
			
			//calling the create seller service
			newProperty = viewedPropertyService.createViewedProperty(viewedProperty, saltstring);
			newProperty.setMessage(CREATEVIEWEDPROPERTYSUCCESSMESSAGE);
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			newProperty.setMessage(string);
			log.error(exception.getMessage());
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			newProperty.setMessage(string);
			log.error(string);
		}
		return newProperty;
	}
	
	@PostMapping(value= {"/get/","/get/{saltstring}"})
	public List<Property> getViewedPropertyForUser(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody User user) throws ServiceException,ControllerException,IOException{
		
		List<Property> newProperty = new ArrayList<>();
		try {
			property.load(inputStream); 
			
			//calling the create seller service
			newProperty = viewedPropertyService.getViewedPropertyForUser(user.getUserId(), saltstring);
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			log.error(exception.getMessage());
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(string);
		}
		return newProperty;
	}

}
