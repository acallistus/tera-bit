package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.service.ServiceException;
import com.project.terabit.service.VerifyAuthorizationService;


@RestController
@RequestMapping(value="/terabit/api/v1/user")
@CrossOrigin("*")
public class VerifyAuthorizationController {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	/** The property. */
	Properties property =new Properties();
	
	/** The input stream. */
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/** Setting the value to be added before the exception*/
	private static final String RESPONSE = "ERROR :";
	
	private static final String STATUS = "verification success";

	
	@Autowired
	VerifyAuthorizationService verifyauthorizationservice;
	
	@PostMapping(value="/verify/{authorizationid}")
	public String verifyUser(@PathVariable (name="authorizationid") String authorizationid ) throws IOException{

		String message;
		try {
			log.info(authorizationid);
			verifyauthorizationservice.userVerification(authorizationid);
			message=STATUS;
			log.info(STATUS);

		}
		catch(ServiceException exception) 
		{
			property.load(inputStream); 

			message=RESPONSE+property.getProperty(exception.getMessage());
			
			log.error(exception.getMessage());
			logg(message);

		}
		catch(Exception exception) 
		{
			property.load(inputStream); 

			message=RESPONSE+property.getProperty(exception.getMessage());
			
			log.error(message);
			logg(message);
		}
		return message;

		
	}
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}	

}
