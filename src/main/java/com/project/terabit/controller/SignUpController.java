package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.User;
import com.project.terabit.service.SignUpUserService;


/**@author deepik sriram & joshua shakespeare
/**
 * The Class SignUpController.
 * CRUD operations of USER is handled
 */
@RestController
@RequestMapping(value="terabit/api/v1/user")
@CrossOrigin(origins = "*")
public class SignUpController {
	
	/** Initializing the logger*/
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** Autowiring the signupservice */
	@Autowired
	private SignUpUserService signupservice;
	
	
	
	/** Initializing the properties */
	Properties prop = new Properties(); 
	
	/** The input stream. */
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
	


   


	/** Setting the value to be added before the exception*/
	private static final String RESPONSE = "ERROR :";
	/** The Constant SERVICEEXCEPTION_UPDATIONSUCCESS. */
	private static final String UPDATIONSUCCESS = "Sucessfully updated";
	
	/** The Constant successMessage. */
	private static final String CREATIONSUCCESSMESSAGE = "User has been added successfully";
	
	private static final String DELETIONSUCCESSMESSAGE = "User has been deleted successfully"; 
	/**
	 * Creates the user.
	 *
	 * @param saltstring the saltstring
	 * @param user the user
	 * @return the string
	 * @throws IOException 
	 * @throws Exception the exception
	 */
	/**creating the user or updating the user*/
	/**passing saltstring in pathvariable for authentification(for update) */
	@PostMapping(value= {"/save/","/save/{saltstring}"})
	public User saveUser(@PathVariable(name="saltstring" ,required=false) String saltstring, @RequestBody User user) throws ServiceException,ControllerException,IOException {
		//initializing the user to be returned
		User newUser=new User();
		
		try {
			//creating user if userid and saltstring is null
			if(user.getUserId()==null && saltstring==null) {
					newUser=signupservice.saveUser(user);
					newUser.setMessage(CREATIONSUCCESSMESSAGE);
					log.info(CREATIONSUCCESSMESSAGE);
			}
			
			//updating the existing user if above both are passed
			else {
					newUser=signupservice.updateUser(user,saltstring);
					newUser.setMessage(UPDATIONSUCCESS);
					log.info(UPDATIONSUCCESS);

			}
			
		}catch(ControllerException|ServiceException exception) {
			
			//getting message definition from application.properties
			prop.load(inputStream); 
			
			String message=RESPONSE+prop.getProperty(exception.getMessage());
			
			//setting the message in the newUser(to be returned)
			newUser.setMessage(message);
			
			//logging the message
			log.error(exception.getMessage());
		}catch(Exception exception) {
			//getting message definition from application.properties
			prop.load(inputStream); 
			
			String message=RESPONSE+prop.getProperty(exception.getMessage());
			
			//setting the message in the newUser(to be returned)
			newUser.setMessage(message);
			
			//logging the message
			log.error(message);
		}
		return newUser;
	}
	
	/**deleting the user*/
	/**passing saltstring in pathvariable for authentification */
    @PostMapping(value= {"/delete/","/delete/{saltstring}"})
    public User deleteUser(@PathVariable(name="saltstring", required=true) String saltstring, @RequestBody User user) throws ControllerException,IOException {
    	   
    	   //initializing the user to be returned
           User newUser=new User();
           
         //initializing a string for defining error messages
           String response="";
           try {
        	   	//passing to deleteuser in service
				newUser=signupservice.deleteUser(saltstring,user);
				newUser.setMessage(DELETIONSUCCESSMESSAGE);
				log.info(DELETIONSUCCESSMESSAGE);
				
           }catch(ControllerException|ServiceException exception) {
        	   
        	//setting value of response to error to differentiate error and success messages
   			response="ERROR : ";
   			
   			//getting message definition from application.properties
 			prop.load(inputStream); 
 			String message=response+prop.getProperty(exception.getMessage());
 			
 			//setting the message in the newUser(to be returned)
 			newUser.setMessage(message);
 			
 			//logging the message
 			log.error(exception.getMessage());
 			
 		}catch(Exception exception){
 			
        	//setting value of response to error to differentiate error and success messages
            response="ERROR : ";
            
            //getting message definition from application.properties
            prop.load(inputStream);
            String message=response+prop.getProperty(exception.getMessage());
            
            //setting the message in the newUser(to be returned)
            newUser.setMessage(response+prop.getProperty(exception.getMessage()));
            
            //logging the message
            log.error(message);      
        }
           
           return newUser;
    }
    
    @PostMapping(value= {"/forgetpassword/{mailid}"})
    public String forgetPassword(@PathVariable(name="mailid", required=true) String mailid) throws ServiceException,IOException,Exception
    {
    	String status="Failed";
    	try {
    		signupservice.forgetPassword(mailid);
    		status="Success";
    	}
    	catch(ControllerException|ServiceException exception) 
		{
    		prop.load(inputStream); 
			status=RESPONSE+prop.getProperty(exception.getMessage());
			
			log.error(exception.getMessage());
			
		}
		catch(Exception exception) 
		{
			prop.load(inputStream); 
			status=RESPONSE+prop.getProperty(exception.getMessage());
			
			log.error(status);
		}
			
    	return status;
    }
	
    @PostMapping(value= {"/changepassword/{authorizationid}"})
    public String changePassword(@PathVariable(name="authorizationid", required=true) String authorizationid,@RequestBody User user) throws ServiceException,IOException,Exception
    {
    	String status="Failed";
    	try {
    		user.setAuthorizationstring(authorizationid);
    		signupservice.changePassword(user);
    		status="Success";
    	}
    	catch(ControllerException|ServiceException exception) 
		{
    		prop.load(inputStream); 
			status=RESPONSE+prop.getProperty(exception.getMessage());
			
			log.error(exception.getMessage());
			
		}
		catch(Exception exception) 
		{
			prop.load(inputStream); 
			status=RESPONSE+prop.getProperty(exception.getMessage());
			
			log.error(status);
		}
    	return status;
    }
	
}
