package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Cart;
import com.project.terabit.model.SaveCart;
import com.project.terabit.model.User;
import com.project.terabit.service.CartService;

@RestController
@RequestMapping(value="/terabit/api/v1/cart")
@CrossOrigin(origins = "*")
public class CartController {
	
	@Autowired
	CartService cartService;
	
	/** The log. */
	//*****Initializing:	log with the object of the current class(AdminController)	# # #
	Logger log=LoggerFactory.getLogger(this.getClass());		
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties property =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	
	
	private static final String RESPONSE="ERROR : ";

	
	@PostMapping(value={"/save/","/save/{saltstring}"})
	public List<Cart> saveCart(@PathVariable(value="saltstring")String saltString,@RequestBody SaveCart cartToBeSaved) throws ServiceException,ControllerException,IOException
	{
		List<Cart> cartToBeReturned = new ArrayList<>();
		try {
			cartToBeReturned=cartService.saveCart(saltString, cartToBeSaved);
		}
		catch(ServiceException exception) {
			property.load(inputStream); 
   			log.error(exception.getMessage());
	   }catch(Exception exception){
		   property.load(inputStream); 
           String error=RESPONSE+property.getProperty(exception.getMessage());
           log.error(error);      
	   }
		return cartToBeReturned;
	}
	
	
	@PostMapping(value={"/delete/","/delete/{saltstring}"})
	public List<Cart> deleteCart(@PathVariable(value="saltstring")String saltString,@RequestBody SaveCart userCartToBeDeleted) throws ServiceException,ControllerException,IOException
	{
		List<Cart> cartFetched=new ArrayList<>();
		try {
			cartFetched=cartService.deleteCart(saltString, userCartToBeDeleted);
		}
		catch(ServiceException exception) {
			property.load(inputStream); 
   			log.error(exception.getMessage());
	   }catch(Exception exception){
		   property.load(inputStream); 
           String error=RESPONSE+property.getProperty(exception.getMessage());
           log.error(error);      
	   }
		return cartFetched;
	}
	
	@PostMapping(value={"/get/","/get/{saltstring}"})
	public List<Cart> getCart(@PathVariable(value="saltstring")String saltString,@RequestBody User userCarts) throws ServiceException,ControllerException,IOException
	{
		List<Cart> cartFetched=new ArrayList<>();
		try {
			cartFetched=cartService.getCart(saltString, userCarts.getUserId());
		}
		catch(ServiceException exception) {
			property.load(inputStream); 
   			log.error(exception.getMessage());
	   }catch(Exception exception){
		   property.load(inputStream); 
           String error=RESPONSE+property.getProperty(exception.getMessage());
           log.error(error);      
	   }
		return cartFetched;
	}

}
