package com.project.terabit.controller;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Image;
import com.project.terabit.service.ImageService;

@RestController
@RequestMapping(value="terabit/api/v1/image")
@CrossOrigin(origins = "*")
public class ImageController {

	/** The log. */
	//*****Initializing:	log with the object of the current class(AdminController)	# # #
	Logger log=LoggerFactory.getLogger(this.getClass());		
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties property =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	
	
	//*****AutoWiring: ImageService Object	# # #
	@Autowired							
	private ImageService imageService;
	
	
	private static final String RESPONSE="ERROR : ";
	
	private static final String CREATIONSUCCESSMESSAGE = "Image successfully created";
	
	private static final String UPDATIONSUCCESSMESSAGE = "Image successfully updated";
	
	private static final String DELETIONSUCCESSMESSAGE = "Image successfully deleted";
	
	@PostMapping(value= {"/save/","/save/{saltstring}"})
	public Image createImage(@PathVariable (name="saltstring" ,required=false) String saltstring,@RequestBody Image image) throws Exception{
		Image newImage = new Image();
		try {
			newImage = imageService.createImage(saltstring, image);
			newImage.setMessage(CREATIONSUCCESSMESSAGE);
		}
		catch(Exception exception) {
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			newImage.setMessage(s);
			log.error(s);
		}
		return newImage;
	}
	
	@PostMapping(value= {"/delete/","/delete/{saltstring}"})
	public Image deleteImage(@PathVariable (name="saltstring" ,required=false) String saltstring,@RequestBody Image image) throws Exception{
		Image newImage = new Image();
		try {
			newImage = imageService.deleteImage(saltstring, image);
			newImage.setMessage(DELETIONSUCCESSMESSAGE);
		}
		catch(Exception exception) {
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			newImage.setMessage(s);
			log.error(s);
		}
		return newImage;
	}
	
	@PostMapping(value= {"/update/","/update/{saltstring}"})
	public Image updateImage(@PathVariable (name="saltstring" ,required=false) String saltstring,@RequestBody Image image) throws Exception{
		Image newImage = new Image();
		try {
			newImage = imageService.updateImage(saltstring, image);
			newImage.setMessage(UPDATIONSUCCESSMESSAGE);
		}
		catch(Exception exception) {
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			newImage.setMessage(s);
			log.error(s);
		}
		return newImage;
	}
}
