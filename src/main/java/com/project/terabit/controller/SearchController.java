package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Property;
import com.project.terabit.model.Search;

import com.project.terabit.model.User;

import com.project.terabit.service.SearchService;

/**@author deepik sriram 
**/
@RestController
@RequestMapping(value="terabit/api/v1")
@CrossOrigin(origins = "*")
public class SearchController {
	
	/** The log. */
	//*****Initializing:	log with the object of the current class(AdminController)	# # #
	Logger log=LoggerFactory.getLogger(this.getClass());		
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties property =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
	
	@Autowired
	SearchService searchService;

	private static final String RESPONSE="ERROR : ";
	
	@PostMapping(value= {"/search/","/search/{saltstring}"})
	public List<Property> searchProperty(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Search search) throws IOException{
		List<Property> propertyList = new ArrayList<>();
		try {
			propertyList = searchService.search(search, saltstring);
		}
		catch(Exception exception) {
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			log.error(s);
		}
		return propertyList;
	}

	
	@PostMapping(value= {"/searchForSeller/","/searchForSeller/{saltstring}"})
	public User searchUserForSeller(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody User user) throws IOException{
		User returnUser = new User();
		try {
			returnUser = searchService.searchForSeller(user, saltstring);
		}
		catch(Exception exception) {
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			log.error(s);
		}
		return returnUser;
	}

}
