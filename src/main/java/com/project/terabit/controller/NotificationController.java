package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Notification;
import com.project.terabit.model.User;
import com.project.terabit.service.NotificationService;

@RestController
@RequestMapping(value="/terabit/api/v1/notification")
@CrossOrigin(origins = "*")
public class NotificationController {
	
	@Autowired
	NotificationService notificationService;
	
	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The property. */
	Properties property =new Properties();
	
	/** The input stream. */
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/** Setting the value to be added before the exception*/
	private static final String RESPONSE = "ERROR :";
	
	@PostMapping(value= {"/save/","/save/{saltstring}"})
	public List<Notification> saveNotifications(@PathVariable(value="saltstring")String saltString,@RequestBody Notification notificationToBeSaved) throws ServiceException,ControllerException,IOException
		{
		List<Notification> notificationsFetched=new ArrayList<>();
		try{
			
			notificationsFetched=notificationService.saveNotification(saltString, notificationToBeSaved);
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			for(Notification notification:notificationsFetched)
				notification.setMessage(string);
			log.error(exception.getMessage());
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			for(Notification notification:notificationsFetched)
				notification.setMessage(string);
			log.error(string);
		}		
		return notificationsFetched;
	}
	
	@PostMapping(value= {"/delete/","/delete/{saltstring}"})
	public List<Notification> deleteNotification(@PathVariable(value="saltstring")String saltString,@RequestBody Notification notificationToBeDeleted) throws ServiceException,ControllerException,IOException
	{
		List<Notification> notificationsFetched=new ArrayList<>();
			try{
				notificationsFetched=notificationService.deleteNotification(saltString, notificationToBeDeleted);
				}
			catch(ControllerException|ServiceException exception) 
			{
				property.load(inputStream); 
				String string=RESPONSE+property.getProperty(exception.getMessage());
				for(Notification notification:notificationsFetched)
					notification.setMessage(string);	
				log.error(exception.getMessage());
			}
			catch(Exception exception) 
			{
				property.load(inputStream); 
				String string=RESPONSE+property.getProperty(exception.getMessage());
				for(Notification notification:notificationsFetched)
					notification.setMessage(string);
				log.error(string);
			}
		return notificationsFetched;
	}
	@PostMapping(value= {"/get/","/get/{saltstring}"})
	public List<Notification> getNotification(@PathVariable(value="saltstring")String saltString,@RequestBody User userNotification) throws ServiceException,ControllerException,IOException
	{
			List<Notification> notificationsFetched=new ArrayList<>();
			try{
				notificationsFetched=notificationService.getNotification(saltString, userNotification.getUserId());
				}
			catch(ControllerException|ServiceException exception) 
			{
				property.load(inputStream); 
				String string=RESPONSE+property.getProperty(exception.getMessage());
				for(Notification notification:notificationsFetched)
					notification.setMessage(string);	
				log.error(exception.getMessage());
			}
			catch(Exception exception) 
			{
				property.load(inputStream); 
				String string=RESPONSE+property.getProperty(exception.getMessage());
				for(Notification notification:notificationsFetched)
					notification.setMessage(string);
				log.error(string);
			}
		return notificationsFetched;
	}
}
