package com.project.terabit.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.UserDetail;
import com.project.terabit.service.ServiceException;
import com.project.terabit.service.UserDetailsFromViewedPropertyPropertyIdServiceImpl;

@RestController
@RequestMapping(value="terabit/api/v1/viewedproperty")
@CrossOrigin(origins = "*")
public class UserDetailsFromViewedPropertyPropertyId {
	
	 /** The log. */
   	Logger log=LoggerFactory.getLogger(this.getClass());
       
       /** The prop. */
       Properties prop = new Properties();
       
       /** The input stream. */
       InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
       
	
	@Autowired
	UserDetailsFromViewedPropertyPropertyIdServiceImpl userdetailservice;
	
	@PostMapping(value= {"/getuserdetails/","/getuserdetails/{saltstring}"})
	public List<UserDetail> getUserDetails(@PathVariable(name="saltstring", required=false)String saltstring,@RequestBody UserDetail incomminguserdetail) throws Exception{
		String response="";
		List<UserDetail> userdetailslist= new ArrayList<>();
		
		try {
			userdetailslist=userdetailservice.getUserDetailsForPropertyId(saltstring,incomminguserdetail);
			
		}catch(ServiceException exception) {
   			prop.load(inputStream); 
   			log.error(exception.getMessage());
	   }catch(Exception exception){
           response="ERROR : ";
           prop.load(inputStream); 
           String error=response+prop.getProperty(exception.getMessage());
           log.error(error);      
    }
		return userdetailslist;
		
	}

}
