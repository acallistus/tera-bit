package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Admin;
import com.project.terabit.model.Seller;
import com.project.terabit.service.SellerApprovalService;
import com.project.terabit.service.ServiceException;

@RestController
@RequestMapping(value="/terabit/api/v1/sellerapproval")
@CrossOrigin(origins = "*")
public class SellerApprovalController {
	
	/** The seller service. */
	@Autowired
	private SellerApprovalService sellerApprovalService;
	

	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The property. */
	Properties property =new Properties();
	
	/** The input stream. */
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/** Setting the value to be added before the exception*/
	private static final String RESPONSE = "ERROR :";
	
	private static final String SUCCESSMESSAGE = "Seller Approved successfully";

	@PostMapping(value= {"/save/","/save/{saltstring}"})
	public Seller createSellerByAdmin(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Seller seller) throws ServiceException,ControllerException,IOException{
		Seller sellerToBeReturned = new Seller();
		try {
			sellerToBeReturned = sellerApprovalService.createSellerByAdmin(saltstring, seller);
			sellerToBeReturned.setMessage(SUCCESSMESSAGE);
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			sellerToBeReturned.setMessage(string);
			log.error(exception.getMessage());
			logg(string);
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			sellerToBeReturned.setMessage(string);
			log.error(string);
			logg(string);
		}
		return sellerToBeReturned;
	}
	
	
	@PostMapping(value= {"/get/","/get/{saltstring}"})
	public List<Seller> getSellerByAdmin(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Admin admin) throws Exception{
		List<Seller> sellerList = new ArrayList<>();
		try {
			sellerList = sellerApprovalService.getSellerByAdmin(saltstring, admin);
			
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(exception.getMessage());
			logg(string);
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(string);
			logg(string);
		}
		return sellerList;
	}
	
	

	@PostMapping(value= {"/delete/","/delete/{saltstring}"})
	public List<Seller> deleteSellerByAdmin(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Admin admin) throws Exception{
		List<Seller> sellerList = new ArrayList<>();
		try {
			sellerList = sellerApprovalService.deleteSellerByAdmin(saltstring, admin);
			
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(exception.getMessage());
			logg(string);
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(string);
			logg(string);
		}
		return sellerList;
	}
	
	@PostMapping(value= {"/getPendingSeller/","/getPendingSeller/{saltstring}"})
	public List<Seller> getApprovalPendingSeller(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Admin admin) throws Exception{
		List<Seller> sellerList = new ArrayList<>();
		try {
			sellerList = sellerApprovalService.getApprovalPendingSeller(saltstring, admin);
			
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(exception.getMessage());
			logg(string);
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			log.error(string);
			logg(string);
		}
		return sellerList;
	}
	
	@PostMapping(value= {"/cancelApproval/","/cancelApproval/{saltstring}"})
	public String cancelSellerApproval(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Admin admin) throws Exception{
		String message;
		try {
			
			message = sellerApprovalService.cancelSellerApproval(saltstring, admin);
			log.info(message+" "+admin.getSellerId());
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			message=RESPONSE+property.getProperty(exception.getMessage());
			log.error(exception.getMessage());
			logg(message);
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			message=RESPONSE+property.getProperty(exception.getMessage());
			log.error(message);
			logg(message);
		}
		return message;
	}
	

	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}	
	
}
